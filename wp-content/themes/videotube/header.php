<?php if( !defined('ABSPATH') ) exit;?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Noto+Sans&display=swap" rel="stylesheet">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge"><![endif]-->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--[if lt IE 9]>
	  <script src="<?php print get_template_directory_uri();?>/assets/js/ie8/html5shiv.js"></script>
      <script src="<?php print get_template_directory_uri();?>/assets/js/ie8/respond.min.js"></script>
	<![endif]-->	
	<?php wp_head();?>
</head>
<body <?php body_class();?>>
<?php global $videotube;?>

	<div id="header">
		<div class="container">
			<div class="row border-md-bottom align-items-md-center">
				<div class="col col-md-3 col-sm-4 pl-md-0" id="logo">
					<a title="<?php bloginfo('description');?>" href="<?php print home_url();?>">
						<?php
							global $req_search;
							$language = $_SESSION['language'] ?? 'ja';
							$image_lang = get_template_directory_uri() . '/img/ja.png';
							if ($language === 'en') {
								$image_lang = get_template_directory_uri() . '/img/en.png';
							} else if ($language === 'zh') {
								$image_lang = get_template_directory_uri() . '/img/zh.png';
							}
							$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
							// $logo_image = isset( $videotube['logo']['url'] ) ? $videotube['logo']['url'] : get_template_directory_uri() . '/img/logosub.png'; 
							$logo_image = get_template_directory_uri() . '/img/no_image_available-150x150.jpg'; 
						?>
						<img src="<?php print esc_url( $logo_image ); ?>" style="object-fit: contain; height:74px" alt="<?php esc_attr( bloginfo('description') );?>" />
					</a>
				</div>
				<form method="get" action="<?php print home_url();?>" class="d-flex justify-content-flex-end col-md-8 col-sm-7">	
					<div class="d-flex col col-md-4 col-sm-12 pr-0 pl-0" id="header-search">
						<?php if( isset( $videotube['video_search'] ) && $videotube['video_search'] == 1 ):?>
							<input type="hidden" name="post_type" value="video">
						<?php endif;?>
						<input value="<?php if(!empty($req_search)){ print $req_search; } ?>" name="s" type="text" placeholder="<?php _e('Search videos...','mars')?>" id="search">
						<div class="button-search"><span>Search</span></div>
					</div>
				</form>
				<div class="box-language col-md-1 col-sm-1">
					<div class="dropdown">
						<button class="dropbtn">
							<img src="<?php print $image_lang ?>"
								width="20" height="15">
						</button>
						<form>
							<div class="dropdown-content">
								<a class="ja">
									<img src="<?php echo get_template_directory_uri() . '/img/ja.png' ?>"
									width="20" height="15">
								</a>
								<a class="en">
									<img src="<?php echo get_template_directory_uri() . '/img/en.png' ?>"
									width="20" height="15">
								</a>
								<a class="zh">
									<img src="<?php echo get_template_directory_uri() . '/img/zh.png' ?>"
									width="20" height="15">
								</a>
							</div>
						<form>
					</div>
				</div>
			</div>
		</div>
	</div><!-- /#header -->
	<div id="navigation-wrapper">
		<div class="container pl-md-0">
			<div class="navbar-header">
				<!-- <div class="box-language-sp col-md-1 col-sm-1"> -->
					<div class="dropdown box-language-sp">
						<div class="dropbtn">
							<img src="<?php print $image_lang ?>"
								width="20" height="15">
						</div>
						<form id="form-language" action="" method="POST">
							<div class="dropdown-content">
								<a class="ja">
									<img src="<?php echo get_template_directory_uri() . '/img/ja.png' ?>"
									width="20" height="15">
								</a>
								<a class="en">
									<img src="<?php echo get_template_directory_uri() . '/img/en.png' ?>"
									width="20" height="15">
								</a>
								<a class="zh">
									<img src="<?php echo get_template_directory_uri() . '/img/zh.png' ?>"
									width="20" height="15">
								</a>
							</div>
						<form>
					</div>
				<!-- </div> -->
			  <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			</div>
			<nav class="collapse navbar-collapse bs-navbar-collapse ml-0 p-0 nav_sp" role="navigation">
			<!-- menu -->
			  	<?php 
			  		if( has_nav_menu('header_main_navigation') ){
				  		wp_nav_menu(array(
				  			'theme_location'=>'header_main_navigation',
				  			'menu_class'=>'nav navbar-nav list-inline menu',
				  			'walker' => new Mars_Walker_Nav_Menu(),
				  			'container'	=>	null
				  		));
			  		}
			  		else{
						?>
			  				<ul class="nav navbar-nav list-inline menu"><li class="active"><a href="<?php print home_url();?>"><?php _e('Home','mars')?></a></li></ul>						
						<?php 			  			
			  		}
			  	?>
			</nav>
		</div>
	</div><!-- /#navigation-wrapper -->	
<script type="text/javascript">
	(function($){
		$(document).ready(function(){
			$('.dropdown-content a').on('click',function(){
				// $('#language').val($(this)[0].classList[0]);
				$.ajax({
                    type : "post",
                    dataType : "json",
                    url : '<?php echo admin_url('admin-ajax.php');?>',
                    data : {
                        action: "setlanguage",
                        language : $(this)[0].classList[0],
                    },
                    context: this,
                    beforeSend: function(){
                    },
                    success: function(response) {
                        if(response.success) {
							location.reload();  
                        }
                        else {
                            alert('error');
                        }
                    },
                    error: function( jqXHR, textStatus, errorThrown ){
                        console.log( 'The following error occured: ' + textStatus, errorThrown );
                    }
                })
                return false;
			})
		});
	})(jQuery);
</script>