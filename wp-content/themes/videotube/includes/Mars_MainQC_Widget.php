<?php
/**
 * Main QC Widget
 * @author 		PTS
 * @category 	Core
 * @version     1.0.0
 */
if( !defined('ABSPATH') ) exit;
if( !function_exists('Mars_MainQC_Widgets') ){
	function Mars_MainQC_Widgets() {
		register_widget('Mars_MainQC_Widget_Class');
	}
	add_action('widgets_init', 'Mars_MainQC_Widgets');
}
class Mars_MainQC_Widget_Class extends WP_Widget{
	
	function __construct(){
		$widget_ops = array( 'classname' => 'mars-mainqc-widgets', 'description' => __('Main QC Widget', 'mars') );
		
		parent::__construct( 'mars-mainqc-widgets' , __('Main QC Widget', 'mars') , $widget_ops);
	}	

	function widget($args, $instance){
		global $post;	
		extract( $args );
		wp_reset_postdata();wp_reset_query();
		$title = apply_filters('widget_title', $instance['title'] );
		$video_rows = isset( $instance['rows'] ) ? (int)$instance['rows'] : 1;
		$columns = isset( $instance['columns'] ) ? absint( $instance['columns'] ) : 3;
		$class_columns = ( 12%$columns == 0 ) ? 12/$columns : 3;
		
		$tablet_columns = isset( $instance['tablet_columns'] ) ? (int)$instance['tablet_columns'] : 2;
		
		$tablet_columns = ceil(12/$tablet_columns);
		
		$mobile_columns = isset( $instance['mobile_columns'] ) ? (int)$instance['mobile_columns'] : 1;
		
		$mobile_columns = ceil(12/$mobile_columns);
		
		$thumbnail_size = isset( $instance['thumbnail_size'] ) ? $instance['thumbnail_size'] : 'video-featured';
		
		if( empty( $thumbnail_size ) ){
			$thumbnail_size  = 'video-featured';
		}	
		
		?>
			<div id="mainqc-wrapper">
				<!-- <div class="section-header">
						<h3 class="" style="padding-left:10px"><?php print $title;?></h3>
				</div>                 
				<div class="mainqc-wrapper">
					<div class="row">
						<div class="col-md-12">
							<div style="padding-bottom:10px" class="col-md-<?php print $class_columns;?> col-sm-<?php echo esc_attr( $tablet_columns );?> col-xs-<?php echo esc_attr( $mobile_columns );?> item responsive-height">
								<div class="item-img">
								<?php 
									$no_image = '"'.home_url().'/wp-content/themes/videotube/img/no_image_available-150x150.jpg 150w,'.home_url().'/wp-content/themes/videotube/img/no_image_available-165x108.jpg 165w,'.home_url().'/wp-content/themes/videotube/img/no_image_available-295x197.jpg 295w,'.home_url().'/wp-content/themes/videotube/img/no_image_available-300x300.jpg 300w,'.home_url().'/wp-content/themes/videotube/img/no_image_available-360x240.jpg 360w,'.home_url().'/wp-content/themes/videotube/img/no_image_available-230x150.jpg 230w,'.home_url().'/wp-content/themes/videotube/img/no_image_available-600x440.jpg 600w'.'"';
									print '<a href="'.get_permalink(get_the_ID()).'">
										<img style="width: 100%;height: 100%;object-fit: contain;" class="img-responsive wp-post-image" loading="lazy" src="" alt=""
										srcset='.$no_image.'></a>';
								?>
								</div> 
							</div>
							<div style="padding-top:10px; padding-bottom:10px" class="col-md-<?php print $class_columns;?> col-sm-<?php echo esc_attr( $tablet_columns );?> col-xs-<?php echo esc_attr( $mobile_columns );?> item responsive-height">
								<div class="item-img">
								<?php 
									$no_image = '"'.home_url().'/wp-content/themes/videotube/img/no_image_available-150x150.jpg 150w,'.home_url().'/wp-content/themes/videotube/img/no_image_available-165x108.jpg 165w,'.home_url().'/wp-content/themes/videotube/img/no_image_available-295x197.jpg 295w,'.home_url().'/wp-content/themes/videotube/img/no_image_available-300x300.jpg 300w,'.home_url().'/wp-content/themes/videotube/img/no_image_available-360x240.jpg 360w,'.home_url().'/wp-content/themes/videotube/img/no_image_available-230x150.jpg 230w,'.home_url().'/wp-content/themes/videotube/img/no_image_available-600x440.jpg 600w'.'"';
									print '<a href="'.get_permalink(get_the_ID()).'">
										<img style="width: 100%;height: 100%;object-fit: contain;" class="img-responsive wp-post-image" loading="lazy" src="" alt=""
										srcset='.$no_image.'></a>';
								?>
								</div> 
							</div>
						</div>
					</div>
				</div> -->
			</div>
		<?php 
	}
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['rows'] = absint( $new_instance['rows'] );
		$instance['columns']	=	absint( $new_instance['columns'] );
		$instance['tablet_columns'] = strip_tags( $new_instance['tablet_columns'] );
		$instance['mobile_columns'] = strip_tags( $new_instance['mobile_columns'] );
		$instance['thumbnail_size'] = strip_tags( $new_instance['thumbnail_size'] );
		return $instance;
		
	}
	function form( $instance ){
		$defaults = array( 
			'title' => __('Main QC', 'mars'),
			'columns'	=>	3,
			'tablet_columns'	=>	2,
			'mobile_columns'	=>	1,
			'thumbnail_size'	=>	''
		);
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'mars'); ?></label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo ( isset( $instance['title'] ) ? $instance['title'] : null ); ?>" style="width:100%;" />
		</p>		
		<p>  
		    <label for="<?php echo $this->get_field_id( 'columns' ); ?>"><?php _e('Desktop Columns:', 'mars'); ?></label>
		    <input id="<?php echo $this->get_field_id( 'columns' ); ?>" name="<?php echo $this->get_field_name( 'columns' ); ?>" value="<?php echo $instance['columns']; ?>" style="width:100%;" />
		    <small><?php _e('You can set the columns for displaying the Videos, example: 3,4 or 6.','mars');?></small>
		</p>
		<p>  
		    <label for="<?php echo $this->get_field_id( 'tablet_columns' ); ?>"><?php _e('Tablet Columns:', 'mars'); ?></label>
		    <input id="<?php echo $this->get_field_id( 'tablet_columns' ); ?>" name="<?php echo $this->get_field_name( 'tablet_columns' ); ?>" value="<?php echo $instance['tablet_columns']; ?>" style="width:100%;" />
		</p>		
		<p>  
		    <label for="<?php echo $this->get_field_id( 'mobile_columns' ); ?>"><?php _e('Mobile Columns:', 'mars'); ?></label>
		    <input id="<?php echo $this->get_field_id( 'mobile_columns' ); ?>" name="<?php echo $this->get_field_name( 'mobile_columns' ); ?>" value="<?php echo $instance['mobile_columns']; ?>" style="width:100%;" />
		</p>
		<p>  
		    <label for="<?php echo $this->get_field_id( 'thumbnail_size' ); ?>"><?php _e('Thumbnail Size:', 'mars'); ?></label>
		    <input id="<?php echo $this->get_field_id( 'thumbnail_size' ); ?>" name="<?php echo $this->get_field_name( 'thumbnail_size' ); ?>" value="<?php echo esc_attr( $instance['thumbnail_size'] );?>" style="width:100%;" />
		    <span class="description">
		    	<?php 
		    		esc_html_e( 'Enter the custom image size of leave blank for default.', 'mars' );
		    	?>
		    </span>
		</p>		
		<p>  
		    <label for="<?php echo $this->get_field_id( 'rows' ); ?>"><?php _e('Rows:', 'mars'); ?></label>
		    <input id="<?php echo $this->get_field_id( 'rows' ); ?>" name="<?php echo $this->get_field_name( 'rows' ); ?>" value="<?php echo (isset( $instance['rows'] )) ? (int)$instance['rows'] : 1; ?>" style="width:100%;" />
		</p>
	<?php
	}
}