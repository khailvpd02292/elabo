<?php if( !defined('ABSPATH') ) exit;?>
<?php get_header(); ?>
	<div class="container">
		<?php if ( function_exists('yoast_breadcrumb') ) {
			yoast_breadcrumb('<p id="breadcrumbs">','</p>');
		} 
		global $wp;
		$queried_object = get_queried_object('term');
		$term = $queried_object->term_id;
		$parent_tax = $queried_object->parent;
		$taxonomy = get_term_children($term, get_query_var('taxonomy'));

		$all_taxonomy = get_terms( array(
			'taxonomy' => get_query_var('taxonomy'),
			'offset'  =>  0,
			'include' => $taxonomy,
			'hide_empty'  => false, 
			'orderby'  => 'include', 
			) );
		$total_taxonomy = count($all_taxonomy);
		$count_taxonomy = 0;

		$page = isset( $_GET["page"] ) ? $_GET["page"] : 1;

		$per_page = get_option( 'posts_per_page' );
		$children_taxonomy = get_terms( array(
				'taxonomy' => get_query_var('taxonomy'),
				'offset'  =>  ( $page-1 ) * $per_page,
				'include' => $taxonomy,
				'hide_empty'  => false, 
				'orderby'  => 'include',
				'number' => $per_page
				) );

		$pages = ceil($total_taxonomy/$per_page);

		$current_url = home_url(add_query_arg(array(), $wp->request));

		foreach ( $children_taxonomy as $pterm ) {
			$count_taxonomy = $count_taxonomy + $pterm->count;
		}
		$columns_tag = '8';
		if(strpos($_SERVER['REQUEST_URI'] , 'video_tag/cast')){
			$columns_tag = '12';
		} else if (strpos($_SERVER['REQUEST_URI'] , 'video_tag/genre')) {
			$columns_tag = '12';
		} else if (strpos($_SERVER['REQUEST_URI'] , 'video_tag/maker')) {
			$columns_tag = '12';
		}	
		?>	
		<div class="row">
			<div class="col-md-<?php echo $columns_tag ?> col-sm-12 main-content">
				<?php if($parent_tax == 0): ?>
					<div class="section-header">
						<h1 class="page-title"><?php
						echo $queried_object->name .' ('.$total_taxonomy.') ' ?></h1>	                    	               
					</div>
					<?php 
						
					echo '<div class="row"> <ul class="list-taxonomy">';
					foreach ( $children_taxonomy as $pterm ) {
						//Get the Child terms
						echo '<li class="col-md-4 col-sm-12 p-0"><a href="' . 	get_term_link( $pterm ) . '">' . $pterm->name .' ('.$pterm->count.') '. '</a></li>';   
					}
					echo '</div></ul>';
					?>
					
					<?php 

					if( $pages > 1 ):
					$pagenum_link = $current_url.'/%_%';
						$paginate = paginate_links(array(
								'base'     => $pagenum_link,
								'format'   => '?page=%#%',
								'current' => $page,
								'mid_size' => 3,
								'total' => $total_taxonomy,
								'type'	=>	'array',
								'prev_next'    => true,
								'prev_text' => !is_rtl() ? __( '&larr; Previous ', 'mars' ) : __( ' &rarr; Previous', 'mars' ),
								'next_text' => !is_rtl() ? __( 'Next &rarr;', 'mars' ) : __( 'Next &larr;', 'mars' )
							));

						if ( is_array( $paginate ) ) :
							echo '<ul class="pagination d-flex justify-content-center">';
							foreach ( $paginate as $page ) {
									echo "<li>$page</li>";
							}
							echo '</ul>';
						endif;
					endif;
					?>
				<?php else: ?>
					<?php if( have_posts() ):?>
					<div class="row video-section meta-maxwidth-230">
						<div class="section-header">
							<?php the_archive_title( '<h1 class="page-title">', '</h1>' )?>
							<?php do_action('mars_orderblock_videos');?>
						</div>					
						<?php 			
							while ( have_posts() ) : the_post();
							
								get_template_part( 'loop', 'video' );
							
							endwhile;
						?>
					</div>
					<?php do_action( 'mars_pagination', null );?>
					<?php else:?>
						<div class="alert alert-info"><?php _e('Not Found','mars')?></div>
					<?php endif;?>
				<?php endif; ?>
			</div>
			<?php if($columns_tag == 8){ ?>

				<div id="mainqc-wrapper" class="col-md-4 col-sm-12">
					<!-- <div class="section-header">
							<h3 class="" style="padding-left:10px"><?php print $title;?></h3>
					</div>                
					<div class="mainqc-wrapper">
						<div class="row">
							<div class="col-md-12">
								<div style="padding-bottom:10px" class="col-md-<?php print $class_columns;?> col-sm-<?php echo esc_attr( $tablet_columns );?> col-xs-<?php echo esc_attr( $mobile_columns );?> item responsive-height">
									<div class="item-img">
									<?php 
										$no_image = '"'.home_url().'/wp-content/themes/videotube/img/no_image_available-150x150.jpg 150w,'.home_url().'/wp-content/themes/videotube/img/no_image_available-165x108.jpg 165w,'.home_url().'/wp-content/themes/videotube/img/no_image_available-295x197.jpg 295w,'.home_url().'/wp-content/themes/videotube/img/no_image_available-300x300.jpg 300w,'.home_url().'/wp-content/themes/videotube/img/no_image_available-360x240.jpg 360w,'.home_url().'/wp-content/themes/videotube/img/no_image_available-230x150.jpg 230w,'.home_url().'/wp-content/themes/videotube/img/no_image_available-600x440.jpg 600w'.'"';
										print '<a href="'.get_permalink(get_the_ID()).'">
											<img style="width: 100%;height: 100%;object-fit: contain;" class="img-responsive wp-post-image" loading="lazy" src="" alt=""
											srcset='.$no_image.'></a>';
									?>
									</div> 
								</div>
								<div style="padding-top:10px; padding-bottom:10px" class="col-md-<?php print $class_columns;?> col-sm-<?php echo esc_attr( $tablet_columns );?> col-xs-<?php echo esc_attr( $mobile_columns );?> item responsive-height">
									<div class="item-img">
									<?php 
										$no_image = '"'.home_url().'/wp-content/themes/videotube/img/no_image_available-150x150.jpg 150w,'.home_url().'/wp-content/themes/videotube/img/no_image_available-165x108.jpg 165w,'.home_url().'/wp-content/themes/videotube/img/no_image_available-295x197.jpg 295w,'.home_url().'/wp-content/themes/videotube/img/no_image_available-300x300.jpg 300w,'.home_url().'/wp-content/themes/videotube/img/no_image_available-360x240.jpg 360w,'.home_url().'/wp-content/themes/videotube/img/no_image_available-230x150.jpg 230w,'.home_url().'/wp-content/themes/videotube/img/no_image_available-600x440.jpg 600w'.'"';
										print '<a href="'.get_permalink(get_the_ID()).'">
											<img style="width: 100%;height: 100%;object-fit: contain;" class="img-responsive wp-post-image" loading="lazy" src="" alt=""
											srcset='.$no_image.'></a>';
									?>
									</div> 
								</div>
							</div>
						</div>
					</div> -->
				</div>

			<?php } ?>
		</div><!-- /.row -->
	</div><!-- /.container -->
<?php get_footer();?>