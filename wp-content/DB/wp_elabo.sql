-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th5 05, 2021 lúc 01:01 PM
-- Phiên bản máy phục vụ: 10.4.11-MariaDB
-- Phiên bản PHP: 7.2.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `wp_elabo`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `wp_commentmeta`
--

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `wp_comments`
--

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT 0,
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'comment',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2021-04-27 13:58:06', '2021-04-27 13:58:06', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href=\"https://gravatar.com\">Gravatar</a>.', 0, 'post-trashed', '', 'comment', 0, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `wp_links`
--

CREATE TABLE `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT 1,
  `link_rating` int(11) NOT NULL DEFAULT 0,
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `wp_options`
--

CREATE TABLE `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://192.168.1.157/elabo', 'yes'),
(2, 'home', 'http://192.168.1.157/elabo', 'yes'),
(3, 'blogname', 'Elabo', 'yes'),
(4, 'blogdescription', '', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'khailv@dn.paracelsoft.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', '', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '24', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:3:{i:0;s:30:\"advanced-custom-fields/acf.php\";i:1;s:51:\"featured-image-from-url/featured-image-from-url.php\";i:3;s:37:\"video-thumbnails/video-thumbnails.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', 'a:3:{i:0;s:60:\"C:\\xampp\\htdocs\\elabo/wp-content/themes/videotube/readme.txt\";i:1;s:59:\"C:\\xampp\\htdocs\\elabo/wp-content/themes/videotube/style.css\";i:2;s:0:\"\";}', 'no'),
(40, 'template', 'videotube', 'yes'),
(41, 'stylesheet', 'videotube', 'yes'),
(42, 'comment_registration', '', 'yes'),
(43, 'html_type', 'text/html', 'yes'),
(44, 'use_trackback', '0', 'yes'),
(45, 'default_role', 'subscriber', 'yes'),
(46, 'db_version', '49752', 'yes'),
(47, 'uploads_use_yearmonth_folders', '1', 'yes'),
(48, 'upload_path', '', 'yes'),
(49, 'blog_public', '1', 'yes'),
(50, 'default_link_category', '2', 'yes'),
(51, 'show_on_front', 'page', 'yes'),
(52, 'tag_base', '', 'yes'),
(53, 'show_avatars', '1', 'yes'),
(54, 'avatar_rating', 'G', 'yes'),
(55, 'upload_url_path', '', 'yes'),
(56, 'thumbnail_size_w', '150', 'yes'),
(57, 'thumbnail_size_h', '150', 'yes'),
(58, 'thumbnail_crop', '1', 'yes'),
(59, 'medium_size_w', '300', 'yes'),
(60, 'medium_size_h', '300', 'yes'),
(61, 'avatar_default', 'mystery', 'yes'),
(62, 'large_size_w', '1024', 'yes'),
(63, 'large_size_h', '1024', 'yes'),
(64, 'image_default_link_type', 'none', 'yes'),
(65, 'image_default_size', '', 'yes'),
(66, 'image_default_align', '', 'yes'),
(67, 'close_comments_for_old_posts', '', 'yes'),
(68, 'close_comments_days_old', '14', 'yes'),
(69, 'thread_comments', '1', 'yes'),
(70, 'thread_comments_depth', '5', 'yes'),
(71, 'page_comments', '', 'yes'),
(72, 'comments_per_page', '50', 'yes'),
(73, 'default_comments_page', 'newest', 'yes'),
(74, 'comment_order', 'asc', 'yes'),
(75, 'sticky_posts', 'a:0:{}', 'yes'),
(76, 'widget_categories', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(77, 'widget_text', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(78, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'uninstall_plugins', 'a:0:{}', 'no'),
(80, 'timezone_string', '', 'yes'),
(81, 'page_for_posts', '0', 'yes'),
(82, 'page_on_front', '2', 'yes'),
(83, 'default_post_format', '0', 'yes'),
(84, 'link_manager_enabled', '0', 'yes'),
(85, 'finished_splitting_shared_terms', '1', 'yes'),
(86, 'site_icon', '0', 'yes'),
(87, 'medium_large_size_w', '768', 'yes'),
(88, 'medium_large_size_h', '0', 'yes'),
(89, 'wp_page_for_privacy_policy', '3', 'yes'),
(90, 'show_comments_cookies_opt_in', '1', 'yes'),
(91, 'admin_email_lifespan', '1635083886', 'yes'),
(92, 'disallowed_keys', '', 'no'),
(93, 'comment_previously_approved', '1', 'yes'),
(94, 'auto_plugin_theme_update_emails', 'a:0:{}', 'no'),
(95, 'auto_update_core_dev', 'enabled', 'yes'),
(96, 'auto_update_core_minor', 'enabled', 'yes'),
(97, 'auto_update_core_major', 'enabled', 'yes'),
(98, 'initial_db_version', '49752', 'yes'),
(99, 'wp_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:61:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'yes'),
(100, 'fresh_site', '0', 'yes'),
(101, 'widget_search', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'widget_recent-posts', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'widget_recent-comments', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_archives', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_meta', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'sidebars_widgets', 'a:10:{s:19:\"wp_inactive_widgets\";a:0:{}s:27:\"mars-homepage-right-sidebar\";a:1:{i:0;s:21:\"mars-mainqc-widgets-3\";}s:29:\"mars-inner-page-right-sidebar\";a:1:{i:0;s:27:\"mars-relatedvideo-widgets-3\";}s:28:\"mars-featured-videos-sidebar\";a:0:{}s:24:\"mars-home-videos-sidebar\";a:3:{i:0;s:24:\"mars-mainvideo-widgets-3\";i:1;s:23:\"pts-add-video-widgets-2\";i:2;s:23:\"pts-add-video-widgets-3\";}s:30:\"mars-author-page-right-sidebar\";a:1:{i:0;s:27:\"mars-relatedvideo-widgets-4\";}s:19:\"mars-footer-sidebar\";a:0:{}s:31:\"mars-video-single-below-sidebar\";a:0:{}s:38:\"mars-post-single-below-content-sidebar\";a:0:{}s:13:\"array_version\";i:3;}', 'yes'),
(107, 'cron', 'a:6:{i:1620215887;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1620223087;a:6:{s:30:\"wp_site_health_scheduled_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"weekly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:604800;}}s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:18:\"wp_https_detection\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1620223097;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1620223099;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1620292763;a:2:{s:16:\"wpseo_ryte_fetch\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"weekly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:604800;}}s:13:\"wpseo-reindex\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}', 'yes'),
(108, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(113, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(114, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(115, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(116, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(118, 'recovery_keys', 'a:1:{s:22:\"DVHCp7hZWdjbGXfU6A1UOV\";a:2:{s:10:\"hashed_key\";s:34:\"$P$BWx5H.oarqZmTAQiBV04MhmZEsegUb0\";s:10:\"created_at\";i:1620114678;}}', 'yes'),
(119, 'theme_mods_twentytwentyone', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1619535206;s:4:\"data\";a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:3:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";}s:9:\"sidebar-2\";a:3:{i:0;s:10:\"archives-2\";i:1;s:12:\"categories-2\";i:2;s:6:\"meta-2\";}}}}', 'yes'),
(120, 'https_detection_errors', 'a:2:{s:23:\"ssl_verification_failed\";a:1:{i:0;s:24:\"SSL verification failed.\";}s:19:\"bad_response_source\";a:1:{i:0;s:55:\"It looks like the response did not come from this site.\";}}', 'yes'),
(137, 'can_compress_scripts', '1', 'no'),
(150, 'finished_updating_comment_type', '1', 'yes'),
(155, 'current_theme', 'VideoTube', 'yes'),
(156, 'theme_mods_videotube', 'a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:22:\"header_main_navigation\";i:6;}s:18:\"custom_css_post_id\";i:-1;s:16:\"background_color\";s:6:\"0a0a0a\";}', 'yes'),
(157, 'theme_switched', '', 'yes'),
(158, 'widget_mars-featuredvideo-widgets', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(159, 'widget_mars-featuredpost-widgets', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(160, 'widget_mars-mainvideo-widgets', 'a:2:{i:3;a:16:{s:5:\"title\";s:13:\"Latest Videos\";s:14:\"video_category\";s:1:\"0\";s:9:\"video_tag\";s:0:\"\";s:4:\"date\";s:0:\"\";s:5:\"today\";s:0:\"\";s:8:\"thisweek\";s:0:\"\";s:13:\"video_orderby\";s:2:\"ID\";s:11:\"video_order\";s:4:\"DESC\";s:13:\"widget_column\";s:1:\"3\";s:14:\"tablet_columns\";s:1:\"2\";s:14:\"mobile_columns\";s:1:\"2\";s:14:\"thumbnail_size\";s:0:\"\";s:11:\"video_shows\";s:2:\"12\";s:4:\"rows\";s:1:\"4\";s:4:\"auto\";s:0:\"\";s:9:\"view_more\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(161, 'widget_mars-mainpost-widgets', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(162, 'widget_mars-onebigvideo-widgets', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(163, 'widget_mars-posts-sidebar-widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(164, 'widget_mars-videos-sidebar-widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(165, 'widget_mars-keycloud-widgets', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(166, 'widget_mars-relatedblog-widgets', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(167, 'widget_mars-relatedvideo-widgets', 'a:3:{i:3;a:11:{s:5:\"title\";s:14:\"Related Videos\";s:13:\"video_orderby\";s:5:\"views\";s:11:\"video_order\";s:3:\"ASC\";s:22:\"video_filter_condition\";s:4:\"both\";s:11:\"video_shows\";s:2:\"16\";s:4:\"rows\";i:8;s:7:\"columns\";i:2;s:14:\"tablet_columns\";s:1:\"2\";s:14:\"mobile_columns\";s:1:\"2\";s:14:\"thumbnail_size\";s:0:\"\";s:4:\"auto\";s:0:\"\";}i:4;a:11:{s:5:\"title\";s:14:\"Related Videos\";s:13:\"video_orderby\";s:2:\"ID\";s:11:\"video_order\";s:3:\"ASC\";s:22:\"video_filter_condition\";s:4:\"both\";s:11:\"video_shows\";s:2:\"16\";s:4:\"rows\";i:1;s:7:\"columns\";i:1;s:14:\"tablet_columns\";s:1:\"2\";s:14:\"mobile_columns\";s:1:\"1\";s:14:\"thumbnail_size\";s:0:\"\";s:4:\"auto\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(168, 'widget_mars-subscribox-widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(169, 'widget_mars-connected-widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(170, 'widget_mars-loginform-widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(171, 'recently_activated', 'a:7:{s:37:\"translatepress-multilingual/index.php\";i:1620203422;s:24:\"wordpress-seo/wp-seo.php\";i:1620117313;s:27:\"js_composer/js_composer.php\";i:1619687682;s:35:\"redux-framework/redux-framework.php\";i:1619687139;s:74:\"Advanced-Custom-Fields-Autocomplete-master/acf-field-type-autocomplete.php\";i:1619663996;s:27:\"acf-gallery/acf-gallery.php\";i:1619663468;s:25:\"tag-groups/tag-groups.php\";i:1619663433;}', 'yes'),
(176, 'acf_version', '5.9.5', 'yes'),
(180, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(185, 'category_children', 'a:0:{}', 'yes'),
(195, '_site_transient_timeout_php_check_0b33beef925141e64ee7657dbe3ef273', '1620180512', 'no'),
(196, '_site_transient_php_check_0b33beef925141e64ee7657dbe3ef273', 'a:5:{s:19:\"recommended_version\";s:3:\"7.4\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:0;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}', 'no'),
(215, 'fifu_social', 'toggleon', 'no'),
(216, 'fifu_social_image_only', 'toggleoff', 'no'),
(217, 'fifu_lazy', 'toggleoff', 'yes'),
(218, 'fifu_reset', 'toggleoff', 'no'),
(219, 'fifu_content', 'toggleoff', 'yes'),
(220, 'fifu_content_page', 'toggleoff', 'yes'),
(221, 'fifu_content_cpt', 'toggleoff', 'yes'),
(222, 'fifu_enable_default_url', 'toggleoff', 'yes'),
(223, 'fifu_spinner_db', '1000', 'no'),
(224, 'fifu_spinner_nth', '1', 'yes'),
(225, 'fifu_fake', 'toggleon', 'no'),
(226, 'fifu_default_url', '', 'yes'),
(227, 'fifu_wc_lbox', 'toggleon', 'yes'),
(228, 'fifu_wc_zoom', 'toggleon', 'yes'),
(229, 'fifu_hide_page', 'toggleoff', 'yes'),
(230, 'fifu_hide_post', 'toggleoff', 'yes'),
(231, 'fifu_hide_cpt', 'toggleoff', 'yes'),
(232, 'fifu_get_first', 'toggleoff', 'yes'),
(233, 'fifu_pop_first', 'toggleoff', 'yes'),
(234, 'fifu_ovw_first', 'toggleoff', 'yes'),
(235, 'fifu_query_strings', 'toggleoff', 'yes'),
(236, 'fifu_confirm_delete_all', 'toggleoff', 'no'),
(237, 'fifu_run_delete_all', 'toggleoff', 'no'),
(238, 'fifu_column_height', '64', 'no'),
(239, 'fifu_decode', 'toggleoff', 'yes'),
(240, 'fifu_grid_category', 'toggleoff', 'yes'),
(241, 'fifu_auto_alt', 'toggleon', 'yes'),
(242, 'fifu_dynamic_alt', 'toggleoff', 'yes'),
(243, 'fifu_data_clean', 'toggleoff', 'no'),
(249, 'vc_save_data', 'a:28:{s:6:\"banner\";s:2:\"on\";s:3:\"ihe\";s:2:\"on\";s:11:\"price_table\";s:2:\"on\";s:13:\"advance_price\";s:2:\"on\";s:8:\"info_box\";s:2:\"on\";s:11:\"advance_btn\";s:2:\"on\";s:9:\"team_prof\";s:2:\"on\";s:11:\"info_circle\";s:2:\"on\";s:7:\"counter\";s:2:\"on\";s:8:\"flip_box\";s:2:\"on\";s:8:\"timeline\";s:2:\"on\";s:9:\"countdown\";s:2:\"on\";s:13:\"creative_link\";s:2:\"on\";s:10:\"text_typer\";s:2:\"on\";s:11:\"social_icon\";s:2:\"on\";s:5:\"popup\";s:2:\"on\";s:18:\"interactive_banner\";s:2:\"on\";s:13:\"highlight_box\";s:2:\"on\";s:9:\"info_list\";s:2:\"on\";s:12:\"google_trend\";s:2:\"on\";s:7:\"tooltip\";s:2:\"on\";s:11:\"testimonial\";s:2:\"on\";s:12:\"adv_carousel\";s:2:\"on\";s:7:\"heading\";s:2:\"on\";s:8:\"img_swap\";s:2:\"on\";s:9:\"accordion\";s:2:\"on\";s:14:\"filter_gallery\";s:2:\"on\";s:6:\"action\";s:12:\"vc_save_data\";}', 'yes'),
(253, 'recovery_mode_email_last_sent', '1620114678', 'yes'),
(263, '_site_transient_timeout_browser_195f77c180c23e3f5a8a8f38e4290186', '1620205384', 'no'),
(264, '_site_transient_browser_195f77c180c23e3f5a8a8f38e4290186', 'a:10:{s:4:\"name\";s:6:\"Safari\";s:7:\"version\";s:6:\"13.0.3\";s:8:\"platform\";s:6:\"iPhone\";s:10:\"update_url\";s:0:\"\";s:7:\"img_src\";s:0:\"\";s:11:\"img_src_ssl\";s:0:\"\";s:15:\"current_version\";s:0:\"\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:1;}', 'no'),
(271, '_transient_health-check-site-status-result', '{\"good\":\"12\",\"recommended\":\"8\",\"critical\":\"0\"}', 'yes'),
(273, '_site_transient_timeout_browser_bd0e4337ee0ab74b4f5d17d0da990a62', '1620226657', 'no'),
(274, '_site_transient_browser_bd0e4337ee0ab74b4f5d17d0da990a62', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:12:\"90.0.4430.93\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(313, 'fs_active_plugins', 'O:8:\"stdClass\":0:{}', 'yes'),
(314, 'fs_debug_mode', '', 'yes'),
(315, 'fs_accounts', 'a:6:{s:21:\"id_slug_type_path_map\";a:1:{i:3545;a:3:{s:4:\"slug\";s:10:\"tag-groups\";s:4:\"type\";s:6:\"plugin\";s:4:\"path\";s:25:\"tag-groups/tag-groups.php\";}}s:11:\"plugin_data\";a:1:{s:10:\"tag-groups\";a:16:{s:16:\"plugin_main_file\";O:8:\"stdClass\":1:{s:4:\"path\";s:25:\"tag-groups/tag-groups.php\";}s:20:\"is_network_activated\";b:0;s:17:\"install_timestamp\";i:1619662617;s:17:\"was_plugin_loaded\";b:1;s:21:\"is_plugin_new_install\";b:1;s:16:\"sdk_last_version\";N;s:11:\"sdk_version\";s:5:\"2.4.2\";s:16:\"sdk_upgrade_mode\";b:1;s:18:\"sdk_downgrade_mode\";b:0;s:19:\"plugin_last_version\";N;s:14:\"plugin_version\";s:6:\"1.41.0\";s:19:\"plugin_upgrade_mode\";b:1;s:21:\"plugin_downgrade_mode\";b:0;s:17:\"connectivity_test\";a:6:{s:12:\"is_connected\";b:1;s:4:\"host\";s:13:\"192.168.1.157\";s:9:\"server_ip\";s:13:\"192.168.1.157\";s:9:\"is_active\";b:1;s:9:\"timestamp\";i:1619662617;s:7:\"version\";s:6:\"1.41.0\";}s:15:\"prev_is_premium\";b:0;s:12:\"is_anonymous\";a:3:{s:2:\"is\";b:1;s:9:\"timestamp\";i:1619662637;s:7:\"version\";s:6:\"1.41.0\";}}}s:13:\"file_slug_map\";a:1:{s:25:\"tag-groups/tag-groups.php\";s:10:\"tag-groups\";}s:7:\"plugins\";a:1:{s:10:\"tag-groups\";O:9:\"FS_Plugin\":23:{s:16:\"parent_plugin_id\";N;s:5:\"title\";s:10:\"Tag Groups\";s:4:\"slug\";s:10:\"tag-groups\";s:12:\"premium_slug\";s:18:\"tag-groups-premium\";s:4:\"type\";s:6:\"plugin\";s:20:\"affiliate_moderation\";b:0;s:19:\"is_wp_org_compliant\";b:1;s:22:\"premium_releases_count\";N;s:4:\"file\";s:25:\"tag-groups/tag-groups.php\";s:7:\"version\";s:6:\"1.41.0\";s:11:\"auto_update\";N;s:4:\"info\";N;s:10:\"is_premium\";b:0;s:14:\"premium_suffix\";s:9:\"(Premium)\";s:7:\"is_live\";b:1;s:9:\"bundle_id\";N;s:17:\"bundle_public_key\";N;s:10:\"public_key\";s:32:\"pk_de9caa44b85150adcf7406ad2e895\";s:10:\"secret_key\";N;s:2:\"id\";s:4:\"3545\";s:7:\"updated\";N;s:7:\"created\";N;s:22:\"\0FS_Entity\0_is_updated\";b:0;}}s:9:\"unique_id\";s:32:\"c3417d4a2d1fca079fdea67ecf37d6aa\";s:13:\"admin_notices\";a:1:{s:10:\"tag-groups\";a:0:{}}}', 'yes'),
(316, 'fs_gdpr', 'a:1:{s:2:\"u1\";a:1:{s:8:\"required\";b:0;}}', 'yes'),
(317, 'fs_api_cache', 'a:0:{}', 'no'),
(320, 'tag_group_base_version', '1.41.0', 'yes'),
(321, 'tag_group_taxonomy', 'a:2:{i:0;s:8:\"post_tag\";i:1;s:9:\"video_tag\";}', 'yes'),
(322, 'tag_group_theme', 'delta', 'yes'),
(323, 'tag_group_base_first_activation_time', '1619662618', 'yes'),
(324, 'tag_group_tags_filter', 'a:1:{s:9:\"video_tag\";i:1;}', 'yes'),
(327, 'tag_group_used_transient_names', 'a:0:{}', 'yes'),
(331, 'term_groups', 'a:2:{i:0;i:0;i:1;i:1;}', 'yes'),
(332, 'term_group_positions', 'a:2:{i:0;i:0;i:1;i:1;}', 'yes'),
(333, 'term_group_labels', 'a:2:{i:0;s:12:\"not assigned\";i:1;s:9:\"Genre Tag\";}', 'yes'),
(336, 'tag_group_sample_page_id', '64', 'yes'),
(349, 'videotube', 'a:57:{s:8:\"last_tab\";s:0:\"\";s:4:\"logo\";a:5:{s:3:\"url\";s:67:\"http://192.168.1.157/elabo/wp-content/themes/videotube/img/logo.png\";s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";}s:10:\"custom_css\";s:0:\"\";s:17:\"custom_css_mobile\";s:0:\"\";s:9:\"custom_js\";s:0:\"\";s:16:\"custom_js_mobile\";s:0:\"\";s:5:\"style\";s:7:\"default\";s:12:\"style_custom\";s:0:\"\";s:12:\"color-header\";s:7:\"#ffffff\";s:15:\"body-background\";s:7:\"#FFFFFF\";s:15:\"typography-body\";s:0:\"\";s:19:\"typography-headings\";s:0:\"\";s:15:\"typography-menu\";s:0:\"\";s:12:\"color-widget\";s:7:\"#e73737\";s:17:\"color-text-widget\";s:18:\"hsl(0, 100%, 100%)\";s:23:\"color-header-navigation\";s:7:\"#4c5358\";s:28:\"color-text-header-navigation\";s:18:\"hsl(0, 100%, 100%)\";s:12:\"color-footer\";s:7:\"#111111\";s:17:\"color-footer-text\";s:7:\"#ffffff\";s:9:\"guestlike\";s:1:\"0\";s:8:\"facebook\";s:0:\"\";s:7:\"twitter\";s:0:\"\";s:11:\"google-plus\";s:0:\"\";s:9:\"instagram\";s:0:\"\";s:8:\"linkedin\";s:0:\"\";s:6:\"tumblr\";s:0:\"\";s:7:\"youtube\";s:0:\"\";s:5:\"vimeo\";s:0:\"\";s:10:\"soundcloud\";s:0:\"\";s:9:\"pinterest\";s:0:\"\";s:8:\"snapchat\";s:0:\"\";s:17:\"submit_permission\";s:1:\"0\";s:10:\"video-type\";a:1:{i:0;s:9:\"videolink\";}s:9:\"videosize\";s:2:\"10\";s:9:\"imagesize\";s:1:\"2\";s:18:\"submit_redirect_to\";s:0:\"\";s:20:\"submit_assigned_user\";s:1:\"1\";s:13:\"submit_status\";s:7:\"pending\";s:13:\"submit_editor\";s:1:\"0\";s:11:\"videolayout\";s:3:\"yes\";s:12:\"rewrite_slug\";s:5:\"video\";s:21:\"rewrite_slug_category\";s:10:\"categories\";s:16:\"rewrite_slug_tag\";s:9:\"video_tag\";s:9:\"loginpage\";s:0:\"\";s:10:\"video_feed\";s:0:\"\";s:15:\"datetime_format\";s:9:\"videotube\";s:12:\"aspect_ratio\";s:5:\"16by9\";s:8:\"autoplay\";s:1:\"1\";s:18:\"enable_channelpage\";s:1:\"0\";s:14:\"read_more_less\";s:1:\"1\";s:15:\"desktop_columns\";s:1:\"3\";s:14:\"tablet_columns\";s:1:\"2\";s:14:\"mobile_columns\";s:1:\"2\";s:14:\"copyright_text\";s:92:\"<p>Copyright 2015 By MarsTheme All rights reserved. Powered by WordPress &amp; MarsTheme</p>\";s:13:\"purchase_code\";s:0:\"\";s:12:\"access_token\";s:0:\"\";s:16:\"defaults-section\";s:13:\"Reset Section\";}', 'yes'),
(350, 'videotube-transients', 'a:5:{s:14:\"changed_values\";a:10:{s:12:\"color-header\";s:7:\"#FFFFFF\";s:15:\"body-background\";a:7:{s:16:\"background-color\";s:0:\"\";s:17:\"background-repeat\";s:0:\"\";s:15:\"background-size\";s:0:\"\";s:21:\"background-attachment\";s:0:\"\";s:19:\"background-position\";s:0:\"\";s:16:\"background-image\";s:0:\"\";s:5:\"media\";a:4:{s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";}}s:15:\"typography-body\";a:3:{s:11:\"font-family\";s:0:\"\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";}s:19:\"typography-headings\";a:3:{s:11:\"font-family\";s:0:\"\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";}s:15:\"typography-menu\";a:3:{s:11:\"font-family\";s:0:\"\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";}s:12:\"color-widget\";s:7:\"#84FF3D\";s:17:\"color-text-widget\";s:0:\"\";s:23:\"color-header-navigation\";s:7:\"#4C5358\";s:28:\"color-text-header-navigation\";s:0:\"\";s:17:\"color-footer-text\";s:7:\"#FFFFFF\";}s:9:\"last_save\";i:1619686955;s:13:\"last_compiler\";i:1619686955;s:11:\"last_import\";i:1619686955;s:7:\"notices\";a:2:{s:6:\"errors\";a:0:{}s:8:\"sanitize\";a:0:{}}}', 'yes'),
(352, 'video_thumbnails', 'a:5:{s:10:\"save_media\";i:1;s:12:\"set_featured\";i:1;s:10:\"post_types\";a:1:{i:0;s:4:\"post\";}s:12:\"custom_field\";s:0:\"\";s:7:\"version\";s:6:\"2.12.3\";}', 'yes'),
(357, 'vc_version', '5.4.4', 'yes'),
(359, 'wpb_js_composer_license_activation_notified', 'yes', 'yes'),
(360, 'redux-framework_allow_tracking', 'no', 'yes'),
(386, 'video_tag_children', 'a:3:{i:34;a:5:{i:0;i:9;i:1;i:14;i:2;i:15;i:3;i:17;i:4;i:21;}i:33;a:6:{i:0;i:10;i:1;i:11;i:2;i:12;i:3;i:13;i:4;i:18;i:5;i:19;}i:35;a:2:{i:0;i:16;i:1;i:20;}}', 'yes'),
(416, 'widget_mars-mainvideoqc-widgets', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(417, 'widget_hstngr_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(426, 'categories_children', 'a:0:{}', 'yes'),
(431, 'yoast_migrations_free', 'a:1:{s:7:\"version\";s:4:\"16.2\";}', 'yes'),
(432, 'wpseo', 'a:44:{s:8:\"tracking\";b:0;s:22:\"license_server_version\";b:0;s:15:\"ms_defaults_set\";b:0;s:40:\"ignore_search_engines_discouraged_notice\";b:0;s:19:\"indexing_first_time\";b:0;s:16:\"indexing_started\";b:0;s:15:\"indexing_reason\";s:13:\"first_install\";s:29:\"indexables_indexing_completed\";b:1;s:7:\"version\";s:4:\"16.2\";s:16:\"previous_version\";s:0:\"\";s:20:\"disableadvanced_meta\";b:1;s:30:\"enable_headless_rest_endpoints\";b:1;s:17:\"ryte_indexability\";b:1;s:11:\"baiduverify\";s:0:\"\";s:12:\"googleverify\";s:0:\"\";s:8:\"msverify\";s:0:\"\";s:12:\"yandexverify\";s:0:\"\";s:9:\"site_type\";s:0:\"\";s:20:\"has_multiple_authors\";s:0:\"\";s:16:\"environment_type\";s:0:\"\";s:23:\"content_analysis_active\";b:1;s:23:\"keyword_analysis_active\";b:1;s:21:\"enable_admin_bar_menu\";b:1;s:26:\"enable_cornerstone_content\";b:1;s:18:\"enable_xml_sitemap\";b:1;s:24:\"enable_text_link_counter\";b:1;s:22:\"show_onboarding_notice\";b:1;s:18:\"first_activated_on\";i:1619687963;s:13:\"myyoast-oauth\";b:0;s:26:\"semrush_integration_active\";b:1;s:14:\"semrush_tokens\";a:0:{}s:20:\"semrush_country_code\";s:2:\"us\";s:19:\"permalink_structure\";s:12:\"/%postname%/\";s:8:\"home_url\";s:26:\"http://192.168.1.157/elabo\";s:18:\"dynamic_permalinks\";b:0;s:17:\"category_base_url\";s:0:\"\";s:12:\"tag_base_url\";s:0:\"\";s:21:\"custom_taxonomy_slugs\";a:2:{s:10:\"categories\";s:10:\"categories\";s:9:\"video_tag\";s:9:\"video_tag\";}s:29:\"enable_enhanced_slack_sharing\";b:1;s:25:\"zapier_integration_active\";b:0;s:19:\"zapier_subscription\";a:0:{}s:14:\"zapier_api_key\";s:0:\"\";s:23:\"enable_metabox_insights\";b:1;s:23:\"enable_link_suggestions\";b:1;}', 'yes'),
(433, 'wpseo_titles', 'a:144:{s:17:\"forcerewritetitle\";b:0;s:9:\"separator\";s:7:\"sc-dash\";s:16:\"title-home-wpseo\";s:42:\"%%sitename%% %%page%% %%sep%% %%sitedesc%%\";s:18:\"title-author-wpseo\";s:41:\"%%name%%, Author at %%sitename%% %%page%%\";s:19:\"title-archive-wpseo\";s:38:\"%%date%% %%page%% %%sep%% %%sitename%%\";s:18:\"title-search-wpseo\";s:63:\"You searched for %%searchphrase%% %%page%% %%sep%% %%sitename%%\";s:15:\"title-404-wpseo\";s:35:\"Page not found %%sep%% %%sitename%%\";s:25:\"social-title-author-wpseo\";s:9:\"%%title%%\";s:26:\"social-title-archive-wpseo\";s:9:\"%%title%%\";s:31:\"social-description-author-wpseo\";s:11:\"%%excerpt%%\";s:32:\"social-description-archive-wpseo\";s:11:\"%%excerpt%%\";s:29:\"social-image-url-author-wpseo\";s:0:\"\";s:30:\"social-image-url-archive-wpseo\";s:0:\"\";s:28:\"social-image-id-author-wpseo\";s:0:\"\";s:29:\"social-image-id-archive-wpseo\";s:0:\"\";s:19:\"metadesc-home-wpseo\";s:0:\"\";s:21:\"metadesc-author-wpseo\";s:0:\"\";s:22:\"metadesc-archive-wpseo\";s:0:\"\";s:9:\"rssbefore\";s:0:\"\";s:8:\"rssafter\";s:53:\"The post %%POSTLINK%% appeared first on %%BLOGLINK%%.\";s:20:\"noindex-author-wpseo\";b:0;s:28:\"noindex-author-noposts-wpseo\";b:1;s:21:\"noindex-archive-wpseo\";b:1;s:14:\"disable-author\";b:0;s:12:\"disable-date\";b:0;s:19:\"disable-post_format\";b:0;s:18:\"disable-attachment\";b:1;s:23:\"is-media-purge-relevant\";b:0;s:20:\"breadcrumbs-404crumb\";s:25:\"Error 404: Page not found\";s:29:\"breadcrumbs-display-blog-page\";b:1;s:20:\"breadcrumbs-boldlast\";b:0;s:25:\"breadcrumbs-archiveprefix\";s:12:\"Archives for\";s:18:\"breadcrumbs-enable\";b:1;s:16:\"breadcrumbs-home\";s:4:\"Home\";s:18:\"breadcrumbs-prefix\";s:0:\"\";s:24:\"breadcrumbs-searchprefix\";s:16:\"You searched for\";s:15:\"breadcrumbs-sep\";s:7:\"&raquo;\";s:12:\"website_name\";s:0:\"\";s:11:\"person_name\";s:0:\"\";s:11:\"person_logo\";s:0:\"\";s:22:\"alternate_website_name\";s:0:\"\";s:12:\"company_logo\";s:0:\"\";s:12:\"company_name\";s:0:\"\";s:17:\"company_or_person\";s:7:\"company\";s:25:\"company_or_person_user_id\";b:0;s:17:\"stripcategorybase\";b:0;s:10:\"title-post\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:13:\"metadesc-post\";s:0:\"\";s:12:\"noindex-post\";b:0;s:23:\"display-metabox-pt-post\";b:1;s:23:\"post_types-post-maintax\";i:0;s:21:\"schema-page-type-post\";s:7:\"WebPage\";s:24:\"schema-article-type-post\";s:7:\"Article\";s:17:\"social-title-post\";s:9:\"%%title%%\";s:23:\"social-description-post\";s:11:\"%%excerpt%%\";s:21:\"social-image-url-post\";s:0:\"\";s:20:\"social-image-id-post\";s:0:\"\";s:10:\"title-page\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:13:\"metadesc-page\";s:0:\"\";s:12:\"noindex-page\";b:0;s:23:\"display-metabox-pt-page\";b:1;s:23:\"post_types-page-maintax\";i:0;s:21:\"schema-page-type-page\";s:7:\"WebPage\";s:24:\"schema-article-type-page\";s:4:\"None\";s:17:\"social-title-page\";s:9:\"%%title%%\";s:23:\"social-description-page\";s:11:\"%%excerpt%%\";s:21:\"social-image-url-page\";s:0:\"\";s:20:\"social-image-id-page\";s:0:\"\";s:16:\"title-attachment\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:19:\"metadesc-attachment\";s:0:\"\";s:18:\"noindex-attachment\";b:0;s:29:\"display-metabox-pt-attachment\";b:1;s:29:\"post_types-attachment-maintax\";i:0;s:27:\"schema-page-type-attachment\";s:7:\"WebPage\";s:30:\"schema-article-type-attachment\";s:4:\"None\";s:23:\"social-title-attachment\";s:9:\"%%title%%\";s:29:\"social-description-attachment\";s:11:\"%%excerpt%%\";s:27:\"social-image-url-attachment\";s:0:\"\";s:26:\"social-image-id-attachment\";s:0:\"\";s:11:\"title-video\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:14:\"metadesc-video\";s:0:\"\";s:13:\"noindex-video\";b:0;s:24:\"display-metabox-pt-video\";b:1;s:24:\"post_types-video-maintax\";i:0;s:22:\"schema-page-type-video\";s:7:\"WebPage\";s:25:\"schema-article-type-video\";s:4:\"None\";s:18:\"social-title-video\";s:9:\"%%title%%\";s:24:\"social-description-video\";s:11:\"%%excerpt%%\";s:22:\"social-image-url-video\";s:0:\"\";s:21:\"social-image-id-video\";s:0:\"\";s:21:\"title-ptarchive-video\";s:51:\"%%pt_plural%% Archive %%page%% %%sep%% %%sitename%%\";s:24:\"metadesc-ptarchive-video\";s:0:\"\";s:23:\"bctitle-ptarchive-video\";s:0:\"\";s:23:\"noindex-ptarchive-video\";b:0;s:28:\"social-title-ptarchive-video\";s:9:\"%%title%%\";s:34:\"social-description-ptarchive-video\";s:11:\"%%excerpt%%\";s:32:\"social-image-url-ptarchive-video\";s:0:\"\";s:31:\"social-image-id-ptarchive-video\";s:0:\"\";s:18:\"title-tax-category\";s:53:\"%%term_title%% Archives %%page%% %%sep%% %%sitename%%\";s:21:\"metadesc-tax-category\";s:0:\"\";s:28:\"display-metabox-tax-category\";b:1;s:20:\"noindex-tax-category\";b:0;s:25:\"social-title-tax-category\";s:9:\"%%title%%\";s:31:\"social-description-tax-category\";s:11:\"%%excerpt%%\";s:29:\"social-image-url-tax-category\";s:0:\"\";s:28:\"social-image-id-tax-category\";s:0:\"\";s:18:\"title-tax-post_tag\";s:53:\"%%term_title%% Archives %%page%% %%sep%% %%sitename%%\";s:21:\"metadesc-tax-post_tag\";s:0:\"\";s:28:\"display-metabox-tax-post_tag\";b:1;s:20:\"noindex-tax-post_tag\";b:0;s:25:\"social-title-tax-post_tag\";s:9:\"%%title%%\";s:31:\"social-description-tax-post_tag\";s:11:\"%%excerpt%%\";s:29:\"social-image-url-tax-post_tag\";s:0:\"\";s:28:\"social-image-id-tax-post_tag\";s:0:\"\";s:21:\"title-tax-post_format\";s:53:\"%%term_title%% Archives %%page%% %%sep%% %%sitename%%\";s:24:\"metadesc-tax-post_format\";s:0:\"\";s:31:\"display-metabox-tax-post_format\";b:1;s:23:\"noindex-tax-post_format\";b:1;s:28:\"social-title-tax-post_format\";s:9:\"%%title%%\";s:34:\"social-description-tax-post_format\";s:11:\"%%excerpt%%\";s:32:\"social-image-url-tax-post_format\";s:0:\"\";s:31:\"social-image-id-tax-post_format\";s:0:\"\";s:20:\"title-tax-categories\";s:53:\"%%term_title%% Archives %%page%% %%sep%% %%sitename%%\";s:23:\"metadesc-tax-categories\";s:0:\"\";s:30:\"display-metabox-tax-categories\";b:1;s:22:\"noindex-tax-categories\";b:0;s:27:\"social-title-tax-categories\";s:9:\"%%title%%\";s:33:\"social-description-tax-categories\";s:11:\"%%excerpt%%\";s:31:\"social-image-url-tax-categories\";s:0:\"\";s:30:\"social-image-id-tax-categories\";s:0:\"\";s:28:\"taxonomy-categories-ptparent\";i:0;s:19:\"title-tax-video_tag\";s:53:\"%%term_title%% Archives %%page%% %%sep%% %%sitename%%\";s:22:\"metadesc-tax-video_tag\";s:0:\"\";s:29:\"display-metabox-tax-video_tag\";b:1;s:21:\"noindex-tax-video_tag\";b:0;s:26:\"social-title-tax-video_tag\";s:9:\"%%title%%\";s:32:\"social-description-tax-video_tag\";s:11:\"%%excerpt%%\";s:30:\"social-image-url-tax-video_tag\";s:0:\"\";s:29:\"social-image-id-tax-video_tag\";s:0:\"\";s:27:\"taxonomy-video_tag-ptparent\";i:0;s:14:\"person_logo_id\";i:0;s:15:\"company_logo_id\";i:0;s:17:\"company_logo_meta\";b:0;s:16:\"person_logo_meta\";b:0;}', 'yes'),
(434, 'wpseo_social', 'a:18:{s:13:\"facebook_site\";s:0:\"\";s:13:\"instagram_url\";s:0:\"\";s:12:\"linkedin_url\";s:0:\"\";s:11:\"myspace_url\";s:0:\"\";s:16:\"og_default_image\";s:0:\"\";s:19:\"og_default_image_id\";s:0:\"\";s:18:\"og_frontpage_title\";s:0:\"\";s:17:\"og_frontpage_desc\";s:0:\"\";s:18:\"og_frontpage_image\";s:0:\"\";s:21:\"og_frontpage_image_id\";s:0:\"\";s:9:\"opengraph\";b:1;s:13:\"pinterest_url\";s:0:\"\";s:15:\"pinterestverify\";s:0:\"\";s:7:\"twitter\";b:1;s:12:\"twitter_site\";s:0:\"\";s:17:\"twitter_card_type\";s:19:\"summary_large_image\";s:11:\"youtube_url\";s:0:\"\";s:13:\"wikipedia_url\";s:0:\"\";}', 'yes'),
(448, 'wpseo_ryte', 'a:2:{s:6:\"status\";i:-1;s:10:\"last_fetch\";i:1619687969;}', 'yes'),
(464, 'WPLANG', '', 'yes'),
(465, 'new_admin_email', 'khailv@dn.paracelsoft.com', 'yes'),
(491, '_transient_timeout_wpseo_unindexed_post_link_count', '1620184127', 'no'),
(492, '_transient_wpseo_unindexed_post_link_count', '0', 'no'),
(501, 'widget_mars-mainqc-widgets', 'a:3:{i:2;a:6:{s:5:\"title\";s:0:\"\";s:4:\"rows\";i:1;s:7:\"columns\";i:1;s:14:\"tablet_columns\";s:1:\"1\";s:14:\"mobile_columns\";s:1:\"1\";s:14:\"thumbnail_size\";s:0:\"\";}i:3;a:6:{s:5:\"title\";s:0:\"\";s:4:\"rows\";i:1;s:7:\"columns\";i:1;s:14:\"tablet_columns\";s:1:\"1\";s:14:\"mobile_columns\";s:1:\"1\";s:14:\"thumbnail_size\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(505, '_transient_timeout_wpseo_total_unindexed_posts', '1620187847', 'no'),
(506, '_transient_wpseo_total_unindexed_posts', '0', 'no'),
(507, '_transient_timeout_wpseo_total_unindexed_terms', '1620187847', 'no'),
(508, '_transient_wpseo_total_unindexed_terms', '0', 'no'),
(509, '_transient_timeout_wpseo_total_unindexed_post_type_archives', '1620187847', 'no'),
(510, '_transient_wpseo_total_unindexed_post_type_archives', '0', 'no'),
(511, '_transient_timeout_wpseo_unindexed_term_link_count', '1620187847', 'no'),
(512, '_transient_wpseo_unindexed_term_link_count', '0', 'no'),
(526, 'widget_mars-uncensoredjav-video-widgets', 'a:3:{i:2;a:15:{s:5:\"title\";s:13:\"Latest Videos\";s:14:\"video_category\";s:1:\"0\";s:4:\"date\";s:0:\"\";s:5:\"today\";s:0:\"\";s:8:\"thisweek\";s:0:\"\";s:13:\"video_orderby\";s:2:\"ID\";s:11:\"video_order\";s:4:\"DESC\";s:13:\"widget_column\";s:1:\"6\";s:14:\"tablet_columns\";s:1:\"2\";s:14:\"mobile_columns\";s:1:\"1\";s:14:\"thumbnail_size\";s:0:\"\";s:11:\"video_shows\";s:2:\"16\";s:4:\"rows\";s:1:\"1\";s:4:\"auto\";s:0:\"\";s:9:\"view_more\";s:0:\"\";}i:3;a:15:{s:5:\"title\";s:13:\"Latest Videos\";s:14:\"video_category\";s:0:\"\";s:4:\"date\";s:0:\"\";s:5:\"today\";s:0:\"\";s:8:\"thisweek\";s:0:\"\";s:13:\"video_orderby\";s:2:\"ID\";s:11:\"video_order\";s:4:\"DESC\";s:13:\"widget_column\";s:1:\"6\";s:14:\"tablet_columns\";s:1:\"2\";s:14:\"mobile_columns\";s:1:\"1\";s:14:\"thumbnail_size\";s:0:\"\";s:11:\"video_shows\";s:2:\"16\";s:4:\"rows\";s:1:\"1\";s:4:\"auto\";s:0:\"\";s:9:\"view_more\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(527, 'widget_pts-add-video-widgets', 'a:3:{i:2;a:15:{s:5:\"title\";s:12:\"Censored JAV\";s:14:\"video_category\";s:2:\"23\";s:4:\"date\";s:0:\"\";s:5:\"today\";s:0:\"\";s:8:\"thisweek\";s:0:\"\";s:13:\"video_orderby\";s:2:\"ID\";s:11:\"video_order\";s:4:\"DESC\";s:13:\"widget_column\";s:1:\"3\";s:14:\"tablet_columns\";s:1:\"2\";s:14:\"mobile_columns\";s:1:\"2\";s:14:\"thumbnail_size\";s:0:\"\";s:11:\"video_shows\";s:2:\"12\";s:4:\"rows\";s:1:\"4\";s:4:\"auto\";s:0:\"\";s:9:\"view_more\";s:0:\"\";}i:3;a:15:{s:5:\"title\";s:7:\"Popular\";s:14:\"video_category\";s:2:\"22\";s:4:\"date\";s:0:\"\";s:5:\"today\";s:0:\"\";s:8:\"thisweek\";s:0:\"\";s:13:\"video_orderby\";s:2:\"ID\";s:11:\"video_order\";s:4:\"DESC\";s:13:\"widget_column\";s:1:\"3\";s:14:\"tablet_columns\";s:1:\"2\";s:14:\"mobile_columns\";s:1:\"2\";s:14:\"thumbnail_size\";s:0:\"\";s:11:\"video_shows\";s:2:\"12\";s:4:\"rows\";s:1:\"4\";s:4:\"auto\";s:0:\"\";s:9:\"view_more\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(544, 'rewrite_rules', 'a:142:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:17:\"^wp-sitemap\\.xml$\";s:23:\"index.php?sitemap=index\";s:17:\"^wp-sitemap\\.xsl$\";s:36:\"index.php?sitemap-stylesheet=sitemap\";s:23:\"^wp-sitemap-index\\.xsl$\";s:34:\"index.php?sitemap-stylesheet=index\";s:48:\"^wp-sitemap-([a-z]+?)-([a-z\\d_-]+?)-(\\d+?)\\.xml$\";s:75:\"index.php?sitemap=$matches[1]&sitemap-subtype=$matches[2]&paged=$matches[3]\";s:34:\"^wp-sitemap-([a-z]+?)-(\\d+?)\\.xml$\";s:47:\"index.php?sitemap=$matches[1]&paged=$matches[2]\";s:8:\"video/?$\";s:25:\"index.php?post_type=video\";s:38:\"video/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?post_type=video&feed=$matches[1]\";s:33:\"video/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?post_type=video&feed=$matches[1]\";s:25:\"video/page/([0-9]{1,})/?$\";s:43:\"index.php?post_type=video&paged=$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:45:\"language_switcher/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:55:\"language_switcher/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:75:\"language_switcher/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:70:\"language_switcher/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:70:\"language_switcher/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:51:\"language_switcher/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:34:\"language_switcher/([^/]+)/embed/?$\";s:50:\"index.php?language_switcher=$matches[1]&embed=true\";s:38:\"language_switcher/([^/]+)/trackback/?$\";s:44:\"index.php?language_switcher=$matches[1]&tb=1\";s:46:\"language_switcher/([^/]+)/page/?([0-9]{1,})/?$\";s:57:\"index.php?language_switcher=$matches[1]&paged=$matches[2]\";s:53:\"language_switcher/([^/]+)/comment-page-([0-9]{1,})/?$\";s:57:\"index.php?language_switcher=$matches[1]&cpage=$matches[2]\";s:42:\"language_switcher/([^/]+)(?:/([0-9]+))?/?$\";s:56:\"index.php?language_switcher=$matches[1]&page=$matches[2]\";s:34:\"language_switcher/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:44:\"language_switcher/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:64:\"language_switcher/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:59:\"language_switcher/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:59:\"language_switcher/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:40:\"language_switcher/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:33:\"video/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:43:\"video/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:63:\"video/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:58:\"video/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:58:\"video/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:39:\"video/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:22:\"video/([^/]+)/embed/?$\";s:38:\"index.php?video=$matches[1]&embed=true\";s:26:\"video/([^/]+)/trackback/?$\";s:32:\"index.php?video=$matches[1]&tb=1\";s:46:\"video/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?video=$matches[1]&feed=$matches[2]\";s:41:\"video/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?video=$matches[1]&feed=$matches[2]\";s:34:\"video/([^/]+)/page/?([0-9]{1,})/?$\";s:45:\"index.php?video=$matches[1]&paged=$matches[2]\";s:41:\"video/([^/]+)/comment-page-([0-9]{1,})/?$\";s:45:\"index.php?video=$matches[1]&cpage=$matches[2]\";s:30:\"video/([^/]+)(?:/([0-9]+))?/?$\";s:44:\"index.php?video=$matches[1]&page=$matches[2]\";s:22:\"video/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:32:\"video/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:52:\"video/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:47:\"video/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:47:\"video/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:28:\"video/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:51:\"categories/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?categories=$matches[1]&feed=$matches[2]\";s:46:\"categories/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?categories=$matches[1]&feed=$matches[2]\";s:27:\"categories/([^/]+)/embed/?$\";s:43:\"index.php?categories=$matches[1]&embed=true\";s:39:\"categories/([^/]+)/page/?([0-9]{1,})/?$\";s:50:\"index.php?categories=$matches[1]&paged=$matches[2]\";s:21:\"categories/([^/]+)/?$\";s:32:\"index.php?categories=$matches[1]\";s:50:\"video_tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:48:\"index.php?video_tag=$matches[1]&feed=$matches[2]\";s:45:\"video_tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:48:\"index.php?video_tag=$matches[1]&feed=$matches[2]\";s:26:\"video_tag/([^/]+)/embed/?$\";s:42:\"index.php?video_tag=$matches[1]&embed=true\";s:38:\"video_tag/([^/]+)/page/?([0-9]{1,})/?$\";s:49:\"index.php?video_tag=$matches[1]&paged=$matches[2]\";s:20:\"video_tag/([^/]+)/?$\";s:31:\"index.php?video_tag=$matches[1]\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:38:\"index.php?&page_id=2&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}', 'yes'),
(569, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.7.1.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.7.1.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.7.1-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.7.1-new-bundled.zip\";s:7:\"partial\";s:0:\"\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:5:\"5.7.1\";s:7:\"version\";s:5:\"5.7.1\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.6\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1620190609;s:15:\"version_checked\";s:5:\"5.7.1\";s:12:\"translations\";a:0:{}}', 'no'),
(570, '_site_transient_update_themes', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1620190610;s:7:\"checked\";a:4:{s:14:\"twentynineteen\";s:3:\"2.0\";s:12:\"twentytwenty\";s:3:\"1.7\";s:15:\"twentytwentyone\";s:3:\"1.3\";s:9:\"videotube\";s:5:\"3.0.8\";}s:8:\"response\";a:0:{}s:9:\"no_update\";a:3:{s:14:\"twentynineteen\";a:6:{s:5:\"theme\";s:14:\"twentynineteen\";s:11:\"new_version\";s:3:\"2.0\";s:3:\"url\";s:44:\"https://wordpress.org/themes/twentynineteen/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/theme/twentynineteen.2.0.zip\";s:8:\"requires\";s:5:\"4.9.6\";s:12:\"requires_php\";s:5:\"5.2.4\";}s:12:\"twentytwenty\";a:6:{s:5:\"theme\";s:12:\"twentytwenty\";s:11:\"new_version\";s:3:\"1.7\";s:3:\"url\";s:42:\"https://wordpress.org/themes/twentytwenty/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/theme/twentytwenty.1.7.zip\";s:8:\"requires\";s:3:\"4.7\";s:12:\"requires_php\";s:5:\"5.2.4\";}s:15:\"twentytwentyone\";a:6:{s:5:\"theme\";s:15:\"twentytwentyone\";s:11:\"new_version\";s:3:\"1.3\";s:3:\"url\";s:45:\"https://wordpress.org/themes/twentytwentyone/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/theme/twentytwentyone.1.3.zip\";s:8:\"requires\";s:3:\"5.3\";s:12:\"requires_php\";s:3:\"5.6\";}}s:12:\"translations\";a:3:{i:0;a:7:{s:4:\"type\";s:5:\"theme\";s:4:\"slug\";s:14:\"twentynineteen\";s:8:\"language\";s:2:\"ja\";s:7:\"version\";s:3:\"2.0\";s:7:\"updated\";s:19:\"2021-04-15 06:03:55\";s:7:\"package\";s:75:\"https://downloads.wordpress.org/translation/theme/twentynineteen/2.0/ja.zip\";s:10:\"autoupdate\";b:1;}i:1;a:7:{s:4:\"type\";s:5:\"theme\";s:4:\"slug\";s:12:\"twentytwenty\";s:8:\"language\";s:2:\"ja\";s:7:\"version\";s:3:\"1.7\";s:7:\"updated\";s:19:\"2021-04-19 21:47:43\";s:7:\"package\";s:73:\"https://downloads.wordpress.org/translation/theme/twentytwenty/1.7/ja.zip\";s:10:\"autoupdate\";b:1;}i:2;a:7:{s:4:\"type\";s:5:\"theme\";s:4:\"slug\";s:15:\"twentytwentyone\";s:8:\"language\";s:2:\"ja\";s:7:\"version\";s:3:\"1.0\";s:7:\"updated\";s:19:\"2021-03-11 02:03:13\";s:7:\"package\";s:76:\"https://downloads.wordpress.org/translation/theme/twentytwentyone/1.0/ja.zip\";s:10:\"autoupdate\";b:1;}}}', 'no'),
(573, '_site_transient_timeout_poptags_40cd750bba9870f18aada2478b24840a', '1620201277', 'no'),
(574, '_site_transient_poptags_40cd750bba9870f18aada2478b24840a', 'O:8:\"stdClass\":100:{s:11:\"woocommerce\";a:3:{s:4:\"name\";s:11:\"woocommerce\";s:4:\"slug\";s:11:\"woocommerce\";s:5:\"count\";i:4865;}s:6:\"widget\";a:3:{s:4:\"name\";s:6:\"widget\";s:4:\"slug\";s:6:\"widget\";s:5:\"count\";i:4762;}s:4:\"post\";a:3:{s:4:\"name\";s:4:\"post\";s:4:\"slug\";s:4:\"post\";s:5:\"count\";i:2716;}s:5:\"admin\";a:3:{s:4:\"name\";s:5:\"admin\";s:4:\"slug\";s:5:\"admin\";s:5:\"count\";i:2590;}s:5:\"posts\";a:3:{s:4:\"name\";s:5:\"posts\";s:4:\"slug\";s:5:\"posts\";s:5:\"count\";i:1998;}s:9:\"shortcode\";a:3:{s:4:\"name\";s:9:\"shortcode\";s:4:\"slug\";s:9:\"shortcode\";s:5:\"count\";i:1855;}s:8:\"comments\";a:3:{s:4:\"name\";s:8:\"comments\";s:4:\"slug\";s:8:\"comments\";s:5:\"count\";i:1831;}s:6:\"images\";a:3:{s:4:\"name\";s:6:\"images\";s:4:\"slug\";s:6:\"images\";s:5:\"count\";i:1511;}s:6:\"google\";a:3:{s:4:\"name\";s:6:\"google\";s:4:\"slug\";s:6:\"google\";s:5:\"count\";i:1506;}s:7:\"twitter\";a:3:{s:4:\"name\";s:7:\"twitter\";s:4:\"slug\";s:7:\"twitter\";s:5:\"count\";i:1497;}s:3:\"seo\";a:3:{s:4:\"name\";s:3:\"seo\";s:4:\"slug\";s:3:\"seo\";s:5:\"count\";i:1488;}s:5:\"image\";a:3:{s:4:\"name\";s:5:\"image\";s:4:\"slug\";s:5:\"image\";s:5:\"count\";i:1480;}s:8:\"facebook\";a:3:{s:4:\"name\";s:8:\"facebook\";s:4:\"slug\";s:8:\"facebook\";s:5:\"count\";i:1468;}s:7:\"sidebar\";a:3:{s:4:\"name\";s:7:\"sidebar\";s:4:\"slug\";s:7:\"sidebar\";s:5:\"count\";i:1310;}s:5:\"email\";a:3:{s:4:\"name\";s:5:\"email\";s:4:\"slug\";s:5:\"email\";s:5:\"count\";i:1255;}s:9:\"ecommerce\";a:3:{s:4:\"name\";s:9:\"ecommerce\";s:4:\"slug\";s:9:\"ecommerce\";s:5:\"count\";i:1243;}s:7:\"gallery\";a:3:{s:4:\"name\";s:7:\"gallery\";s:4:\"slug\";s:7:\"gallery\";s:5:\"count\";i:1227;}s:4:\"page\";a:3:{s:4:\"name\";s:4:\"page\";s:4:\"slug\";s:4:\"page\";s:5:\"count\";i:1152;}s:6:\"social\";a:3:{s:4:\"name\";s:6:\"social\";s:4:\"slug\";s:6:\"social\";s:5:\"count\";i:1124;}s:5:\"login\";a:3:{s:4:\"name\";s:5:\"login\";s:4:\"slug\";s:5:\"login\";s:5:\"count\";i:1051;}s:8:\"security\";a:3:{s:4:\"name\";s:8:\"security\";s:4:\"slug\";s:8:\"security\";s:5:\"count\";i:946;}s:5:\"video\";a:3:{s:4:\"name\";s:5:\"video\";s:4:\"slug\";s:5:\"video\";s:5:\"count\";i:927;}s:7:\"widgets\";a:3:{s:4:\"name\";s:7:\"widgets\";s:4:\"slug\";s:7:\"widgets\";s:5:\"count\";i:904;}s:5:\"links\";a:3:{s:4:\"name\";s:5:\"links\";s:4:\"slug\";s:5:\"links\";s:5:\"count\";i:886;}s:10:\"e-commerce\";a:3:{s:4:\"name\";s:10:\"e-commerce\";s:4:\"slug\";s:10:\"e-commerce\";s:5:\"count\";i:876;}s:4:\"spam\";a:3:{s:4:\"name\";s:4:\"spam\";s:4:\"slug\";s:4:\"spam\";s:5:\"count\";i:821;}s:6:\"slider\";a:3:{s:4:\"name\";s:6:\"slider\";s:4:\"slug\";s:6:\"slider\";s:5:\"count\";i:816;}s:7:\"content\";a:3:{s:4:\"name\";s:7:\"content\";s:4:\"slug\";s:7:\"content\";s:5:\"count\";i:805;}s:9:\"analytics\";a:3:{s:4:\"name\";s:9:\"analytics\";s:4:\"slug\";s:9:\"analytics\";s:5:\"count\";i:803;}s:4:\"form\";a:3:{s:4:\"name\";s:4:\"form\";s:4:\"slug\";s:4:\"form\";s:5:\"count\";i:780;}s:10:\"buddypress\";a:3:{s:4:\"name\";s:10:\"buddypress\";s:4:\"slug\";s:10:\"buddypress\";s:5:\"count\";i:761;}s:5:\"media\";a:3:{s:4:\"name\";s:5:\"media\";s:4:\"slug\";s:5:\"media\";s:5:\"count\";i:743;}s:3:\"rss\";a:3:{s:4:\"name\";s:3:\"rss\";s:4:\"slug\";s:3:\"rss\";s:5:\"count\";i:727;}s:6:\"search\";a:3:{s:4:\"name\";s:6:\"search\";s:4:\"slug\";s:6:\"search\";s:5:\"count\";i:725;}s:5:\"pages\";a:3:{s:4:\"name\";s:5:\"pages\";s:4:\"slug\";s:5:\"pages\";s:5:\"count\";i:712;}s:6:\"editor\";a:3:{s:4:\"name\";s:6:\"editor\";s:4:\"slug\";s:6:\"editor\";s:5:\"count\";i:711;}s:4:\"menu\";a:3:{s:4:\"name\";s:4:\"menu\";s:4:\"slug\";s:4:\"menu\";s:5:\"count\";i:678;}s:7:\"payment\";a:3:{s:4:\"name\";s:7:\"payment\";s:4:\"slug\";s:7:\"payment\";s:5:\"count\";i:670;}s:6:\"jquery\";a:3:{s:4:\"name\";s:6:\"jquery\";s:4:\"slug\";s:6:\"jquery\";s:5:\"count\";i:664;}s:4:\"feed\";a:3:{s:4:\"name\";s:4:\"feed\";s:4:\"slug\";s:4:\"feed\";s:5:\"count\";i:663;}s:12:\"contact-form\";a:3:{s:4:\"name\";s:12:\"contact form\";s:4:\"slug\";s:12:\"contact-form\";s:5:\"count\";i:661;}s:8:\"category\";a:3:{s:4:\"name\";s:8:\"category\";s:4:\"slug\";s:8:\"category\";s:5:\"count\";i:659;}s:5:\"embed\";a:3:{s:4:\"name\";s:5:\"embed\";s:4:\"slug\";s:5:\"embed\";s:5:\"count\";i:652;}s:4:\"ajax\";a:3:{s:4:\"name\";s:4:\"ajax\";s:4:\"slug\";s:4:\"ajax\";s:5:\"count\";i:646;}s:9:\"gutenberg\";a:3:{s:4:\"name\";s:9:\"gutenberg\";s:4:\"slug\";s:9:\"gutenberg\";s:5:\"count\";i:627;}s:15:\"payment-gateway\";a:3:{s:4:\"name\";s:15:\"payment gateway\";s:4:\"slug\";s:15:\"payment-gateway\";s:5:\"count\";i:599;}s:7:\"youtube\";a:3:{s:4:\"name\";s:7:\"youtube\";s:4:\"slug\";s:7:\"youtube\";s:5:\"count\";i:598;}s:3:\"css\";a:3:{s:4:\"name\";s:3:\"css\";s:4:\"slug\";s:3:\"css\";s:5:\"count\";i:595;}s:4:\"link\";a:3:{s:4:\"name\";s:4:\"link\";s:4:\"slug\";s:4:\"link\";s:5:\"count\";i:588;}s:10:\"javascript\";a:3:{s:4:\"name\";s:10:\"javascript\";s:4:\"slug\";s:10:\"javascript\";s:5:\"count\";i:588;}s:9:\"affiliate\";a:3:{s:4:\"name\";s:9:\"affiliate\";s:4:\"slug\";s:9:\"affiliate\";s:5:\"count\";i:576;}s:5:\"share\";a:3:{s:4:\"name\";s:5:\"share\";s:4:\"slug\";s:5:\"share\";s:5:\"count\";i:571;}s:10:\"responsive\";a:3:{s:4:\"name\";s:10:\"responsive\";s:4:\"slug\";s:10:\"responsive\";s:5:\"count\";i:564;}s:5:\"theme\";a:3:{s:4:\"name\";s:5:\"theme\";s:4:\"slug\";s:5:\"theme\";s:5:\"count\";i:561;}s:7:\"comment\";a:3:{s:4:\"name\";s:7:\"comment\";s:4:\"slug\";s:7:\"comment\";s:5:\"count\";i:557;}s:9:\"dashboard\";a:3:{s:4:\"name\";s:9:\"dashboard\";s:4:\"slug\";s:9:\"dashboard\";s:5:\"count\";i:557;}s:3:\"ads\";a:3:{s:4:\"name\";s:3:\"ads\";s:4:\"slug\";s:3:\"ads\";s:5:\"count\";i:544;}s:7:\"contact\";a:3:{s:4:\"name\";s:7:\"contact\";s:4:\"slug\";s:7:\"contact\";s:5:\"count\";i:543;}s:3:\"api\";a:3:{s:4:\"name\";s:3:\"api\";s:4:\"slug\";s:3:\"api\";s:5:\"count\";i:542;}s:6:\"custom\";a:3:{s:4:\"name\";s:6:\"custom\";s:4:\"slug\";s:6:\"custom\";s:5:\"count\";i:539;}s:10:\"categories\";a:3:{s:4:\"name\";s:10:\"categories\";s:4:\"slug\";s:10:\"categories\";s:5:\"count\";i:530;}s:4:\"user\";a:3:{s:4:\"name\";s:4:\"user\";s:4:\"slug\";s:4:\"user\";s:5:\"count\";i:517;}s:6:\"button\";a:3:{s:4:\"name\";s:6:\"button\";s:4:\"slug\";s:6:\"button\";s:5:\"count\";i:510;}s:6:\"events\";a:3:{s:4:\"name\";s:6:\"events\";s:4:\"slug\";s:6:\"events\";s:5:\"count\";i:501;}s:4:\"tags\";a:3:{s:4:\"name\";s:4:\"tags\";s:4:\"slug\";s:4:\"tags\";s:5:\"count\";i:499;}s:9:\"elementor\";a:3:{s:4:\"name\";s:9:\"elementor\";s:4:\"slug\";s:9:\"elementor\";s:5:\"count\";i:499;}s:6:\"mobile\";a:3:{s:4:\"name\";s:6:\"mobile\";s:4:\"slug\";s:6:\"mobile\";s:5:\"count\";i:491;}s:5:\"block\";a:3:{s:4:\"name\";s:5:\"block\";s:4:\"slug\";s:5:\"block\";s:5:\"count\";i:491;}s:5:\"users\";a:3:{s:4:\"name\";s:5:\"users\";s:4:\"slug\";s:5:\"users\";s:5:\"count\";i:488;}s:9:\"marketing\";a:3:{s:4:\"name\";s:9:\"marketing\";s:4:\"slug\";s:9:\"marketing\";s:5:\"count\";i:486;}s:4:\"chat\";a:3:{s:4:\"name\";s:4:\"chat\";s:4:\"slug\";s:4:\"chat\";s:5:\"count\";i:473;}s:5:\"popup\";a:3:{s:4:\"name\";s:5:\"popup\";s:4:\"slug\";s:5:\"popup\";s:5:\"count\";i:464;}s:8:\"calendar\";a:3:{s:4:\"name\";s:8:\"calendar\";s:4:\"slug\";s:8:\"calendar\";s:5:\"count\";i:458;}s:5:\"forms\";a:3:{s:4:\"name\";s:5:\"forms\";s:4:\"slug\";s:5:\"forms\";s:5:\"count\";i:454;}s:14:\"contact-form-7\";a:3:{s:4:\"name\";s:14:\"contact form 7\";s:4:\"slug\";s:14:\"contact-form-7\";s:5:\"count\";i:446;}s:5:\"photo\";a:3:{s:4:\"name\";s:5:\"photo\";s:4:\"slug\";s:5:\"photo\";s:5:\"count\";i:445;}s:10:\"navigation\";a:3:{s:4:\"name\";s:10:\"navigation\";s:4:\"slug\";s:10:\"navigation\";s:5:\"count\";i:444;}s:9:\"slideshow\";a:3:{s:4:\"name\";s:9:\"slideshow\";s:4:\"slug\";s:9:\"slideshow\";s:5:\"count\";i:443;}s:10:\"newsletter\";a:3:{s:4:\"name\";s:10:\"newsletter\";s:4:\"slug\";s:10:\"newsletter\";s:5:\"count\";i:441;}s:8:\"shipping\";a:3:{s:4:\"name\";s:8:\"shipping\";s:4:\"slug\";s:8:\"shipping\";s:5:\"count\";i:437;}s:5:\"stats\";a:3:{s:4:\"name\";s:5:\"stats\";s:4:\"slug\";s:5:\"stats\";s:5:\"count\";i:432;}s:6:\"photos\";a:3:{s:4:\"name\";s:6:\"photos\";s:4:\"slug\";s:6:\"photos\";s:5:\"count\";i:424;}s:10:\"statistics\";a:3:{s:4:\"name\";s:10:\"statistics\";s:4:\"slug\";s:10:\"statistics\";s:5:\"count\";i:419;}s:12:\"social-media\";a:3:{s:4:\"name\";s:12:\"social media\";s:4:\"slug\";s:12:\"social-media\";s:5:\"count\";i:409;}s:11:\"performance\";a:3:{s:4:\"name\";s:11:\"performance\";s:4:\"slug\";s:11:\"performance\";s:5:\"count\";i:407;}s:8:\"redirect\";a:3:{s:4:\"name\";s:8:\"redirect\";s:4:\"slug\";s:8:\"redirect\";s:5:\"count\";i:406;}s:4:\"news\";a:3:{s:4:\"name\";s:4:\"news\";s:4:\"slug\";s:4:\"news\";s:5:\"count\";i:406;}s:10:\"shortcodes\";a:3:{s:4:\"name\";s:10:\"shortcodes\";s:4:\"slug\";s:10:\"shortcodes\";s:5:\"count\";i:400;}s:12:\"notification\";a:3:{s:4:\"name\";s:12:\"notification\";s:4:\"slug\";s:12:\"notification\";s:5:\"count\";i:391;}s:4:\"code\";a:3:{s:4:\"name\";s:4:\"code\";s:4:\"slug\";s:4:\"code\";s:5:\"count\";i:390;}s:7:\"plugins\";a:3:{s:4:\"name\";s:7:\"plugins\";s:4:\"slug\";s:7:\"plugins\";s:5:\"count\";i:388;}s:9:\"multisite\";a:3:{s:4:\"name\";s:9:\"multisite\";s:4:\"slug\";s:9:\"multisite\";s:5:\"count\";i:380;}s:3:\"url\";a:3:{s:4:\"name\";s:3:\"url\";s:4:\"slug\";s:3:\"url\";s:5:\"count\";i:379;}s:8:\"tracking\";a:3:{s:4:\"name\";s:8:\"tracking\";s:4:\"slug\";s:8:\"tracking\";s:5:\"count\";i:372;}s:4:\"meta\";a:3:{s:4:\"name\";s:4:\"meta\";s:4:\"slug\";s:4:\"meta\";s:5:\"count\";i:371;}s:4:\"list\";a:3:{s:4:\"name\";s:4:\"list\";s:4:\"slug\";s:4:\"list\";s:5:\"count\";i:364;}s:6:\"import\";a:3:{s:4:\"name\";s:6:\"import\";s:4:\"slug\";s:6:\"import\";s:5:\"count\";i:363;}s:16:\"google-analytics\";a:3:{s:4:\"name\";s:16:\"google analytics\";s:4:\"slug\";s:16:\"google-analytics\";s:5:\"count\";i:355;}s:5:\"cache\";a:3:{s:4:\"name\";s:5:\"cache\";s:4:\"slug\";s:5:\"cache\";s:5:\"count\";i:354;}s:16:\"custom-post-type\";a:3:{s:4:\"name\";s:16:\"custom post type\";s:4:\"slug\";s:16:\"custom-post-type\";s:5:\"count\";i:347;}}', 'no'),
(578, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1620203185;s:7:\"checked\";a:8:{s:30:\"advanced-custom-fields/acf.php\";s:5:\"5.9.5\";s:19:\"akismet/akismet.php\";s:5:\"4.1.9\";s:51:\"featured-image-from-url/featured-image-from-url.php\";s:5:\"3.5.7\";s:9:\"hello.php\";s:5:\"1.7.2\";s:35:\"redux-framework/redux-framework.php\";s:6:\"4.1.26\";s:37:\"translatepress-multilingual/index.php\";s:5:\"2.0.0\";s:37:\"video-thumbnails/video-thumbnails.php\";s:6:\"2.12.3\";s:27:\"js_composer/js_composer.php\";s:5:\"5.4.4\";}s:8:\"response\";a:1:{s:51:\"featured-image-from-url/featured-image-from-url.php\";O:8:\"stdClass\":13:{s:2:\"id\";s:37:\"w.org/plugins/featured-image-from-url\";s:4:\"slug\";s:23:\"featured-image-from-url\";s:6:\"plugin\";s:51:\"featured-image-from-url/featured-image-from-url.php\";s:11:\"new_version\";s:5:\"3.5.8\";s:3:\"url\";s:54:\"https://wordpress.org/plugins/featured-image-from-url/\";s:7:\"package\";s:72:\"https://downloads.wordpress.org/plugin/featured-image-from-url.3.5.8.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:76:\"https://ps.w.org/featured-image-from-url/assets/icon-256x256.png?rev=2384757\";s:2:\"1x\";s:76:\"https://ps.w.org/featured-image-from-url/assets/icon-128x128.png?rev=2384757\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:79:\"https://ps.w.org/featured-image-from-url/assets/banner-1544x500.png?rev=1694895\";s:2:\"1x\";s:78:\"https://ps.w.org/featured-image-from-url/assets/banner-772x250.png?rev=1694895\";}s:11:\"banners_rtl\";a:0:{}s:14:\"upgrade_notice\";s:132:\"<ul>\n<li>New feature: Auto set featured image using web page address; new feature: Auto set screenshot as featured image.</li>\n</ul>\";s:6:\"tested\";s:5:\"5.7.1\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}}s:12:\"translations\";a:3:{i:0;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:22:\"advanced-custom-fields\";s:8:\"language\";s:2:\"ja\";s:7:\"version\";s:5:\"5.9.5\";s:7:\"updated\";s:19:\"2021-01-26 03:48:53\";s:7:\"package\";s:86:\"https://downloads.wordpress.org/translation/plugin/advanced-custom-fields/5.9.5/ja.zip\";s:10:\"autoupdate\";b:1;}i:1;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:7:\"akismet\";s:8:\"language\";s:2:\"ja\";s:7:\"version\";s:5:\"4.1.9\";s:7:\"updated\";s:19:\"2020-10-22 19:38:27\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/translation/plugin/akismet/4.1.9/ja.zip\";s:10:\"autoupdate\";b:1;}i:2;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:11:\"hello-dolly\";s:8:\"language\";s:2:\"ja\";s:7:\"version\";s:5:\"1.7.2\";s:7:\"updated\";s:19:\"2020-10-30 07:03:00\";s:7:\"package\";s:75:\"https://downloads.wordpress.org/translation/plugin/hello-dolly/1.7.2/ja.zip\";s:10:\"autoupdate\";b:1;}}s:9:\"no_update\";a:6:{s:30:\"advanced-custom-fields/acf.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:36:\"w.org/plugins/advanced-custom-fields\";s:4:\"slug\";s:22:\"advanced-custom-fields\";s:6:\"plugin\";s:30:\"advanced-custom-fields/acf.php\";s:11:\"new_version\";s:5:\"5.9.5\";s:3:\"url\";s:53:\"https://wordpress.org/plugins/advanced-custom-fields/\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/plugin/advanced-custom-fields.5.9.5.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png?rev=1082746\";s:2:\"1x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-128x128.png?rev=1082746\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";s:2:\"1x\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";}s:11:\"banners_rtl\";a:0:{}}s:19:\"akismet/akismet.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:5:\"4.1.9\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/akismet.4.1.9.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}}s:9:\"hello.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:9:\"hello.php\";s:11:\"new_version\";s:5:\"1.7.2\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/hello-dolly.1.7.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=2052855\";s:2:\"1x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=2052855\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:66:\"https://ps.w.org/hello-dolly/assets/banner-772x250.jpg?rev=2052855\";}s:11:\"banners_rtl\";a:0:{}}s:35:\"redux-framework/redux-framework.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:29:\"w.org/plugins/redux-framework\";s:4:\"slug\";s:15:\"redux-framework\";s:6:\"plugin\";s:35:\"redux-framework/redux-framework.php\";s:11:\"new_version\";s:6:\"4.1.26\";s:3:\"url\";s:46:\"https://wordpress.org/plugins/redux-framework/\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/plugin/redux-framework.4.1.26.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:68:\"https://ps.w.org/redux-framework/assets/icon-256x256.png?rev=2352112\";s:2:\"1x\";s:59:\"https://ps.w.org/redux-framework/assets/icon.svg?rev=995554\";s:3:\"svg\";s:59:\"https://ps.w.org/redux-framework/assets/icon.svg?rev=995554\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:71:\"https://ps.w.org/redux-framework/assets/banner-1544x500.png?rev=2352114\";s:2:\"1x\";s:70:\"https://ps.w.org/redux-framework/assets/banner-772x250.png?rev=2352114\";}s:11:\"banners_rtl\";a:0:{}}s:37:\"translatepress-multilingual/index.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:41:\"w.org/plugins/translatepress-multilingual\";s:4:\"slug\";s:27:\"translatepress-multilingual\";s:6:\"plugin\";s:37:\"translatepress-multilingual/index.php\";s:11:\"new_version\";s:5:\"2.0.0\";s:3:\"url\";s:58:\"https://wordpress.org/plugins/translatepress-multilingual/\";s:7:\"package\";s:76:\"https://downloads.wordpress.org/plugin/translatepress-multilingual.2.0.0.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:80:\"https://ps.w.org/translatepress-multilingual/assets/icon-256x256.png?rev=1722670\";s:2:\"1x\";s:80:\"https://ps.w.org/translatepress-multilingual/assets/icon-128x128.png?rev=1722670\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:83:\"https://ps.w.org/translatepress-multilingual/assets/banner-1544x500.png?rev=2312348\";s:2:\"1x\";s:82:\"https://ps.w.org/translatepress-multilingual/assets/banner-772x250.png?rev=2312348\";}s:11:\"banners_rtl\";a:0:{}}s:37:\"video-thumbnails/video-thumbnails.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:30:\"w.org/plugins/video-thumbnails\";s:4:\"slug\";s:16:\"video-thumbnails\";s:6:\"plugin\";s:37:\"video-thumbnails/video-thumbnails.php\";s:11:\"new_version\";s:6:\"2.12.3\";s:3:\"url\";s:47:\"https://wordpress.org/plugins/video-thumbnails/\";s:7:\"package\";s:66:\"https://downloads.wordpress.org/plugin/video-thumbnails.2.12.3.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/video-thumbnails/assets/icon-256x256.png?rev=1111154\";s:2:\"1x\";s:69:\"https://ps.w.org/video-thumbnails/assets/icon-128x128.png?rev=1111154\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:71:\"https://ps.w.org/video-thumbnails/assets/banner-1544x500.png?rev=840515\";s:2:\"1x\";s:70:\"https://ps.w.org/video-thumbnails/assets/banner-772x250.png?rev=840515\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no'),
(579, 'trp_settings', 'a:14:{s:16:\"default-language\";s:2:\"ja\";s:17:\"publish-languages\";a:2:{i:0;s:2:\"ja\";i:1;s:5:\"en_US\";}s:21:\"translation-languages\";a:2:{i:0;s:2:\"ja\";i:1;s:5:\"en_US\";}s:9:\"url-slugs\";a:2:{s:2:\"ja\";s:2:\"ja\";s:5:\"en_US\";s:2:\"en\";}s:22:\"native_or_english_name\";s:12:\"english_name\";s:36:\"add-subdirectory-to-default-language\";s:2:\"no\";s:30:\"force-language-to-custom-links\";s:3:\"yes\";s:17:\"shortcode-options\";s:16:\"flags-full-names\";s:12:\"menu-options\";s:16:\"flags-full-names\";s:14:\"trp-ls-floater\";s:3:\"yes\";s:15:\"floater-options\";s:16:\"flags-full-names\";s:13:\"floater-color\";s:4:\"dark\";s:16:\"floater-position\";s:12:\"bottom-right\";s:21:\"trp-ls-show-poweredby\";s:2:\"no\";}', 'yes'),
(580, 'trp_db_stored_data', 'a:1:{s:17:\"install_timestamp\";i:1620190615;}', 'yes'),
(583, 'trp_plugin_version', '2.0.0', 'yes'),
(584, '_transient_timeout_trp_checked_if_site_meets_conditions_for_review', '1620277016', 'no'),
(585, '_transient_trp_checked_if_site_meets_conditions_for_review', 'yes', 'no'),
(592, '_site_transient_timeout_available_translations', '1620213987', 'no');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(593, '_site_transient_available_translations', 'a:126:{s:2:\"af\";a:8:{s:8:\"language\";s:2:\"af\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-02-23 11:41:38\";s:12:\"english_name\";s:9:\"Afrikaans\";s:11:\"native_name\";s:9:\"Afrikaans\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.7.1/af.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"af\";i:2;s:3:\"afr\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Gaan voort\";}}s:2:\"ar\";a:8:{s:8:\"language\";s:2:\"ar\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-21 20:16:59\";s:12:\"english_name\";s:6:\"Arabic\";s:11:\"native_name\";s:14:\"العربية\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.7.1/ar.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ar\";i:2;s:3:\"ara\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"المتابعة\";}}s:3:\"ary\";a:8:{s:8:\"language\";s:3:\"ary\";s:7:\"version\";s:6:\"4.8.16\";s:7:\"updated\";s:19:\"2017-01-26 15:42:35\";s:12:\"english_name\";s:15:\"Moroccan Arabic\";s:11:\"native_name\";s:31:\"العربية المغربية\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/translation/core/4.8.16/ary.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ar\";i:3;s:3:\"ary\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"المتابعة\";}}s:2:\"as\";a:8:{s:8:\"language\";s:2:\"as\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-05-04 13:19:36\";s:12:\"english_name\";s:8:\"Assamese\";s:11:\"native_name\";s:21:\"অসমীয়া\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.7.1/as.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"as\";i:2;s:3:\"asm\";i:3;s:3:\"asm\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:2:\"az\";a:8:{s:8:\"language\";s:2:\"az\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-06 00:09:27\";s:12:\"english_name\";s:11:\"Azerbaijani\";s:11:\"native_name\";s:16:\"Azərbaycan dili\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/az.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"az\";i:2;s:3:\"aze\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:5:\"Davam\";}}s:3:\"azb\";a:8:{s:8:\"language\";s:3:\"azb\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-09-12 20:34:31\";s:12:\"english_name\";s:17:\"South Azerbaijani\";s:11:\"native_name\";s:29:\"گؤنئی آذربایجان\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/azb.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"az\";i:3;s:3:\"azb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:3:\"bel\";a:8:{s:8:\"language\";s:3:\"bel\";s:7:\"version\";s:6:\"4.9.17\";s:7:\"updated\";s:19:\"2019-10-29 07:54:22\";s:12:\"english_name\";s:10:\"Belarusian\";s:11:\"native_name\";s:29:\"Беларуская мова\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/translation/core/4.9.17/bel.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"be\";i:2;s:3:\"bel\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Працягнуць\";}}s:5:\"bg_BG\";a:8:{s:8:\"language\";s:5:\"bg_BG\";s:7:\"version\";s:5:\"5.4.5\";s:7:\"updated\";s:19:\"2020-07-01 06:36:01\";s:12:\"english_name\";s:9:\"Bulgarian\";s:11:\"native_name\";s:18:\"Български\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.5/bg_BG.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"bg\";i:2;s:3:\"bul\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:22:\"Продължение\";}}s:5:\"bn_BD\";a:8:{s:8:\"language\";s:5:\"bn_BD\";s:7:\"version\";s:5:\"5.4.5\";s:7:\"updated\";s:19:\"2020-10-31 08:48:37\";s:12:\"english_name\";s:20:\"Bengali (Bangladesh)\";s:11:\"native_name\";s:15:\"বাংলা\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.5/bn_BD.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"bn\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:23:\"এগিয়ে চল.\";}}s:2:\"bo\";a:8:{s:8:\"language\";s:2:\"bo\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2020-10-30 03:24:38\";s:12:\"english_name\";s:7:\"Tibetan\";s:11:\"native_name\";s:21:\"བོད་ཡིག\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.7.1/bo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"bo\";i:2;s:3:\"tib\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:24:\"མུ་མཐུད།\";}}s:5:\"bs_BA\";a:8:{s:8:\"language\";s:5:\"bs_BA\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-25 07:27:37\";s:12:\"english_name\";s:7:\"Bosnian\";s:11:\"native_name\";s:8:\"Bosanski\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.1/bs_BA.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"bs\";i:2;s:3:\"bos\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Nastavi\";}}s:2:\"ca\";a:8:{s:8:\"language\";s:2:\"ca\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-15 04:10:23\";s:12:\"english_name\";s:7:\"Catalan\";s:11:\"native_name\";s:7:\"Català\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.7.1/ca.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ca\";i:2;s:3:\"cat\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continua\";}}s:3:\"ceb\";a:8:{s:8:\"language\";s:3:\"ceb\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-03-02 17:25:51\";s:12:\"english_name\";s:7:\"Cebuano\";s:11:\"native_name\";s:7:\"Cebuano\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/ceb.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"ceb\";i:3;s:3:\"ceb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Padayun\";}}s:5:\"cs_CZ\";a:8:{s:8:\"language\";s:5:\"cs_CZ\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-19 06:51:04\";s:12:\"english_name\";s:5:\"Czech\";s:11:\"native_name\";s:9:\"Čeština\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.1/cs_CZ.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"cs\";i:2;s:3:\"ces\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:11:\"Pokračovat\";}}s:2:\"cy\";a:8:{s:8:\"language\";s:2:\"cy\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-15 10:32:41\";s:12:\"english_name\";s:5:\"Welsh\";s:11:\"native_name\";s:7:\"Cymraeg\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.7.1/cy.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"cy\";i:2;s:3:\"cym\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Parhau\";}}s:5:\"da_DK\";a:8:{s:8:\"language\";s:5:\"da_DK\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-20 06:42:13\";s:12:\"english_name\";s:6:\"Danish\";s:11:\"native_name\";s:5:\"Dansk\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.1/da_DK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"da\";i:2;s:3:\"dan\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Forts&#230;t\";}}s:5:\"de_DE\";a:8:{s:8:\"language\";s:5:\"de_DE\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-15 04:41:08\";s:12:\"english_name\";s:6:\"German\";s:11:\"native_name\";s:7:\"Deutsch\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.1/de_DE.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Fortfahren\";}}s:5:\"de_AT\";a:8:{s:8:\"language\";s:5:\"de_AT\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-03-10 19:15:18\";s:12:\"english_name\";s:16:\"German (Austria)\";s:11:\"native_name\";s:21:\"Deutsch (Österreich)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.1/de_AT.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Weiter\";}}s:14:\"de_CH_informal\";a:8:{s:8:\"language\";s:14:\"de_CH_informal\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-03-14 20:06:52\";s:12:\"english_name\";s:30:\"German (Switzerland, Informal)\";s:11:\"native_name\";s:21:\"Deutsch (Schweiz, Du)\";s:7:\"package\";s:73:\"https://downloads.wordpress.org/translation/core/5.7.1/de_CH_informal.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Weiter\";}}s:5:\"de_CH\";a:8:{s:8:\"language\";s:5:\"de_CH\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-03-14 20:06:23\";s:12:\"english_name\";s:20:\"German (Switzerland)\";s:11:\"native_name\";s:17:\"Deutsch (Schweiz)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.1/de_CH.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Fortfahren\";}}s:12:\"de_DE_formal\";a:8:{s:8:\"language\";s:12:\"de_DE_formal\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-15 04:43:41\";s:12:\"english_name\";s:15:\"German (Formal)\";s:11:\"native_name\";s:13:\"Deutsch (Sie)\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/translation/core/5.7.1/de_DE_formal.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Fortfahren\";}}s:3:\"dsb\";a:8:{s:8:\"language\";s:3:\"dsb\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-15 13:33:04\";s:12:\"english_name\";s:13:\"Lower Sorbian\";s:11:\"native_name\";s:16:\"Dolnoserbšćina\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.7.1/dsb.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"dsb\";i:3;s:3:\"dsb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:5:\"Dalej\";}}s:3:\"dzo\";a:8:{s:8:\"language\";s:3:\"dzo\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-06-29 08:59:03\";s:12:\"english_name\";s:8:\"Dzongkha\";s:11:\"native_name\";s:18:\"རྫོང་ཁ\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/dzo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"dz\";i:2;s:3:\"dzo\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:2:\"el\";a:8:{s:8:\"language\";s:2:\"el\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-27 18:07:44\";s:12:\"english_name\";s:5:\"Greek\";s:11:\"native_name\";s:16:\"Ελληνικά\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.7.1/el.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"el\";i:2;s:3:\"ell\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"Συνέχεια\";}}s:5:\"en_GB\";a:8:{s:8:\"language\";s:5:\"en_GB\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-15 07:31:22\";s:12:\"english_name\";s:12:\"English (UK)\";s:11:\"native_name\";s:12:\"English (UK)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.1/en_GB.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_CA\";a:8:{s:8:\"language\";s:5:\"en_CA\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-15 04:12:51\";s:12:\"english_name\";s:16:\"English (Canada)\";s:11:\"native_name\";s:16:\"English (Canada)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.1/en_CA.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_ZA\";a:8:{s:8:\"language\";s:5:\"en_ZA\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-15 07:22:30\";s:12:\"english_name\";s:22:\"English (South Africa)\";s:11:\"native_name\";s:22:\"English (South Africa)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.1/en_ZA.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_AU\";a:8:{s:8:\"language\";s:5:\"en_AU\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-15 04:12:40\";s:12:\"english_name\";s:19:\"English (Australia)\";s:11:\"native_name\";s:19:\"English (Australia)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.1/en_AU.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_NZ\";a:8:{s:8:\"language\";s:5:\"en_NZ\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-15 04:12:28\";s:12:\"english_name\";s:21:\"English (New Zealand)\";s:11:\"native_name\";s:21:\"English (New Zealand)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.1/en_NZ.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:2:\"eo\";a:8:{s:8:\"language\";s:2:\"eo\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-18 09:35:35\";s:12:\"english_name\";s:9:\"Esperanto\";s:11:\"native_name\";s:9:\"Esperanto\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.7.1/eo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"eo\";i:2;s:3:\"epo\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Daŭrigi\";}}s:5:\"es_ES\";a:8:{s:8:\"language\";s:5:\"es_ES\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-24 16:46:30\";s:12:\"english_name\";s:15:\"Spanish (Spain)\";s:11:\"native_name\";s:8:\"Español\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.1/es_ES.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_CO\";a:8:{s:8:\"language\";s:5:\"es_CO\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-03-11 17:28:23\";s:12:\"english_name\";s:18:\"Spanish (Colombia)\";s:11:\"native_name\";s:20:\"Español de Colombia\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.1/es_CO.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_CR\";a:8:{s:8:\"language\";s:5:\"es_CR\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-03-03 15:32:45\";s:12:\"english_name\";s:20:\"Spanish (Costa Rica)\";s:11:\"native_name\";s:22:\"Español de Costa Rica\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.1/es_CR.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_CL\";a:8:{s:8:\"language\";s:5:\"es_CL\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-03-10 15:04:44\";s:12:\"english_name\";s:15:\"Spanish (Chile)\";s:11:\"native_name\";s:17:\"Español de Chile\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.1/es_CL.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_EC\";a:8:{s:8:\"language\";s:5:\"es_EC\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-15 02:05:34\";s:12:\"english_name\";s:17:\"Spanish (Ecuador)\";s:11:\"native_name\";s:19:\"Español de Ecuador\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.1/es_EC.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_VE\";a:8:{s:8:\"language\";s:5:\"es_VE\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-15 02:05:15\";s:12:\"english_name\";s:19:\"Spanish (Venezuela)\";s:11:\"native_name\";s:21:\"Español de Venezuela\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.1/es_VE.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_UY\";a:8:{s:8:\"language\";s:5:\"es_UY\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-03-31 18:33:26\";s:12:\"english_name\";s:17:\"Spanish (Uruguay)\";s:11:\"native_name\";s:19:\"Español de Uruguay\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.1/es_UY.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_PE\";a:8:{s:8:\"language\";s:5:\"es_PE\";s:7:\"version\";s:5:\"5.6.3\";s:7:\"updated\";s:19:\"2020-12-11 02:12:59\";s:12:\"english_name\";s:14:\"Spanish (Peru)\";s:11:\"native_name\";s:17:\"Español de Perú\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.6.3/es_PE.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_PR\";a:8:{s:8:\"language\";s:5:\"es_PR\";s:7:\"version\";s:5:\"5.4.5\";s:7:\"updated\";s:19:\"2020-04-29 15:36:59\";s:12:\"english_name\";s:21:\"Spanish (Puerto Rico)\";s:11:\"native_name\";s:23:\"Español de Puerto Rico\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.5/es_PR.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_GT\";a:8:{s:8:\"language\";s:5:\"es_GT\";s:7:\"version\";s:6:\"5.2.10\";s:7:\"updated\";s:19:\"2019-03-02 06:35:01\";s:12:\"english_name\";s:19:\"Spanish (Guatemala)\";s:11:\"native_name\";s:21:\"Español de Guatemala\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/5.2.10/es_GT.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_MX\";a:8:{s:8:\"language\";s:5:\"es_MX\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-16 13:07:32\";s:12:\"english_name\";s:16:\"Spanish (Mexico)\";s:11:\"native_name\";s:19:\"Español de México\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.1/es_MX.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_AR\";a:8:{s:8:\"language\";s:5:\"es_AR\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-16 02:17:21\";s:12:\"english_name\";s:19:\"Spanish (Argentina)\";s:11:\"native_name\";s:21:\"Español de Argentina\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.1/es_AR.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:2:\"et\";a:8:{s:8:\"language\";s:2:\"et\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2020-08-12 08:38:59\";s:12:\"english_name\";s:8:\"Estonian\";s:11:\"native_name\";s:5:\"Eesti\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.7.1/et.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"et\";i:2;s:3:\"est\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Jätka\";}}s:2:\"eu\";a:8:{s:8:\"language\";s:2:\"eu\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-18 11:35:03\";s:12:\"english_name\";s:6:\"Basque\";s:11:\"native_name\";s:7:\"Euskara\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.7.1/eu.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"eu\";i:2;s:3:\"eus\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Jarraitu\";}}s:5:\"fa_AF\";a:8:{s:8:\"language\";s:5:\"fa_AF\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-05-04 18:27:22\";s:12:\"english_name\";s:21:\"Persian (Afghanistan)\";s:11:\"native_name\";s:31:\"(فارسی (افغانستان\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.1/fa_AF.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fa\";i:2;s:3:\"fas\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"ادامه\";}}s:5:\"fa_IR\";a:8:{s:8:\"language\";s:5:\"fa_IR\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-01 11:39:36\";s:12:\"english_name\";s:7:\"Persian\";s:11:\"native_name\";s:10:\"فارسی\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.1/fa_IR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fa\";i:2;s:3:\"fas\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"ادامه\";}}s:2:\"fi\";a:8:{s:8:\"language\";s:2:\"fi\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-27 17:53:38\";s:12:\"english_name\";s:7:\"Finnish\";s:11:\"native_name\";s:5:\"Suomi\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.7.1/fi.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fi\";i:2;s:3:\"fin\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:5:\"Jatka\";}}s:5:\"fr_BE\";a:8:{s:8:\"language\";s:5:\"fr_BE\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-02-22 13:54:46\";s:12:\"english_name\";s:16:\"French (Belgium)\";s:11:\"native_name\";s:21:\"Français de Belgique\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.1/fr_BE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fr\";i:2;s:3:\"fra\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuer\";}}s:5:\"fr_FR\";a:8:{s:8:\"language\";s:5:\"fr_FR\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-28 18:07:16\";s:12:\"english_name\";s:15:\"French (France)\";s:11:\"native_name\";s:9:\"Français\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.1/fr_FR.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"fr\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuer\";}}s:5:\"fr_CA\";a:8:{s:8:\"language\";s:5:\"fr_CA\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-30 13:29:35\";s:12:\"english_name\";s:15:\"French (Canada)\";s:11:\"native_name\";s:19:\"Français du Canada\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.1/fr_CA.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fr\";i:2;s:3:\"fra\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuer\";}}s:3:\"fur\";a:8:{s:8:\"language\";s:3:\"fur\";s:7:\"version\";s:6:\"4.8.16\";s:7:\"updated\";s:19:\"2018-01-29 17:32:35\";s:12:\"english_name\";s:8:\"Friulian\";s:11:\"native_name\";s:8:\"Friulian\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/translation/core/4.8.16/fur.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"fur\";i:3;s:3:\"fur\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:2:\"gd\";a:8:{s:8:\"language\";s:2:\"gd\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-08-23 17:41:37\";s:12:\"english_name\";s:15:\"Scottish Gaelic\";s:11:\"native_name\";s:9:\"Gàidhlig\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/gd.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"gd\";i:2;s:3:\"gla\";i:3;s:3:\"gla\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:15:\"Lean air adhart\";}}s:5:\"gl_ES\";a:8:{s:8:\"language\";s:5:\"gl_ES\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-15 18:17:43\";s:12:\"english_name\";s:8:\"Galician\";s:11:\"native_name\";s:6:\"Galego\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.1/gl_ES.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"gl\";i:2;s:3:\"glg\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:2:\"gu\";a:8:{s:8:\"language\";s:2:\"gu\";s:7:\"version\";s:6:\"4.9.17\";s:7:\"updated\";s:19:\"2018-09-14 12:33:48\";s:12:\"english_name\";s:8:\"Gujarati\";s:11:\"native_name\";s:21:\"ગુજરાતી\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.9.17/gu.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"gu\";i:2;s:3:\"guj\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:31:\"ચાલુ રાખવું\";}}s:3:\"haz\";a:8:{s:8:\"language\";s:3:\"haz\";s:7:\"version\";s:6:\"4.4.24\";s:7:\"updated\";s:19:\"2015-12-05 00:59:09\";s:12:\"english_name\";s:8:\"Hazaragi\";s:11:\"native_name\";s:15:\"هزاره گی\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/translation/core/4.4.24/haz.zip\";s:3:\"iso\";a:1:{i:3;s:3:\"haz\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"ادامه\";}}s:5:\"he_IL\";a:8:{s:8:\"language\";s:5:\"he_IL\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-16 12:47:15\";s:12:\"english_name\";s:6:\"Hebrew\";s:11:\"native_name\";s:16:\"עִבְרִית\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.1/he_IL.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"he\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"להמשיך\";}}s:5:\"hi_IN\";a:8:{s:8:\"language\";s:5:\"hi_IN\";s:7:\"version\";s:5:\"5.4.5\";s:7:\"updated\";s:19:\"2020-11-06 12:34:38\";s:12:\"english_name\";s:5:\"Hindi\";s:11:\"native_name\";s:18:\"हिन्दी\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.5/hi_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hi\";i:2;s:3:\"hin\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"जारी\";}}s:2:\"hr\";a:8:{s:8:\"language\";s:2:\"hr\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-27 09:02:30\";s:12:\"english_name\";s:8:\"Croatian\";s:11:\"native_name\";s:8:\"Hrvatski\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.7.1/hr.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hr\";i:2;s:3:\"hrv\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Nastavi\";}}s:3:\"hsb\";a:8:{s:8:\"language\";s:3:\"hsb\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-15 13:34:18\";s:12:\"english_name\";s:13:\"Upper Sorbian\";s:11:\"native_name\";s:17:\"Hornjoserbšćina\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.7.1/hsb.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"hsb\";i:3;s:3:\"hsb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:4:\"Dale\";}}s:5:\"hu_HU\";a:8:{s:8:\"language\";s:5:\"hu_HU\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-23 17:06:16\";s:12:\"english_name\";s:9:\"Hungarian\";s:11:\"native_name\";s:6:\"Magyar\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.1/hu_HU.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hu\";i:2;s:3:\"hun\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Tovább\";}}s:2:\"hy\";a:8:{s:8:\"language\";s:2:\"hy\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-12-03 16:21:10\";s:12:\"english_name\";s:8:\"Armenian\";s:11:\"native_name\";s:14:\"Հայերեն\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/hy.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hy\";i:2;s:3:\"hye\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Շարունակել\";}}s:5:\"id_ID\";a:8:{s:8:\"language\";s:5:\"id_ID\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-24 02:11:27\";s:12:\"english_name\";s:10:\"Indonesian\";s:11:\"native_name\";s:16:\"Bahasa Indonesia\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.1/id_ID.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"id\";i:2;s:3:\"ind\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Lanjutkan\";}}s:5:\"is_IS\";a:8:{s:8:\"language\";s:5:\"is_IS\";s:7:\"version\";s:6:\"4.9.17\";s:7:\"updated\";s:19:\"2018-12-11 10:40:02\";s:12:\"english_name\";s:9:\"Icelandic\";s:11:\"native_name\";s:9:\"Íslenska\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.17/is_IS.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"is\";i:2;s:3:\"isl\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Áfram\";}}s:5:\"it_IT\";a:8:{s:8:\"language\";s:5:\"it_IT\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-25 18:51:00\";s:12:\"english_name\";s:7:\"Italian\";s:11:\"native_name\";s:8:\"Italiano\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.1/it_IT.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"it\";i:2;s:3:\"ita\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continua\";}}s:2:\"ja\";a:8:{s:8:\"language\";s:2:\"ja\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-20 23:32:04\";s:12:\"english_name\";s:8:\"Japanese\";s:11:\"native_name\";s:9:\"日本語\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.7.1/ja.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"ja\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"続ける\";}}s:5:\"jv_ID\";a:8:{s:8:\"language\";s:5:\"jv_ID\";s:7:\"version\";s:6:\"4.9.17\";s:7:\"updated\";s:19:\"2019-02-16 23:58:56\";s:12:\"english_name\";s:8:\"Javanese\";s:11:\"native_name\";s:9:\"Basa Jawa\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.17/jv_ID.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"jv\";i:2;s:3:\"jav\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Nutugne\";}}s:5:\"ka_GE\";a:8:{s:8:\"language\";s:5:\"ka_GE\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-03-29 10:00:46\";s:12:\"english_name\";s:8:\"Georgian\";s:11:\"native_name\";s:21:\"ქართული\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.1/ka_GE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ka\";i:2;s:3:\"kat\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:30:\"გაგრძელება\";}}s:3:\"kab\";a:8:{s:8:\"language\";s:3:\"kab\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-03-11 22:04:53\";s:12:\"english_name\";s:6:\"Kabyle\";s:11:\"native_name\";s:9:\"Taqbaylit\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.7.1/kab.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"kab\";i:3;s:3:\"kab\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuer\";}}s:2:\"kk\";a:8:{s:8:\"language\";s:2:\"kk\";s:7:\"version\";s:6:\"4.9.17\";s:7:\"updated\";s:19:\"2018-07-10 11:35:44\";s:12:\"english_name\";s:6:\"Kazakh\";s:11:\"native_name\";s:19:\"Қазақ тілі\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.9.17/kk.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"kk\";i:2;s:3:\"kaz\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Жалғастыру\";}}s:2:\"km\";a:8:{s:8:\"language\";s:2:\"km\";s:7:\"version\";s:6:\"5.2.10\";s:7:\"updated\";s:19:\"2019-06-10 16:18:28\";s:12:\"english_name\";s:5:\"Khmer\";s:11:\"native_name\";s:27:\"ភាសាខ្មែរ\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.2.10/km.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"km\";i:2;s:3:\"khm\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"បន្ត\";}}s:2:\"kn\";a:8:{s:8:\"language\";s:2:\"kn\";s:7:\"version\";s:6:\"4.9.17\";s:7:\"updated\";s:19:\"2020-09-30 14:08:59\";s:12:\"english_name\";s:7:\"Kannada\";s:11:\"native_name\";s:15:\"ಕನ್ನಡ\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.9.17/kn.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"kn\";i:2;s:3:\"kan\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:30:\"ಮುಂದುವರೆಸಿ\";}}s:5:\"ko_KR\";a:8:{s:8:\"language\";s:5:\"ko_KR\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-21 08:25:29\";s:12:\"english_name\";s:6:\"Korean\";s:11:\"native_name\";s:9:\"한국어\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.1/ko_KR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ko\";i:2;s:3:\"kor\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"계속\";}}s:3:\"ckb\";a:8:{s:8:\"language\";s:3:\"ckb\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-29 01:20:15\";s:12:\"english_name\";s:16:\"Kurdish (Sorani)\";s:11:\"native_name\";s:13:\"كوردی‎\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.7.1/ckb.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ku\";i:3;s:3:\"ckb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:30:\"به‌رده‌وام به‌\";}}s:2:\"lo\";a:8:{s:8:\"language\";s:2:\"lo\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-12 09:59:23\";s:12:\"english_name\";s:3:\"Lao\";s:11:\"native_name\";s:21:\"ພາສາລາວ\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/lo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"lo\";i:2;s:3:\"lao\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"ຕໍ່\";}}s:5:\"lt_LT\";a:8:{s:8:\"language\";s:5:\"lt_LT\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-03-23 12:35:40\";s:12:\"english_name\";s:10:\"Lithuanian\";s:11:\"native_name\";s:15:\"Lietuvių kalba\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.1/lt_LT.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"lt\";i:2;s:3:\"lit\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Tęsti\";}}s:2:\"lv\";a:8:{s:8:\"language\";s:2:\"lv\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-16 19:35:51\";s:12:\"english_name\";s:7:\"Latvian\";s:11:\"native_name\";s:16:\"Latviešu valoda\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.7.1/lv.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"lv\";i:2;s:3:\"lav\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Turpināt\";}}s:5:\"mk_MK\";a:8:{s:8:\"language\";s:5:\"mk_MK\";s:7:\"version\";s:5:\"5.4.5\";s:7:\"updated\";s:19:\"2020-07-01 09:16:57\";s:12:\"english_name\";s:10:\"Macedonian\";s:11:\"native_name\";s:31:\"Македонски јазик\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.5/mk_MK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"mk\";i:2;s:3:\"mkd\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"Продолжи\";}}s:5:\"ml_IN\";a:8:{s:8:\"language\";s:5:\"ml_IN\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-27 03:43:32\";s:12:\"english_name\";s:9:\"Malayalam\";s:11:\"native_name\";s:18:\"മലയാളം\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/ml_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ml\";i:2;s:3:\"mal\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:18:\"തുടരുക\";}}s:2:\"mn\";a:8:{s:8:\"language\";s:2:\"mn\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-12 07:29:35\";s:12:\"english_name\";s:9:\"Mongolian\";s:11:\"native_name\";s:12:\"Монгол\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/mn.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"mn\";i:2;s:3:\"mon\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:24:\"Үргэлжлүүлэх\";}}s:2:\"mr\";a:8:{s:8:\"language\";s:2:\"mr\";s:7:\"version\";s:6:\"4.9.17\";s:7:\"updated\";s:19:\"2019-11-22 15:32:08\";s:12:\"english_name\";s:7:\"Marathi\";s:11:\"native_name\";s:15:\"मराठी\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.9.17/mr.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"mr\";i:2;s:3:\"mar\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:25:\"सुरु ठेवा\";}}s:5:\"ms_MY\";a:8:{s:8:\"language\";s:5:\"ms_MY\";s:7:\"version\";s:6:\"4.9.17\";s:7:\"updated\";s:19:\"2018-08-31 11:57:07\";s:12:\"english_name\";s:5:\"Malay\";s:11:\"native_name\";s:13:\"Bahasa Melayu\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.17/ms_MY.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ms\";i:2;s:3:\"msa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Teruskan\";}}s:5:\"my_MM\";a:8:{s:8:\"language\";s:5:\"my_MM\";s:7:\"version\";s:6:\"4.2.29\";s:7:\"updated\";s:19:\"2017-12-26 11:57:10\";s:12:\"english_name\";s:17:\"Myanmar (Burmese)\";s:11:\"native_name\";s:15:\"ဗမာစာ\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.2.29/my_MM.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"my\";i:2;s:3:\"mya\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:54:\"ဆက်လက်လုပ်ေဆာင်ပါ။\";}}s:5:\"nb_NO\";a:8:{s:8:\"language\";s:5:\"nb_NO\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-15 16:50:37\";s:12:\"english_name\";s:19:\"Norwegian (Bokmål)\";s:11:\"native_name\";s:13:\"Norsk bokmål\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.1/nb_NO.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nb\";i:2;s:3:\"nob\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Fortsett\";}}s:5:\"ne_NP\";a:8:{s:8:\"language\";s:5:\"ne_NP\";s:7:\"version\";s:6:\"5.2.10\";s:7:\"updated\";s:19:\"2020-05-31 16:07:59\";s:12:\"english_name\";s:6:\"Nepali\";s:11:\"native_name\";s:18:\"नेपाली\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/5.2.10/ne_NP.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ne\";i:2;s:3:\"nep\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:31:\"जारीराख्नु \";}}s:5:\"nl_NL\";a:8:{s:8:\"language\";s:5:\"nl_NL\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-25 19:59:32\";s:12:\"english_name\";s:5:\"Dutch\";s:11:\"native_name\";s:10:\"Nederlands\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.1/nl_NL.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nl\";i:2;s:3:\"nld\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Doorgaan\";}}s:5:\"nl_BE\";a:8:{s:8:\"language\";s:5:\"nl_BE\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-15 06:38:49\";s:12:\"english_name\";s:15:\"Dutch (Belgium)\";s:11:\"native_name\";s:20:\"Nederlands (België)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.1/nl_BE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nl\";i:2;s:3:\"nld\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Doorgaan\";}}s:12:\"nl_NL_formal\";a:8:{s:8:\"language\";s:12:\"nl_NL_formal\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-15 05:37:11\";s:12:\"english_name\";s:14:\"Dutch (Formal)\";s:11:\"native_name\";s:20:\"Nederlands (Formeel)\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/translation/core/5.7.1/nl_NL_formal.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nl\";i:2;s:3:\"nld\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Doorgaan\";}}s:5:\"nn_NO\";a:8:{s:8:\"language\";s:5:\"nn_NO\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-03-18 10:59:16\";s:12:\"english_name\";s:19:\"Norwegian (Nynorsk)\";s:11:\"native_name\";s:13:\"Norsk nynorsk\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.1/nn_NO.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nn\";i:2;s:3:\"nno\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Hald fram\";}}s:3:\"oci\";a:8:{s:8:\"language\";s:3:\"oci\";s:7:\"version\";s:6:\"4.8.16\";s:7:\"updated\";s:19:\"2017-08-25 10:03:08\";s:12:\"english_name\";s:7:\"Occitan\";s:11:\"native_name\";s:7:\"Occitan\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/translation/core/4.8.16/oci.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"oc\";i:2;s:3:\"oci\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Contunhar\";}}s:5:\"pa_IN\";a:8:{s:8:\"language\";s:5:\"pa_IN\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-16 05:19:43\";s:12:\"english_name\";s:7:\"Punjabi\";s:11:\"native_name\";s:18:\"ਪੰਜਾਬੀ\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/pa_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"pa\";i:2;s:3:\"pan\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:25:\"ਜਾਰੀ ਰੱਖੋ\";}}s:5:\"pl_PL\";a:8:{s:8:\"language\";s:5:\"pl_PL\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-26 06:40:10\";s:12:\"english_name\";s:6:\"Polish\";s:11:\"native_name\";s:6:\"Polski\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.1/pl_PL.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"pl\";i:2;s:3:\"pol\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Kontynuuj\";}}s:2:\"ps\";a:8:{s:8:\"language\";s:2:\"ps\";s:7:\"version\";s:6:\"4.3.25\";s:7:\"updated\";s:19:\"2015-12-02 21:41:29\";s:12:\"english_name\";s:6:\"Pashto\";s:11:\"native_name\";s:8:\"پښتو\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.3.25/ps.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ps\";i:2;s:3:\"pus\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"دوام\";}}s:5:\"pt_BR\";a:8:{s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-26 17:10:08\";s:12:\"english_name\";s:19:\"Portuguese (Brazil)\";s:11:\"native_name\";s:20:\"Português do Brasil\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.1/pt_BR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"pt\";i:2;s:3:\"por\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"pt_AO\";a:8:{s:8:\"language\";s:5:\"pt_AO\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2020-12-08 14:21:04\";s:12:\"english_name\";s:19:\"Portuguese (Angola)\";s:11:\"native_name\";s:20:\"Português de Angola\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.1/pt_AO.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"pt\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:10:\"pt_PT_ao90\";a:8:{s:8:\"language\";s:10:\"pt_PT_ao90\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-15 08:18:42\";s:12:\"english_name\";s:27:\"Portuguese (Portugal, AO90)\";s:11:\"native_name\";s:17:\"Português (AO90)\";s:7:\"package\";s:69:\"https://downloads.wordpress.org/translation/core/5.7.1/pt_PT_ao90.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"pt\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"pt_PT\";a:8:{s:8:\"language\";s:5:\"pt_PT\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-22 11:19:45\";s:12:\"english_name\";s:21:\"Portuguese (Portugal)\";s:11:\"native_name\";s:10:\"Português\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.1/pt_PT.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"pt\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:3:\"rhg\";a:8:{s:8:\"language\";s:3:\"rhg\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-03-16 13:03:18\";s:12:\"english_name\";s:8:\"Rohingya\";s:11:\"native_name\";s:8:\"Ruáinga\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/rhg.zip\";s:3:\"iso\";a:1:{i:3;s:3:\"rhg\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"ro_RO\";a:8:{s:8:\"language\";s:5:\"ro_RO\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-05-04 12:55:46\";s:12:\"english_name\";s:8:\"Romanian\";s:11:\"native_name\";s:8:\"Română\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.1/ro_RO.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ro\";i:2;s:3:\"ron\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuă\";}}s:5:\"ru_RU\";a:8:{s:8:\"language\";s:5:\"ru_RU\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-15 04:46:07\";s:12:\"english_name\";s:7:\"Russian\";s:11:\"native_name\";s:14:\"Русский\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.1/ru_RU.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ru\";i:2;s:3:\"rus\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Продолжить\";}}s:3:\"sah\";a:8:{s:8:\"language\";s:3:\"sah\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-21 02:06:41\";s:12:\"english_name\";s:5:\"Sakha\";s:11:\"native_name\";s:14:\"Сахалыы\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/sah.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"sah\";i:3;s:3:\"sah\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Салҕаа\";}}s:3:\"snd\";a:8:{s:8:\"language\";s:3:\"snd\";s:7:\"version\";s:5:\"5.4.5\";s:7:\"updated\";s:19:\"2020-07-07 01:53:37\";s:12:\"english_name\";s:6:\"Sindhi\";s:11:\"native_name\";s:8:\"سنڌي\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.4.5/snd.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"sd\";i:2;s:3:\"snd\";i:3;s:3:\"snd\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:15:\"اڳتي هلو\";}}s:5:\"si_LK\";a:8:{s:8:\"language\";s:5:\"si_LK\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-12 06:00:52\";s:12:\"english_name\";s:7:\"Sinhala\";s:11:\"native_name\";s:15:\"සිංහල\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/si_LK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"si\";i:2;s:3:\"sin\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:44:\"දිගටම කරගෙන යන්න\";}}s:5:\"sk_SK\";a:8:{s:8:\"language\";s:5:\"sk_SK\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-15 04:31:32\";s:12:\"english_name\";s:6:\"Slovak\";s:11:\"native_name\";s:11:\"Slovenčina\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.1/sk_SK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sk\";i:2;s:3:\"slk\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Pokračovať\";}}s:3:\"skr\";a:8:{s:8:\"language\";s:3:\"skr\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-23 11:54:14\";s:12:\"english_name\";s:7:\"Saraiki\";s:11:\"native_name\";s:14:\"سرائیکی\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.7.1/skr.zip\";s:3:\"iso\";a:1:{i:3;s:3:\"skr\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:17:\"جاری رکھو\";}}s:5:\"sl_SI\";a:8:{s:8:\"language\";s:5:\"sl_SI\";s:7:\"version\";s:5:\"5.1.9\";s:7:\"updated\";s:19:\"2019-04-30 13:03:56\";s:12:\"english_name\";s:9:\"Slovenian\";s:11:\"native_name\";s:13:\"Slovenščina\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.1.9/sl_SI.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sl\";i:2;s:3:\"slv\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Nadaljujte\";}}s:2:\"sq\";a:8:{s:8:\"language\";s:2:\"sq\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-15 09:02:54\";s:12:\"english_name\";s:8:\"Albanian\";s:11:\"native_name\";s:5:\"Shqip\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.7.1/sq.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sq\";i:2;s:3:\"sqi\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Vazhdo\";}}s:5:\"sr_RS\";a:8:{s:8:\"language\";s:5:\"sr_RS\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-14 22:03:48\";s:12:\"english_name\";s:7:\"Serbian\";s:11:\"native_name\";s:23:\"Српски језик\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.1/sr_RS.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sr\";i:2;s:3:\"srp\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:14:\"Настави\";}}s:5:\"sv_SE\";a:8:{s:8:\"language\";s:5:\"sv_SE\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-29 13:05:40\";s:12:\"english_name\";s:7:\"Swedish\";s:11:\"native_name\";s:7:\"Svenska\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.1/sv_SE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sv\";i:2;s:3:\"swe\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Fortsätt\";}}s:2:\"sw\";a:8:{s:8:\"language\";s:2:\"sw\";s:7:\"version\";s:5:\"5.3.7\";s:7:\"updated\";s:19:\"2019-10-13 15:35:35\";s:12:\"english_name\";s:7:\"Swahili\";s:11:\"native_name\";s:9:\"Kiswahili\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.3.7/sw.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sw\";i:2;s:3:\"swa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Endelea\";}}s:3:\"szl\";a:8:{s:8:\"language\";s:3:\"szl\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-09-24 19:58:14\";s:12:\"english_name\";s:8:\"Silesian\";s:11:\"native_name\";s:17:\"Ślōnskŏ gŏdka\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/szl.zip\";s:3:\"iso\";a:1:{i:3;s:3:\"szl\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:13:\"Kōntynuować\";}}s:5:\"ta_IN\";a:8:{s:8:\"language\";s:5:\"ta_IN\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-27 03:22:47\";s:12:\"english_name\";s:5:\"Tamil\";s:11:\"native_name\";s:15:\"தமிழ்\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/ta_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ta\";i:2;s:3:\"tam\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:24:\"தொடரவும்\";}}s:5:\"ta_LK\";a:8:{s:8:\"language\";s:5:\"ta_LK\";s:7:\"version\";s:6:\"4.2.29\";s:7:\"updated\";s:19:\"2015-12-03 01:07:44\";s:12:\"english_name\";s:17:\"Tamil (Sri Lanka)\";s:11:\"native_name\";s:15:\"தமிழ்\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.2.29/ta_LK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ta\";i:2;s:3:\"tam\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:18:\"தொடர்க\";}}s:2:\"te\";a:8:{s:8:\"language\";s:2:\"te\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-26 15:47:39\";s:12:\"english_name\";s:6:\"Telugu\";s:11:\"native_name\";s:18:\"తెలుగు\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/te.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"te\";i:2;s:3:\"tel\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:30:\"కొనసాగించు\";}}s:2:\"th\";a:8:{s:8:\"language\";s:2:\"th\";s:7:\"version\";s:5:\"5.5.4\";s:7:\"updated\";s:19:\"2021-04-22 18:43:36\";s:12:\"english_name\";s:4:\"Thai\";s:11:\"native_name\";s:9:\"ไทย\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.5.4/th.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"th\";i:2;s:3:\"tha\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:15:\"ต่อไป\";}}s:2:\"tl\";a:8:{s:8:\"language\";s:2:\"tl\";s:7:\"version\";s:6:\"4.8.16\";s:7:\"updated\";s:19:\"2017-09-30 09:04:29\";s:12:\"english_name\";s:7:\"Tagalog\";s:11:\"native_name\";s:7:\"Tagalog\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8.16/tl.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"tl\";i:2;s:3:\"tgl\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Magpatuloy\";}}s:5:\"tr_TR\";a:8:{s:8:\"language\";s:5:\"tr_TR\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-15 03:34:29\";s:12:\"english_name\";s:7:\"Turkish\";s:11:\"native_name\";s:8:\"Türkçe\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.1/tr_TR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"tr\";i:2;s:3:\"tur\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:5:\"Devam\";}}s:5:\"tt_RU\";a:8:{s:8:\"language\";s:5:\"tt_RU\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-20 20:20:50\";s:12:\"english_name\";s:5:\"Tatar\";s:11:\"native_name\";s:19:\"Татар теле\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/tt_RU.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"tt\";i:2;s:3:\"tat\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:17:\"дәвам итү\";}}s:3:\"tah\";a:8:{s:8:\"language\";s:3:\"tah\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-03-06 18:39:39\";s:12:\"english_name\";s:8:\"Tahitian\";s:11:\"native_name\";s:10:\"Reo Tahiti\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/tah.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"ty\";i:2;s:3:\"tah\";i:3;s:3:\"tah\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"ug_CN\";a:8:{s:8:\"language\";s:5:\"ug_CN\";s:7:\"version\";s:6:\"4.9.17\";s:7:\"updated\";s:19:\"2018-05-16 07:36:13\";s:12:\"english_name\";s:6:\"Uighur\";s:11:\"native_name\";s:16:\"ئۇيغۇرچە\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.17/ug_CN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ug\";i:2;s:3:\"uig\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:26:\"داۋاملاشتۇرۇش\";}}s:2:\"uk\";a:8:{s:8:\"language\";s:2:\"uk\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-24 18:04:20\";s:12:\"english_name\";s:9:\"Ukrainian\";s:11:\"native_name\";s:20:\"Українська\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.7.1/uk.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"uk\";i:2;s:3:\"ukr\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Продовжити\";}}s:2:\"ur\";a:8:{s:8:\"language\";s:2:\"ur\";s:7:\"version\";s:5:\"5.4.5\";s:7:\"updated\";s:19:\"2020-04-09 11:17:33\";s:12:\"english_name\";s:4:\"Urdu\";s:11:\"native_name\";s:8:\"اردو\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.4.5/ur.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ur\";i:2;s:3:\"urd\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:19:\"جاری رکھیں\";}}s:5:\"uz_UZ\";a:8:{s:8:\"language\";s:5:\"uz_UZ\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-02-28 12:02:22\";s:12:\"english_name\";s:5:\"Uzbek\";s:11:\"native_name\";s:11:\"O‘zbekcha\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.1/uz_UZ.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"uz\";i:2;s:3:\"uzb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Продолжить\";}}s:2:\"vi\";a:8:{s:8:\"language\";s:2:\"vi\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-03-10 06:03:58\";s:12:\"english_name\";s:10:\"Vietnamese\";s:11:\"native_name\";s:14:\"Tiếng Việt\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.7.1/vi.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"vi\";i:2;s:3:\"vie\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Tiếp tục\";}}s:5:\"zh_TW\";a:8:{s:8:\"language\";s:5:\"zh_TW\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-15 05:47:07\";s:12:\"english_name\";s:16:\"Chinese (Taiwan)\";s:11:\"native_name\";s:12:\"繁體中文\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.1/zh_TW.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"zh\";i:2;s:3:\"zho\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"繼續\";}}s:5:\"zh_HK\";a:8:{s:8:\"language\";s:5:\"zh_HK\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-04-07 07:43:24\";s:12:\"english_name\";s:19:\"Chinese (Hong Kong)\";s:11:\"native_name\";s:16:\"香港中文版	\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.1/zh_HK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"zh\";i:2;s:3:\"zho\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"繼續\";}}s:5:\"zh_CN\";a:8:{s:8:\"language\";s:5:\"zh_CN\";s:7:\"version\";s:5:\"5.7.1\";s:7:\"updated\";s:19:\"2021-05-04 08:08:55\";s:12:\"english_name\";s:15:\"Chinese (China)\";s:11:\"native_name\";s:12:\"简体中文\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.1/zh_CN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"zh\";i:2;s:3:\"zho\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"继续\";}}}', 'no');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(594, '_transient_timeout_processed_original_string_meta_post_id_for_330b4e03875bb9d28e5b47c4d31cd10a', '1620289661', 'no'),
(595, '_transient_processed_original_string_meta_post_id_for_330b4e03875bb9d28e5b47c4d31cd10a', 'done', 'no'),
(596, '_transient_timeout_processed_original_string_meta_post_id_for_481457163b9211c8aad822096fe5ef55', '1620289673', 'no'),
(597, '_transient_processed_original_string_meta_post_id_for_481457163b9211c8aad822096fe5ef55', 'done', 'no'),
(600, '_site_transient_timeout_theme_roots', '1620209744', 'no'),
(601, '_site_transient_theme_roots', 'a:4:{s:14:\"twentynineteen\";s:7:\"/themes\";s:12:\"twentytwenty\";s:7:\"/themes\";s:15:\"twentytwentyone\";s:7:\"/themes\";s:9:\"videotube\";s:7:\"/themes\";}', 'no');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `wp_postmeta`
--

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'tpl-videos.php'),
(2, 3, '_wp_page_template', 'default'),
(59, 12, '_menu_item_type', 'post_type'),
(60, 12, '_menu_item_menu_item_parent', '0'),
(61, 12, '_menu_item_object_id', '2'),
(62, 12, '_menu_item_object', 'page'),
(63, 12, '_menu_item_target', ''),
(64, 12, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(65, 12, '_menu_item_xfn', ''),
(66, 12, '_menu_item_url', ''),
(68, 2, '_edit_lock', '1620101640:1'),
(69, 2, '_edit_last', '1'),
(70, 2, 'videotube_post_type', 'post'),
(71, 2, '_videotube_post_type', 'field_535765e9e7089'),
(72, 13, 'videotube_post_type', 'post'),
(73, 13, '_videotube_post_type', 'field_535765e9e7089'),
(74, 14, 'like_key', '1'),
(75, 14, '_edit_last', '1'),
(76, 14, '_edit_lock', '1619538803:1'),
(77, 15, '_wp_attached_file', '2021/04/JeALalala.jpg'),
(78, 15, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:1080;s:4:\"file\";s:21:\"2021/04/JeALalala.jpg\";s:5:\"sizes\";a:11:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"JeALalala-300x169.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:169;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:22:\"JeALalala-1024x576.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"JeALalala-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:21:\"JeALalala-768x432.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:432;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:22:\"JeALalala-1536x864.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:864;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"video-featured\";a:4:{s:4:\"file\";s:21:\"JeALalala-360x240.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:240;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"video-lastest\";a:4:{s:4:\"file\";s:21:\"JeALalala-230x150.jpg\";s:5:\"width\";i:230;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:23:\"video-category-featured\";a:4:{s:4:\"file\";s:21:\"JeALalala-295x197.jpg\";s:5:\"width\";i:295;s:6:\"height\";i:197;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:28:\"video-item-category-featured\";a:4:{s:4:\"file\";s:21:\"JeALalala-750x440.jpg\";s:5:\"width\";i:750;s:6:\"height\";i:440;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"most-video-2col\";a:4:{s:4:\"file\";s:21:\"JeALalala-165x108.jpg\";s:5:\"width\";i:165;s:6:\"height\";i:108;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:16:\"blog-large-thumb\";a:4:{s:4:\"file\";s:21:\"JeALalala-750x422.jpg\";s:5:\"width\";i:750;s:6:\"height\";i:422;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(79, 14, '_thumbnail_id', '15'),
(80, 14, 'layout', 'small'),
(81, 14, '_layout', 'field_531980e906752'),
(82, 14, 'video_type', 'normal'),
(83, 14, '_video_type', 'field_53eb79f33936e'),
(84, 14, 'video_url', '<iframe style=\"width:100%;height:100%;position:absolute;left:0px;top:0px;overflow:hidden\" frameborder=\"0\" type=\"text/html\" src=\"https://www.dailymotion.com/embed/video/x7of8y3?autoplay=1\" width=\"100%\" height=\"100%\" allowfullscreen allow=\"autoplay\"> </iframe>\r\n<iframe style=\"width:100%;height:100%;position:absolute;left:0px;top:0px;overflow:hidden\" frameborder=\"0\" type=\"text/html\" src=\"https://www.dailymotion.com/embed/video/x7of8y3?autoplay=1\" width=\"100%\" height=\"100%\" allowfullscreen allow=\"autoplay\"> </iframe>\r\n<iframe style=\"width:100%;height:100%;position:absolute;left:0px;top:0px;overflow:hidden\" frameborder=\"0\" type=\"text/html\" src=\"https://www.dailymotion.com/embed/video/x7of8y3?autoplay=1\" width=\"100%\" height=\"100%\" allowfullscreen allow=\"autoplay\"> </iframe>\r\n<iframe style=\"width:100%;height:100%;position:absolute;left:0px;top:0px;overflow:hidden\" frameborder=\"0\" type=\"text/html\" src=\"https://www.dailymotion.com/embed/video/x7of8y3?autoplay=1\" width=\"100%\" height=\"100%\" allowfullscreen allow=\"autoplay\"> </iframe>'),
(85, 14, '_video_url', 'field_53eb7a453936f'),
(86, 14, 'count_viewed', '4'),
(87, 16, '_wp_trash_meta_status', 'publish'),
(88, 16, '_wp_trash_meta_time', '1619538126'),
(89, 1, '_wp_trash_meta_status', 'publish'),
(90, 1, '_wp_trash_meta_time', '1619538953'),
(91, 1, '_wp_desired_post_slug', 'hello-world'),
(92, 1, '_wp_trash_meta_comments_status', 'a:1:{i:1;s:1:\"1\";}'),
(93, 18, 'like_key', '1'),
(94, 18, '_edit_last', '1'),
(95, 18, '_edit_lock', '1619599962:1'),
(96, 19, '_wp_attached_file', '2021/04/580eb99da440318419e445bb83d884937c089497r1_647_1018v2_uhq.jpg'),
(97, 19, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:647;s:6:\"height\";i:1018;s:4:\"file\";s:69:\"2021/04/580eb99da440318419e445bb83d884937c089497r1_647_1018v2_uhq.jpg\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:69:\"580eb99da440318419e445bb83d884937c089497r1_647_1018v2_uhq-191x300.jpg\";s:5:\"width\";i:191;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:69:\"580eb99da440318419e445bb83d884937c089497r1_647_1018v2_uhq-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"video-featured\";a:4:{s:4:\"file\";s:69:\"580eb99da440318419e445bb83d884937c089497r1_647_1018v2_uhq-360x240.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:240;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"video-lastest\";a:4:{s:4:\"file\";s:69:\"580eb99da440318419e445bb83d884937c089497r1_647_1018v2_uhq-230x150.jpg\";s:5:\"width\";i:230;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:23:\"video-category-featured\";a:4:{s:4:\"file\";s:69:\"580eb99da440318419e445bb83d884937c089497r1_647_1018v2_uhq-295x197.jpg\";s:5:\"width\";i:295;s:6:\"height\";i:197;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:28:\"video-item-category-featured\";a:4:{s:4:\"file\";s:69:\"580eb99da440318419e445bb83d884937c089497r1_647_1018v2_uhq-647x440.jpg\";s:5:\"width\";i:647;s:6:\"height\";i:440;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"most-video-2col\";a:4:{s:4:\"file\";s:69:\"580eb99da440318419e445bb83d884937c089497r1_647_1018v2_uhq-165x108.jpg\";s:5:\"width\";i:165;s:6:\"height\";i:108;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(98, 18, '_thumbnail_id', '19'),
(99, 18, 'layout', 'small'),
(100, 18, '_layout', 'field_531980e906752'),
(101, 18, 'video_type', 'normal'),
(102, 18, '_video_type', 'field_53eb79f33936e'),
(103, 18, 'video_url', '<iframe style=\"width:100%;height:100%;position:absolute;left:0px;top:0px;overflow:hidden\" frameborder=\"0\" type=\"text/html\" src=\"https://www.dailymotion.com/embed/video/x7x586d?autoplay=1\" width=\"100%\" height=\"100%\" allowfullscreen allow=\"autoplay\"> </iframe>'),
(104, 18, '_video_url', 'field_53eb7a453936f'),
(105, 18, 'count_viewed', '5'),
(106, 20, 'like_key', '1'),
(107, 20, '_edit_last', '1'),
(108, 20, '_edit_lock', '1619581032:1'),
(109, 21, '_wp_attached_file', '2021/04/f531fec46cb7475ab03bb412c1344f77.jpg'),
(110, 21, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:900;s:6:\"height\";i:600;s:4:\"file\";s:44:\"2021/04/f531fec46cb7475ab03bb412c1344f77.jpg\";s:5:\"sizes\";a:9:{s:6:\"medium\";a:4:{s:4:\"file\";s:44:\"f531fec46cb7475ab03bb412c1344f77-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:44:\"f531fec46cb7475ab03bb412c1344f77-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:44:\"f531fec46cb7475ab03bb412c1344f77-768x512.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"video-featured\";a:4:{s:4:\"file\";s:44:\"f531fec46cb7475ab03bb412c1344f77-360x240.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:240;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"video-lastest\";a:4:{s:4:\"file\";s:44:\"f531fec46cb7475ab03bb412c1344f77-230x150.jpg\";s:5:\"width\";i:230;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:23:\"video-category-featured\";a:4:{s:4:\"file\";s:44:\"f531fec46cb7475ab03bb412c1344f77-295x197.jpg\";s:5:\"width\";i:295;s:6:\"height\";i:197;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:28:\"video-item-category-featured\";a:4:{s:4:\"file\";s:44:\"f531fec46cb7475ab03bb412c1344f77-750x440.jpg\";s:5:\"width\";i:750;s:6:\"height\";i:440;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"most-video-2col\";a:4:{s:4:\"file\";s:44:\"f531fec46cb7475ab03bb412c1344f77-165x108.jpg\";s:5:\"width\";i:165;s:6:\"height\";i:108;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:16:\"blog-large-thumb\";a:4:{s:4:\"file\";s:44:\"f531fec46cb7475ab03bb412c1344f77-750x500.jpg\";s:5:\"width\";i:750;s:6:\"height\";i:500;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(111, 20, '_thumbnail_id', '21'),
(112, 20, 'layout', 'small'),
(113, 20, '_layout', 'field_531980e906752'),
(114, 20, 'video_type', 'normal'),
(115, 20, '_video_type', 'field_53eb79f33936e'),
(116, 20, 'video_url', '<iframe style=\"width:100%;height:100%;position:absolute;left:0px;top:0px;overflow:hidden\" frameborder=\"0\" type=\"text/html\" src=\"https://www.dailymotion.com/embed/video/x7u8uz2?autoplay=1\" width=\"100%\" height=\"100%\" allowfullscreen allow=\"autoplay\"> </iframe>'),
(117, 20, '_video_url', 'field_53eb7a453936f'),
(118, 22, 'like_key', '1'),
(119, 22, '_edit_last', '1'),
(120, 22, '_edit_lock', '1620097772:1'),
(121, 23, '_wp_attached_file', '2021/04/img-share.jpg'),
(122, 23, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1200;s:6:\"height\";i:630;s:4:\"file\";s:21:\"2021/04/img-share.jpg\";s:5:\"sizes\";a:10:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"img-share-300x158.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:158;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:22:\"img-share-1024x538.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:538;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"img-share-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:21:\"img-share-768x403.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:403;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"video-featured\";a:4:{s:4:\"file\";s:21:\"img-share-360x240.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:240;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"video-lastest\";a:4:{s:4:\"file\";s:21:\"img-share-230x150.jpg\";s:5:\"width\";i:230;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:23:\"video-category-featured\";a:4:{s:4:\"file\";s:21:\"img-share-295x197.jpg\";s:5:\"width\";i:295;s:6:\"height\";i:197;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:28:\"video-item-category-featured\";a:4:{s:4:\"file\";s:21:\"img-share-750x440.jpg\";s:5:\"width\";i:750;s:6:\"height\";i:440;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"most-video-2col\";a:4:{s:4:\"file\";s:21:\"img-share-165x108.jpg\";s:5:\"width\";i:165;s:6:\"height\";i:108;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:16:\"blog-large-thumb\";a:4:{s:4:\"file\";s:21:\"img-share-750x394.jpg\";s:5:\"width\";i:750;s:6:\"height\";i:394;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(123, 22, '_thumbnail_id', '23'),
(124, 22, 'layout', 'small'),
(125, 22, '_layout', 'field_531980e906752'),
(126, 22, 'video_type', 'normal'),
(127, 22, '_video_type', 'field_53eb79f33936e'),
(128, 22, 'video_url', '<iframe style=\"width:100%;height:100%;position:absolute;left:0px;top:0px;overflow:hidden\" frameborder=\"0\" type=\"text/html\" src=\"https://www.dailymotion.com/embed/video/x7qo9rn?autoplay=1\" width=\"100%\" height=\"100%\" allowfullscreen allow=\"autoplay\"> </iframe> '),
(129, 22, '_video_url', 'field_53eb7a453936f'),
(130, 24, '_wp_trash_meta_status', 'publish'),
(131, 24, '_wp_trash_meta_time', '1619542496'),
(132, 20, 'count_viewed', '3'),
(133, 25, '_wp_trash_meta_status', 'publish'),
(134, 25, '_wp_trash_meta_time', '1619543022'),
(135, 26, '_wp_trash_meta_status', 'publish'),
(136, 26, '_wp_trash_meta_time', '1619543041'),
(137, 22, 'count_viewed', '1'),
(138, 27, 'like_key', '1'),
(139, 27, '_edit_last', '1'),
(140, 27, '_edit_lock', '1619592304:1'),
(143, 29, '_wp_attached_file', '2021/04/pc_login.jpg'),
(144, 29, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:1681;s:4:\"file\";s:20:\"2021/04/pc_login.jpg\";s:5:\"sizes\";a:11:{s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"pc_login-300x263.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:263;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:21:\"pc_login-1024x897.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:897;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"pc_login-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:20:\"pc_login-768x672.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:672;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:22:\"pc_login-1536x1345.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:1345;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"video-featured\";a:4:{s:4:\"file\";s:20:\"pc_login-360x240.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:240;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"video-lastest\";a:4:{s:4:\"file\";s:20:\"pc_login-230x150.jpg\";s:5:\"width\";i:230;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:23:\"video-category-featured\";a:4:{s:4:\"file\";s:20:\"pc_login-295x197.jpg\";s:5:\"width\";i:295;s:6:\"height\";i:197;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:28:\"video-item-category-featured\";a:4:{s:4:\"file\";s:20:\"pc_login-750x440.jpg\";s:5:\"width\";i:750;s:6:\"height\";i:440;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"most-video-2col\";a:4:{s:4:\"file\";s:20:\"pc_login-165x108.jpg\";s:5:\"width\";i:165;s:6:\"height\";i:108;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:16:\"blog-large-thumb\";a:4:{s:4:\"file\";s:20:\"pc_login-750x657.jpg\";s:5:\"width\";i:750;s:6:\"height\";i:657;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(146, 27, 'layout', 'small'),
(147, 27, '_layout', 'field_531980e906752'),
(148, 27, 'video_type', 'normal'),
(149, 27, '_video_type', 'field_53eb79f33936e'),
(150, 27, 'video_url', '<iframe style=\"width:100%;height:100%;position:absolute;left:0px;top:0px;overflow:hidden\" frameborder=\"0\" type=\"text/html\" src=\"https://www.dailymotion.com/embed/video/x7qo9rn?autoplay=1\" width=\"100%\" height=\"100%\" allowfullscreen allow=\"autoplay\"> </iframe> '),
(151, 27, '_video_url', 'field_53eb7a453936f'),
(152, 30, 'like_key', '1'),
(153, 27, '_custom_url_image', 'https://s.w.org/plugins/geopattern-icon/custom-url-to-featured-image_541e8e.svg'),
(156, 27, 'count_viewed', '1'),
(167, 33, 'like_key', '1'),
(168, 33, '_edit_last', '1'),
(169, 33, '_edit_lock', '1619594373:1'),
(170, 33, '_oembed_32a7ebeebff0c7e248eaaebb54ee19ce', '{{unknown}}'),
(171, 33, 'layout', 'small'),
(172, 33, '_layout', 'field_531980e906752'),
(173, 33, 'video_type', 'normal'),
(174, 33, '_video_type', 'field_53eb79f33936e'),
(175, 33, 'video_url', '<iframe style=\"width:100%;height:100%;position:absolute;left:0px;top:0px;overflow:hidden\" frameborder=\"0\" type=\"text/html\" src=\"https://www.dailymotion.com/embed/video/x7qo9rn?autoplay=1\" width=\"100%\" height=\"100%\" allowfullscreen allow=\"autoplay\"> </iframe> '),
(176, 33, '_video_url', 'field_53eb7a453936f'),
(195, 33, 'count_viewed', '2'),
(196, 39, '_edit_lock', '1619580732:1'),
(197, 41, 'like_key', '1'),
(198, 42, 'like_key', '1'),
(212, 46, 'like_key', '1'),
(213, 46, '_edit_last', '1'),
(214, 46, '_edit_lock', '1620182522:1'),
(216, 46, 'layout', 'small'),
(217, 46, '_layout', 'field_531980e906752'),
(218, 46, 'video_type', 'normal'),
(219, 46, '_video_type', 'field_53eb79f33936e'),
(220, 46, 'video_url', '<iframe style=\"width:100%;height:100%;position:absolute;left:0px;top:0px;overflow:hidden\" frameborder=\"0\" type=\"text/html\" src=\"https://www.dailymotion.com/embed/video/x7qo9rn?autoplay=1\" width=\"100%\" height=\"100%\" allowfullscreen allow=\"autoplay\"> </iframe>'),
(221, 46, '_video_url', 'field_53eb7a453936f'),
(222, 46, 'count_viewed', '2'),
(223, 47, 'like_key', '1'),
(224, 47, '_edit_last', '1'),
(225, 47, '_edit_lock', '1620097733:1'),
(226, 47, '_thumbnail_id', '21'),
(227, 47, 'layout', 'small'),
(228, 47, '_layout', 'field_531980e906752'),
(229, 47, 'video_type', 'normal'),
(230, 47, '_video_type', 'field_53eb79f33936e'),
(231, 47, 'video_url', '<iframe style=\"width:100%;height:100%;position:absolute;left:0px;top:0px;overflow:hidden\" frameborder=\"0\" type=\"text/html\" src=\"https://www.dailymotion.com/embed/video/x7x586d?autoplay=1\" width=\"100%\" height=\"100%\" allowfullscreen allow=\"autoplay\"> </iframe>'),
(232, 47, '_video_url', 'field_53eb7a453936f'),
(233, 48, 'like_key', '1'),
(234, 48, '_edit_last', '1'),
(235, 48, '_edit_lock', '1620097739:1'),
(236, 48, '_thumbnail_id', '19'),
(237, 48, 'layout', 'small'),
(238, 48, '_layout', 'field_531980e906752'),
(239, 48, 'video_type', 'normal'),
(240, 48, '_video_type', 'field_53eb79f33936e'),
(241, 48, 'video_url', '<iframe style=\"width:100%;height:100%;position:absolute;left:0px;top:0px;overflow:hidden\" frameborder=\"0\" type=\"text/html\" src=\"https://www.dailymotion.com/embed/video/x7x586d?autoplay=1\" width=\"100%\" height=\"100%\" allowfullscreen allow=\"autoplay\"> </iframe>'),
(242, 48, '_video_url', 'field_53eb7a453936f'),
(243, 49, 'like_key', '1'),
(244, 49, '_edit_last', '1'),
(245, 49, '_edit_lock', '1620097708:1'),
(246, 49, 'layout', 'small'),
(247, 49, '_layout', 'field_531980e906752'),
(248, 49, 'video_type', 'normal'),
(249, 49, '_video_type', 'field_53eb79f33936e'),
(250, 49, 'video_url', '<iframe style=\"width:100%;height:100%;position:absolute;left:0px;top:0px;overflow:hidden\" frameborder=\"0\" type=\"text/html\" src=\"https://www.dailymotion.com/embed/video/x7x586d?autoplay=1\" width=\"100%\" height=\"100%\" allowfullscreen allow=\"autoplay\"> </iframe>'),
(251, 49, '_video_url', 'field_53eb7a453936f'),
(252, 49, 'count_viewed', '2'),
(253, 50, 'like_key', '2'),
(254, 50, '_edit_last', '1'),
(255, 50, '_edit_lock', '1620097629:1'),
(256, 50, '_thumbnail_id', '15'),
(257, 50, 'layout', 'small'),
(258, 50, '_layout', 'field_531980e906752'),
(259, 50, 'video_type', 'normal'),
(260, 50, '_video_type', 'field_53eb79f33936e'),
(261, 50, 'video_url', '<iframe style=\"width:100%;height:100%;position:absolute;left:0px;top:0px;overflow:hidden\" frameborder=\"0\" type=\"text/html\" src=\"https://www.dailymotion.com/embed/video/x7x586d?autoplay=1\" width=\"100%\" height=\"100%\" allowfullscreen allow=\"autoplay\"> </iframe>'),
(262, 50, '_video_url', 'field_53eb7a453936f'),
(263, 51, 'like_key', '1'),
(264, 51, '_edit_last', '1'),
(265, 51, '_edit_lock', '1620198576:1'),
(266, 51, '_thumbnail_id', '21'),
(267, 51, 'layout', 'small'),
(268, 51, '_layout', 'field_531980e906752'),
(269, 51, 'video_type', 'normal'),
(270, 51, '_video_type', 'field_53eb79f33936e'),
(271, 51, 'video_url', '<iframe style=\"width:100%;height:100%;position:absolute;left:0px;top:0px;overflow:hidden\" frameborder=\"0\" type=\"text/html\" src=\"https://www.dailymotion.com/embed/video/x7x586d?autoplay=1\" width=\"100%\" height=\"100%\" allowfullscreen allow=\"autoplay\"> </iframe>'),
(272, 51, '_video_url', 'field_53eb7a453936f'),
(273, 52, 'like_key', '1'),
(274, 52, '_edit_last', '1'),
(275, 52, '_edit_lock', '1620113541:1'),
(277, 52, 'layout', 'small'),
(278, 52, '_layout', 'field_531980e906752'),
(279, 52, 'video_type', 'normal'),
(280, 52, '_video_type', 'field_53eb79f33936e'),
(281, 52, 'video_url', '<iframe style=\"width:100%;height:100%;position:absolute;left:0px;top:0px;overflow:hidden\" frameborder=\"0\" type=\"text/html\" src=\"https://www.dailymotion.com/embed/video/x7x586d?autoplay=1\" width=\"100%\" height=\"100%\" allowfullscreen allow=\"autoplay\"> </iframe>'),
(282, 52, '_video_url', 'field_53eb7a453936f'),
(283, 52, 'count_viewed', '3'),
(284, 50, 'count_viewed', '3'),
(285, 53, '_wp_attached_file', '2021/04/no_image_available.jpg'),
(286, 53, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:600;s:6:\"height\";i:600;s:4:\"file\";s:30:\"2021/04/no_image_available.jpg\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"no_image_available-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:30:\"no_image_available-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"video-featured\";a:4:{s:4:\"file\";s:30:\"no_image_available-360x240.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:240;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"video-lastest\";a:4:{s:4:\"file\";s:30:\"no_image_available-230x150.jpg\";s:5:\"width\";i:230;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:23:\"video-category-featured\";a:4:{s:4:\"file\";s:30:\"no_image_available-295x197.jpg\";s:5:\"width\";i:295;s:6:\"height\";i:197;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:28:\"video-item-category-featured\";a:4:{s:4:\"file\";s:30:\"no_image_available-600x440.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:440;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"most-video-2col\";a:4:{s:4:\"file\";s:30:\"no_image_available-165x108.jpg\";s:5:\"width\";i:165;s:6:\"height\";i:108;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(287, 19, '_edit_lock', '1619604726:1'),
(288, 19, '_edit_last', '1'),
(289, 21, '_edit_lock', '1619604832:1'),
(290, 21, '_edit_last', '1'),
(291, 54, '_menu_item_type', 'taxonomy'),
(292, 54, '_menu_item_menu_item_parent', '0'),
(293, 54, '_menu_item_object_id', '25'),
(294, 54, '_menu_item_object', 'categories'),
(295, 54, '_menu_item_target', ''),
(296, 54, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(297, 54, '_menu_item_xfn', ''),
(298, 54, '_menu_item_url', ''),
(300, 55, '_menu_item_type', 'taxonomy'),
(301, 55, '_menu_item_menu_item_parent', '0'),
(302, 55, '_menu_item_object_id', '23'),
(303, 55, '_menu_item_object', 'categories'),
(304, 55, '_menu_item_target', ''),
(305, 55, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(306, 55, '_menu_item_xfn', ''),
(307, 55, '_menu_item_url', ''),
(309, 56, '_menu_item_type', 'taxonomy'),
(310, 56, '_menu_item_menu_item_parent', '0'),
(311, 56, '_menu_item_object_id', '26'),
(312, 56, '_menu_item_object', 'categories'),
(313, 56, '_menu_item_target', ''),
(314, 56, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(315, 56, '_menu_item_xfn', ''),
(316, 56, '_menu_item_url', ''),
(318, 57, '_menu_item_type', 'taxonomy'),
(319, 57, '_menu_item_menu_item_parent', '0'),
(320, 57, '_menu_item_object_id', '28'),
(321, 57, '_menu_item_object', 'categories'),
(322, 57, '_menu_item_target', ''),
(323, 57, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(324, 57, '_menu_item_xfn', ''),
(325, 57, '_menu_item_url', ''),
(327, 58, '_menu_item_type', 'taxonomy'),
(328, 58, '_menu_item_menu_item_parent', '0'),
(329, 58, '_menu_item_object_id', '22'),
(330, 58, '_menu_item_object', 'categories'),
(331, 58, '_menu_item_target', ''),
(332, 58, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(333, 58, '_menu_item_xfn', ''),
(334, 58, '_menu_item_url', ''),
(336, 59, '_menu_item_type', 'taxonomy'),
(337, 59, '_menu_item_menu_item_parent', '0'),
(338, 59, '_menu_item_object_id', '27'),
(339, 59, '_menu_item_object', 'categories'),
(340, 59, '_menu_item_target', ''),
(341, 59, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(342, 59, '_menu_item_xfn', ''),
(343, 59, '_menu_item_url', ''),
(345, 60, '_menu_item_type', 'taxonomy'),
(346, 60, '_menu_item_menu_item_parent', '0'),
(347, 60, '_menu_item_object_id', '24'),
(348, 60, '_menu_item_object', 'categories'),
(349, 60, '_menu_item_target', ''),
(350, 60, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(351, 60, '_menu_item_xfn', ''),
(352, 60, '_menu_item_url', ''),
(354, 12, '_wp_old_date', '2021-04-27'),
(382, 65, '_vc_post_settings', 'a:1:{s:10:\"vc_grid_id\";a:0:{}}'),
(383, 65, '_edit_last', '1'),
(384, 65, '_wp_page_template', 'default'),
(385, 66, '_vc_post_settings', 'a:1:{s:10:\"vc_grid_id\";a:0:{}}'),
(386, 65, '_edit_lock', '1619664048:1'),
(387, 65, '_wp_trash_meta_status', 'publish'),
(388, 65, '_wp_trash_meta_time', '1619664080'),
(389, 65, '_wp_desired_post_slug', 'group_608a1a93131ca'),
(390, 66, '_wp_trash_meta_status', 'publish'),
(391, 66, '_wp_trash_meta_time', '1619664080'),
(392, 66, '_wp_desired_post_slug', 'field_608a1ab2f2f97'),
(393, 67, '_vc_post_settings', 'a:1:{s:10:\"vc_grid_id\";a:0:{}}'),
(394, 67, '_menu_item_type', 'taxonomy'),
(395, 67, '_menu_item_menu_item_parent', '0'),
(396, 67, '_menu_item_object_id', '33'),
(397, 67, '_menu_item_object', 'video_tag'),
(398, 67, '_menu_item_target', ''),
(399, 67, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(400, 67, '_menu_item_xfn', ''),
(401, 67, '_menu_item_url', ''),
(403, 68, '_vc_post_settings', 'a:1:{s:10:\"vc_grid_id\";a:0:{}}'),
(404, 68, '_menu_item_type', 'taxonomy'),
(405, 68, '_menu_item_menu_item_parent', '0'),
(406, 68, '_menu_item_object_id', '34'),
(407, 68, '_menu_item_object', 'video_tag'),
(408, 68, '_menu_item_target', ''),
(409, 68, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(410, 68, '_menu_item_xfn', ''),
(411, 68, '_menu_item_url', ''),
(413, 69, '_vc_post_settings', 'a:1:{s:10:\"vc_grid_id\";a:0:{}}'),
(414, 69, '_menu_item_type', 'taxonomy'),
(415, 69, '_menu_item_menu_item_parent', '0'),
(416, 69, '_menu_item_object_id', '35'),
(417, 69, '_menu_item_object', 'video_tag'),
(418, 69, '_menu_item_target', ''),
(419, 69, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(420, 69, '_menu_item_xfn', ''),
(421, 69, '_menu_item_url', ''),
(423, 12, '_wp_old_date', '2021-04-28'),
(424, 12, '_vc_post_settings', 'a:1:{s:10:\"vc_grid_id\";a:0:{}}'),
(425, 58, '_wp_old_date', '2021-04-28'),
(426, 58, '_vc_post_settings', 'a:1:{s:10:\"vc_grid_id\";a:0:{}}'),
(427, 55, '_wp_old_date', '2021-04-28'),
(428, 55, '_vc_post_settings', 'a:1:{s:10:\"vc_grid_id\";a:0:{}}'),
(429, 60, '_wp_old_date', '2021-04-28'),
(430, 60, '_vc_post_settings', 'a:1:{s:10:\"vc_grid_id\";a:0:{}}'),
(431, 54, '_wp_old_date', '2021-04-28'),
(432, 54, '_vc_post_settings', 'a:1:{s:10:\"vc_grid_id\";a:0:{}}'),
(433, 56, '_wp_old_date', '2021-04-28'),
(434, 56, '_vc_post_settings', 'a:1:{s:10:\"vc_grid_id\";a:0:{}}'),
(435, 59, '_wp_old_date', '2021-04-28'),
(436, 59, '_vc_post_settings', 'a:1:{s:10:\"vc_grid_id\";a:0:{}}'),
(437, 57, '_wp_old_date', '2021-04-28'),
(438, 57, '_vc_post_settings', 'a:1:{s:10:\"vc_grid_id\";a:0:{}}'),
(439, 52, '_vc_post_settings', 'a:1:{s:10:\"vc_grid_id\";a:0:{}}'),
(440, 52, 'fifu_image_url', 'https://hivepress.io/wp-content/uploads/2021/01/logo.svg'),
(441, 52, 'fifu_image_alt', '7. Park Hye-won (HYYN)’s NEW SONG ‘한 번만 내 마음대로 하자’ㅣOasis LIVEㅣ'),
(442, 52, '_thumbnail_id', '70'),
(443, 70, '_wp_attached_file', 'https://hivepress.io/wp-content/uploads/2021/01/logo.svg'),
(444, 70, '_wp_attachment_image_alt', '7. Park Hye-won (HYYN)’s NEW SONG ‘한 번만 내 마음대로 하자’ㅣOasis LIVEㅣ'),
(445, 70, '_wp_attachment_metadata', 'a:2:{s:5:\"width\";s:3:\"186\";s:6:\"height\";s:2:\"40\";}'),
(446, 71, '_vc_post_settings', 'a:1:{s:10:\"vc_grid_id\";a:0:{}}'),
(447, 71, 'like_key', '1'),
(467, 74, '_vc_post_settings', 'a:1:{s:10:\"vc_grid_id\";a:0:{}}'),
(468, 74, '_edit_lock', '1619687531:1'),
(481, 52, '_wp_old_slug', '7-park-hye-won-hyyns-new-song-%ed%95%9c-%eb%b2%88%eb%a7%8c-%eb%82%b4-%eb%a7%88%ec%9d%8c%eb%8c%80%eb%a1%9c-%ed%95%98%ec%9e%90%e3%85%a3oasis-live%e3%85%a3'),
(483, 52, '_yoast_wpseo_estimated-reading-time-minutes', ''),
(484, 51, 'count_viewed', '5'),
(485, 76, 'videotube_post_type', 'post'),
(486, 76, '_videotube_post_type', 'field_535765e9e7089'),
(487, 2, '_yoast_wpseo_estimated-reading-time-minutes', '0'),
(488, 51, '_yoast_wpseo_primary_categories', '24'),
(489, 51, '_yoast_wpseo_content_score', '30'),
(490, 51, '_yoast_wpseo_estimated-reading-time-minutes', ''),
(491, 50, '_yoast_wpseo_primary_categories', '22'),
(492, 50, '_yoast_wpseo_content_score', '90'),
(493, 50, '_yoast_wpseo_estimated-reading-time-minutes', ''),
(494, 49, '_yoast_wpseo_primary_categories', '22'),
(495, 49, '_yoast_wpseo_content_score', '90'),
(496, 49, '_yoast_wpseo_estimated-reading-time-minutes', ''),
(497, 49, '_thumbnail_id', '53'),
(498, 48, '_yoast_wpseo_primary_categories', '22'),
(499, 48, '_yoast_wpseo_content_score', '90'),
(500, 48, '_yoast_wpseo_estimated-reading-time-minutes', ''),
(501, 47, '_yoast_wpseo_primary_categories', '22'),
(502, 47, '_yoast_wpseo_content_score', '90'),
(503, 47, '_yoast_wpseo_estimated-reading-time-minutes', ''),
(504, 22, '_yoast_wpseo_primary_categories', '22'),
(505, 22, '_yoast_wpseo_estimated-reading-time-minutes', ''),
(506, 64, '_edit_lock', '1620101300:1'),
(507, 77, 'like_key', '1'),
(508, 78, 'like_key', '1'),
(509, 52, '_yoast_wpseo_primary_video_tag', '10'),
(510, 52, '_yoast_wpseo_content_score', '30'),
(511, 51, '_yoast_wpseo_primary_video_tag', '10'),
(512, 47, 'count_viewed', '1'),
(513, 79, 'like_key', '1'),
(514, 79, '_edit_last', '1'),
(515, 79, '_edit_lock', '1620116874:1'),
(516, 79, 'layout', 'small'),
(517, 79, '_layout', 'field_531980e906752'),
(518, 79, 'video_type', 'normal'),
(519, 79, '_video_type', 'field_53eb79f33936e'),
(520, 79, 'video_url', ''),
(521, 79, '_video_url', 'field_53eb7a453936f'),
(522, 79, '_yoast_wpseo_estimated-reading-time-minutes', ''),
(523, 79, 'fifu_image_url', 'https://wpastra.com/wp-content/uploads/2019/05/astra-logo.svg'),
(524, 79, 'fifu_image_alt', 'test'),
(525, 79, '_thumbnail_id', '80'),
(526, 80, '_wp_attached_file', 'https://wpastra.com/wp-content/uploads/2019/05/astra-logo.svg'),
(527, 80, '_wp_attachment_image_alt', 'test'),
(528, 80, '_wp_attachment_metadata', 'a:2:{s:5:\"width\";s:3:\"155\";s:6:\"height\";s:2:\"45\";}'),
(529, 79, '_wp_trash_meta_status', 'publish'),
(530, 79, '_wp_trash_meta_time', '1620117017'),
(531, 79, '_wp_desired_post_slug', 'test'),
(532, 81, 'like_key', '1'),
(533, 81, '_edit_last', '1'),
(534, 81, '_edit_lock', '1620117097:1'),
(535, 81, 'layout', 'small'),
(536, 81, '_layout', 'field_531980e906752'),
(537, 81, 'video_type', 'normal'),
(538, 81, '_video_type', 'field_53eb79f33936e'),
(539, 81, 'video_url', 'test video'),
(540, 81, '_video_url', 'field_53eb7a453936f'),
(541, 81, 'fifu_image_url', 'https://wpastra.com/wp-content/uploads/2019/05/astra-logo.svg'),
(542, 81, 'fifu_image_alt', 'test video'),
(543, 81, '_thumbnail_id', '82'),
(544, 82, '_wp_attached_file', 'https://wpastra.com/wp-content/uploads/2019/05/astra-logo.svg'),
(545, 82, '_wp_attachment_image_alt', 'test video'),
(546, 82, '_wp_attachment_metadata', 'a:2:{s:5:\"width\";s:3:\"155\";s:6:\"height\";s:2:\"45\";}'),
(547, 81, '_yoast_wpseo_content_score', '60'),
(548, 81, '_yoast_wpseo_estimated-reading-time-minutes', ''),
(549, 83, 'like_key', '1'),
(550, 84, 'like_key', '1'),
(551, 85, 'like_key', '1'),
(552, 85, '_edit_last', '1'),
(553, 85, '_edit_lock', '1620118773:1'),
(554, 85, 'layout', 'small'),
(555, 85, '_layout', 'field_531980e906752'),
(556, 85, 'video_type', 'normal'),
(557, 85, '_video_type', 'field_53eb79f33936e'),
(558, 85, 'video_url', 'test add image'),
(559, 85, '_video_url', 'field_53eb7a453936f'),
(560, 85, 'fifu_image_url', 'https://wpastra.com/wp-content/uploads/2019/05/astra-logo.svg'),
(561, 85, 'fifu_image_alt', 'test add image'),
(562, 85, '_thumbnail_id', '86'),
(563, 86, '_wp_attached_file', 'https://wpastra.com/wp-content/uploads/2019/05/astra-logo.svg'),
(564, 86, '_wp_attachment_image_alt', 'test add image'),
(565, 86, '_wp_attachment_metadata', 'a:2:{s:5:\"width\";s:3:\"155\";s:6:\"height\";s:2:\"45\";}'),
(566, 85, 'count_viewed', '1'),
(567, 85, '_wp_trash_meta_status', 'publish'),
(568, 85, '_wp_trash_meta_time', '1620118917'),
(569, 85, '_wp_desired_post_slug', 'test-add-image'),
(570, 81, '_wp_trash_meta_status', 'publish'),
(571, 81, '_wp_trash_meta_time', '1620118920'),
(572, 81, '_wp_desired_post_slug', 'test-video'),
(573, 48, 'count_viewed', '1'),
(574, 87, 'like_key', '1'),
(575, 46, 'fifu_image_url', 'https://wpastra.com/wp-content/uploads/2019/05/astra-logo.svg'),
(576, 46, 'fifu_image_alt', 'Movies 03'),
(577, 46, '_thumbnail_id', '88'),
(578, 88, '_wp_attached_file', 'https://wpastra.com/wp-content/uploads/2019/05/astra-logo.svg'),
(579, 88, '_wp_attachment_image_alt', 'Movies 03'),
(580, 88, '_wp_attachment_metadata', 'a:2:{s:5:\"width\";s:3:\"155\";s:6:\"height\";s:2:\"45\";}'),
(581, 92, '_menu_item_type', 'post_type'),
(582, 92, '_menu_item_menu_item_parent', '0'),
(583, 92, '_menu_item_object_id', '90'),
(584, 92, '_menu_item_object', 'language_switcher'),
(585, 92, '_menu_item_target', ''),
(586, 92, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(587, 92, '_menu_item_xfn', ''),
(588, 92, '_menu_item_url', ''),
(589, 92, '_menu_item_orphaned', '1620190764'),
(590, 93, '_menu_item_type', 'post_type'),
(591, 93, '_menu_item_menu_item_parent', '0'),
(592, 93, '_menu_item_object_id', '89'),
(593, 93, '_menu_item_object', 'language_switcher'),
(594, 93, '_menu_item_target', ''),
(595, 93, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(596, 93, '_menu_item_xfn', ''),
(597, 93, '_menu_item_url', ''),
(598, 93, '_menu_item_orphaned', '1620190764'),
(599, 94, 'like_key', '1'),
(600, 95, 'like_key', '1'),
(601, 96, 'like_key', '1'),
(602, 97, 'like_key', '1'),
(603, 98, 'like_key', '1'),
(604, 100, 'like_key', '1'),
(605, 101, 'like_key', '1'),
(606, 102, 'like_key', '1'),
(607, 103, 'like_key', '1'),
(608, 104, 'like_key', '1'),
(609, 106, 'like_key', '1'),
(610, 107, 'like_key', '1'),
(611, 108, 'like_key', '1'),
(612, 51, 'title_en', 'title eng 6Park'),
(613, 51, '_title_en', 'field_title_video_en'),
(614, 51, 'title_jp', 'title jp 1'),
(615, 51, '_title_jp', 'field_title_video_jp'),
(616, 51, 'title_cn', ''),
(617, 51, '_title_cn', 'field_title_video_cn'),
(627, 12, '_wp_old_date', '2021-04-29'),
(628, 58, '_wp_old_date', '2021-04-29'),
(629, 55, '_wp_old_date', '2021-04-29'),
(630, 60, '_wp_old_date', '2021-04-29'),
(631, 54, '_wp_old_date', '2021-04-29'),
(632, 56, '_wp_old_date', '2021-04-29'),
(633, 59, '_wp_old_date', '2021-04-29'),
(634, 57, '_wp_old_date', '2021-04-29'),
(635, 67, '_wp_old_date', '2021-04-29'),
(636, 68, '_wp_old_date', '2021-04-29'),
(637, 69, '_wp_old_date', '2021-04-29');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `wp_posts`
--

CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `guid` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `menu_order` int(11) NOT NULL DEFAULT 0,
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2021-04-27 13:58:06', '2021-04-27 13:58:06', '<!-- wp:paragraph -->\n<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\n<!-- /wp:paragraph -->', 'Hello world!', '', 'trash', 'open', 'open', '', 'hello-world__trashed', '', '', '2021-04-27 15:55:53', '2021-04-27 15:55:53', '', 0, 'http://localhost/elabo/?p=1', 0, 'post', '', 1),
(2, 1, '2021-04-27 13:58:06', '2021-04-27 13:58:06', '', 'Home Page', '', 'publish', 'closed', 'open', '', 'home-page', '', '', '2021-04-29 09:53:53', '2021-04-29 09:53:53', '', 0, 'http://localhost/elabo/?page_id=2', 0, 'page', '', 0),
(3, 1, '2021-04-27 13:58:06', '2021-04-27 13:58:06', '<!-- wp:heading --><h2>Who we are</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>Our website address is: http://localhost/elabo.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Comments</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Media</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Cookies</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you visit our login page, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Embedded content from other websites</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Who we share your data with</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>If you request a password reset, your IP address will be included in the reset email.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>How long we retain your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What rights you have over your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Where we send your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>Visitor comments may be checked through an automated spam detection service.</p><!-- /wp:paragraph -->', 'Privacy Policy', '', 'draft', 'closed', 'open', '', 'privacy-policy', '', '', '2021-04-27 13:58:06', '2021-04-27 13:58:06', '', 0, 'http://localhost/elabo/?page_id=3', 0, 'page', '', 0),
(4, 1, '2021-04-27 13:58:19', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2021-04-27 13:58:19', '0000-00-00 00:00:00', '', 0, 'http://localhost/elabo/?p=4', 0, 'post', '', 0),
(12, 1, '2021-05-05 07:17:09', '2021-04-27 15:11:41', '', 'Home', '', 'publish', 'closed', 'closed', '', '12', '', '', '2021-05-05 07:17:09', '2021-05-05 07:17:09', '', 0, 'http://localhost/elabo/?p=12', 1, 'nav_menu_item', '', 0),
(13, 1, '2021-04-27 15:12:49', '2021-04-27 15:12:49', '<!-- wp:paragraph -->\n<p>This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like piña coladas. (And gettin\' caught in the rain.)</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...or something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>As a new WordPress user, you should go to <a href=\"http://localhost/elabo/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!</p>\n<!-- /wp:paragraph -->', 'Home Page', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2021-04-27 15:12:49', '2021-04-27 15:12:49', '', 2, 'http://localhost/elabo/?p=13', 0, 'revision', '', 0),
(14, 1, '2021-04-27 15:24:40', '2021-04-27 15:24:40', '<h1 class=\"VideoInfoTitle__videoTitle___UDnVC\" title=\"What if JeA is the professor at the practical music interview!?Real tipsㅣJeA Lalala EP.10ㅣ\">What if JeA is the professor at the practical music interview!?Real tipsㅣJeA Lalala EP.10</h1>', 'What if JeA is the professor at the practical music interview!?Real tipsㅣJeA Lalala EP.10', '', 'publish', 'open', 'closed', '', 'what-if-jea-is-the-professor-at-the-practical-music-interviewreal-tips%e3%85%a3jea-lalala-ep-10', '', '', '2021-04-27 15:37:55', '2021-04-27 15:37:55', '', 0, 'http://localhost/elabo/?post_type=video&#038;p=14', 0, 'video', '', 0),
(15, 1, '2021-04-27 15:24:16', '2021-04-27 15:24:16', '', 'JeALalala', '', 'inherit', 'open', 'closed', '', 'jealalala', '', '', '2021-04-27 15:24:16', '2021-04-27 15:24:16', '', 14, 'http://localhost/elabo/wp-content/uploads/2021/04/JeALalala.jpg', 0, 'attachment', 'image/jpeg', 0),
(16, 1, '2021-04-27 15:42:06', '2021-04-27 15:42:06', '{\n    \"blogdescription\": {\n        \"value\": \"\",\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2021-04-27 15:42:06\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'dd3bc265-2498-4c8e-858b-99d63fe9a025', '', '', '2021-04-27 15:42:06', '2021-04-27 15:42:06', '', 0, 'http://localhost/elabo/dd3bc265-2498-4c8e-858b-99d63fe9a025/', 0, 'customize_changeset', '', 0),
(17, 1, '2021-04-27 15:55:53', '2021-04-27 15:55:53', '<!-- wp:paragraph -->\n<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\n<!-- /wp:paragraph -->', 'Hello world!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2021-04-27 15:55:53', '2021-04-27 15:55:53', '', 1, 'http://localhost/elabo/?p=17', 0, 'revision', '', 0),
(18, 1, '2021-04-27 16:00:33', '2021-04-27 16:00:33', 'HYNN Park Hye-won\'s new song is crazy.\r\nI know she is a good singer, but this new song expresses her feelings so well.\r\nHow many times have you said good-bye? Please don\'t let her break up.\r\nAll the staff got goosebumps when we heard the live.\r\n\r\n#HYYN #박예원 #한_번만_내_마음대로_하자', 'Park Hye-won (HYYN)\'s NEW SONG \'한 번만 내 마음대로 하자\'ㅣOasis LIVEㅣ', '', 'publish', 'open', 'closed', '', 'park-hye-won-hyyns-new-song-%ed%95%9c-%eb%b2%88%eb%a7%8c-%eb%82%b4-%eb%a7%88%ec%9d%8c%eb%8c%80%eb%a1%9c-%ed%95%98%ec%9e%90%e3%85%a3oasis-live%e3%85%a3', '', '', '2021-04-27 16:00:33', '2021-04-27 16:00:33', '', 0, 'http://localhost/elabo/?post_type=video&#038;p=18', 0, 'video', '', 0),
(19, 1, '2021-04-27 16:00:13', '2021-04-27 16:00:13', '', '580eb99da440318419e445bb83d884937c089497r1_647_1018v2_uhq', '', 'inherit', 'open', 'closed', '', '580eb99da440318419e445bb83d884937c089497r1_647_1018v2_uhq', '', '', '2021-04-28 10:14:28', '2021-04-28 10:14:28', '', 18, 'http://localhost/elabo/wp-content/uploads/2021/04/580eb99da440318419e445bb83d884937c089497r1_647_1018v2_uhq.jpg', 0, 'attachment', 'image/jpeg', 0),
(20, 1, '2021-04-27 16:20:35', '2021-04-27 16:20:35', '<p>khaile</p>\r\nK-pop Idol doing FREAKIN\' MEDICAL CHECK _ GWSN\r\nGWSN is promoting with their new song BAZOOKA!\r\nA medical checkup service for them to promote healthily!\r\n\r\nBut the checkups aren\'t what they used to know! LOL\r\nWhich member gets their name on the Hall of Fame for King Healthy Idols?!\r\nCheck out the members\' fill-out form information on the Community tab!\r\n\r\n▶ Please subscribe and enable notifications <a href=\"http://bit.ly/2RpCcV1\" target=\"_blank\" rel=\"nofollow noopener\">http://bit.ly/2RpCcV1</a>\r\n\r\nGot Mental Breakdown While Getting Crazy Medical Checkups!girls nearly fought with their manager during the checkup haha ㅣMy Idol\'s Medical Checkup | GWSN', 'Got Mental Breakdown While Getting Crazy Medical Checkups!ㅣMy Idol\'s Medical Checkup | GWSN', '', 'publish', 'open', 'closed', '', 'got-mental-breakdown-while-getting-crazy-medical-checkups%e3%85%a3my-idols-medical-checkup-gwsn', '', '', '2021-04-28 03:35:56', '2021-04-28 03:35:56', '', 0, 'http://localhost/elabo/?post_type=video&#038;p=20', 0, 'video', '', 0),
(21, 1, '2021-04-27 16:20:26', '2021-04-27 16:20:26', '', 'f531fec46cb7475ab03bb412c1344f77', '', 'inherit', 'open', 'closed', '', 'f531fec46cb7475ab03bb412c1344f77', '', '', '2021-04-28 10:15:00', '2021-04-28 10:15:00', '', 20, 'http://localhost/elabo/wp-content/uploads/2021/04/f531fec46cb7475ab03bb412c1344f77.jpg', 0, 'attachment', 'image/jpeg', 0),
(22, 1, '2021-04-27 16:24:18', '2021-04-27 16:24:18', '', '4-14天崩开局！国服阿轲自带bug级高能四杀翻盘！', '', 'publish', 'open', 'closed', '', '4-14%e5%a4%a9%e5%b4%a9%e5%bc%80%e5%b1%80%ef%bc%81%e5%9b%bd%e6%9c%8d%e9%98%bf%e8%bd%b2%e8%87%aa%e5%b8%a6bug%e7%ba%a7%e9%ab%98%e8%83%bd%e5%9b%9b%e6%9d%80%e7%bf%bb%e7%9b%98%ef%bc%81', '', '', '2021-05-04 03:11:30', '2021-05-04 03:11:30', '', 0, 'http://localhost/elabo/?post_type=video&#038;p=22', 0, 'video', '', 0),
(23, 1, '2021-04-27 16:24:10', '2021-04-27 16:24:10', '', 'img-share', '', 'inherit', 'open', 'closed', '', 'img-share', '', '', '2021-04-27 16:24:10', '2021-04-27 16:24:10', '', 22, 'http://localhost/elabo/wp-content/uploads/2021/04/img-share.jpg', 0, 'attachment', 'image/jpeg', 0),
(24, 1, '2021-04-27 16:54:56', '2021-04-27 16:54:56', '{\n    \"videotube::background_color\": {\n        \"value\": \"#0a0a0a\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2021-04-27 16:54:56\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '9138f5c0-8a73-45b9-9511-a43d7f26f9cf', '', '', '2021-04-27 16:54:56', '2021-04-27 16:54:56', '', 0, 'http://localhost/elabo/9138f5c0-8a73-45b9-9511-a43d7f26f9cf/', 0, 'customize_changeset', '', 0),
(25, 1, '2021-04-27 17:03:42', '2021-04-27 17:03:42', '{\n    \"videotube::background_color\": {\n        \"value\": \"#efefef\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2021-04-27 17:03:42\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'f1a19878-b987-412b-a591-d9305e681ebb', '', '', '2021-04-27 17:03:42', '2021-04-27 17:03:42', '', 0, 'http://localhost/elabo/f1a19878-b987-412b-a591-d9305e681ebb/', 0, 'customize_changeset', '', 0),
(26, 1, '2021-04-27 17:04:01', '2021-04-27 17:04:01', '{\n    \"videotube::background_color\": {\n        \"value\": \"#0a0a0a\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2021-04-27 17:04:01\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'cb0c05f3-48e0-4b39-8cda-b3477fca32dc', '', '', '2021-04-27 17:04:01', '2021-04-27 17:04:01', '', 0, 'http://localhost/elabo/cb0c05f3-48e0-4b39-8cda-b3477fca32dc/', 0, 'customize_changeset', '', 0),
(27, 1, '2021-04-28 02:24:46', '2021-04-28 02:24:46', '', 'Movie 001', '', 'publish', 'open', 'closed', '', 'movie-001', '', '', '2021-04-28 06:45:33', '2021-04-28 06:45:33', '', 0, 'http://localhost/elabo/?post_type=video&#038;p=27', 0, 'video', '', 0),
(29, 1, '2021-04-28 02:24:15', '2021-04-28 02:24:15', '', 'pc_login', '', 'inherit', 'open', 'closed', '', 'pc_login', '', '', '2021-04-28 02:24:15', '2021-04-28 02:24:15', '', 27, 'http://localhost/elabo/wp-content/uploads/2021/04/pc_login.jpg', 0, 'attachment', 'image/jpeg', 0),
(30, 1, '2021-04-28 02:32:08', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2021-04-28 02:32:08', '0000-00-00 00:00:00', '', 0, 'http://localhost/elabo/?post_type=video&p=30', 0, 'video', '', 0),
(33, 1, '2021-04-28 02:57:11', '2021-04-28 02:57:11', 'K-pop Idol doing FREAKIN\' MEDICAL CHECK _ GWSN\r\nGWSN is promoting with their new song BAZOOKA!\r\nA medical checkup service for them to promote healthily!\r\n\r\nBut the checkups aren\'t what they used to know! LOL\r\nWhich member gets their name on the Hall of Fame for King Healthy Idols?!\r\nCheck out the members\' fill-out form information on the Community tab!\r\n\r\n Please subscribe and enable notifications http://bit.ly/2RpCcV1\r\n\r\nGot Mental Breakdown While Getting Crazy Medical Checkups!girls nearly fought with their manager during the checkup haha ㅣMy Idol\'s Medical Checkup | GWSN', 'Movie002', '', 'publish', 'open', 'closed', '', 'movie002', '', '', '2021-04-28 07:04:31', '2021-04-28 07:04:31', '', 0, 'http://localhost/elabo/?post_type=video&#038;p=33', 0, 'video', '', 0),
(38, 1, '2021-04-28 03:35:24', '2021-04-28 03:35:24', 'K-pop Idol doing FREAKIN\' MEDICAL CHECK _ GWSN\nGWSN is promoting with their new song BAZOOKA!\nA medical checkup service for them to promote healthily!\n\nBut the checkups aren\'t what they used to know! LOL\nWhich member gets their name on the Hall of Fame for King Healthy Idols?!\nCheck out the members\' fill-out form information on the Community tab!\n\n▶ Please subscribe and enable notifications <a href=\"http://bit.ly/2RpCcV1\" target=\"_blank\" rel=\"nofollow noopener\">http://bit.ly/2RpCcV1</a>\n\nGot Mental Breakdown While Getting Crazy Medical Checkups!girls nearly fought with their manager during the checkup haha ㅣMy Idol\'s Medical Checkup | GWSN', 'Movie002', '', 'inherit', 'closed', 'closed', '', '33-autosave-v1', '', '', '2021-04-28 03:35:24', '2021-04-28 03:35:24', '', 33, 'http://localhost/elabo/?p=38', 0, 'revision', '', 0),
(39, 1, '2021-04-28 03:34:33', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2021-04-28 03:34:33', '0000-00-00 00:00:00', '', 0, 'http://localhost/elabo/?p=39', 0, 'post', '', 0),
(40, 1, '2021-04-28 03:35:52', '2021-04-28 03:35:52', '<p>khaile</p>\nK-pop Idol doing FREAKIN\' MEDICAL CHECK _ GWSN\nGWSN is promoting with their new song BAZOOKA!\nA medical checkup service for them to promote healthily!\n\nBut the checkups aren\'t what they used to know! LOL\nWhich member gets their name on the Hall of Fame for King Healthy Idols?!\nCheck out the members\' fill-out form information on the Community tab!\n\n▶ Please subscribe and enable notifications <a href=\"http://bit.ly/2RpCcV1\" target=\"_blank\" rel=\"nofollow noopener\">http://bit.ly/2RpCcV1</a>\n\nGot Mental Breakdown While Getting Crazy Medical Checkups!girls nearly fought with their manager during the checkup haha ㅣMy Idol\'s Medical Checkup | GWSN', 'Got Mental Breakdown While Getting Crazy Medical Checkups!ㅣMy Idol\'s Medical Checkup | GWSN', '', 'inherit', 'closed', 'closed', '', '20-autosave-v1', '', '', '2021-04-28 03:35:52', '2021-04-28 03:35:52', '', 20, 'http://localhost/elabo/?p=40', 0, 'revision', '', 0),
(41, 1, '2021-04-28 03:42:31', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2021-04-28 03:42:31', '0000-00-00 00:00:00', '', 0, 'http://localhost/elabo/?post_type=video&p=41', 0, 'video', '', 0),
(42, 1, '2021-04-28 03:42:37', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2021-04-28 03:42:37', '0000-00-00 00:00:00', '', 0, 'http://localhost/elabo/?post_type=video&p=42', 0, 'video', '', 0),
(46, 1, '2021-04-28 07:22:16', '2021-04-28 07:22:16', '', 'Movies 03', '', 'publish', 'closed', 'closed', '', 'movies-03', '', '', '2021-05-05 02:13:43', '2021-05-05 02:13:43', '', 0, 'http://localhost/elabo/?post_type=video&#038;p=46', 0, 'video', '', 0),
(47, 1, '2021-04-28 08:25:07', '2021-04-28 08:25:07', 'HYNN Park Hye-won\'s new song is crazy.\r\nI know she is a good singer, but this new song expresses her feelings so well.\r\nHow many times have you said good-bye? Please don\'t let her break up.\r\nAll the staff got goosebumps when we heard the live.\r\n\r\n#HYYN #박예원 #한_번만_내_마음대로_하자', '2. Park Hye-won (HYYN)\'s NEW SONG \'한 번만 내 마음대로 하자\'ㅣOasis LIVEㅣ', '', 'publish', 'open', 'closed', '', '2-park-hye-won-hyyns-new-song-%ed%95%9c-%eb%b2%88%eb%a7%8c-%eb%82%b4-%eb%a7%88%ec%9d%8c%eb%8c%80%eb%a1%9c-%ed%95%98%ec%9e%90%e3%85%a3oasis-live%e3%85%a3', '', '', '2021-05-04 03:11:11', '2021-05-04 03:11:11', '', 0, 'http://localhost/elabo/?post_type=video&#038;p=47', 0, 'video', '', 0),
(48, 1, '2021-04-28 08:25:43', '2021-04-28 08:25:43', 'HYNN Park Hye-won\'s new song is crazy.\r\nI know she is a good singer, but this new song expresses her feelings so well.\r\nHow many times have you said good-bye? Please don\'t let her break up.\r\nAll the staff got goosebumps when we heard the live.\r\n\r\n#HYYN #박예원 #한_번만_내_마음대로_하자', '3. Park Hye-won (HYYN)\'s NEW SONG \'한 번만 내 마음대로 하자\'ㅣOasis LIVEㅣ', '', 'publish', 'open', 'closed', '', '3-park-hye-won-hyyns-new-song-%ed%95%9c-%eb%b2%88%eb%a7%8c-%eb%82%b4-%eb%a7%88%ec%9d%8c%eb%8c%80%eb%a1%9c-%ed%95%98%ec%9e%90%e3%85%a3oasis-live%e3%85%a3', '', '', '2021-05-04 03:11:01', '2021-05-04 03:11:01', '', 0, 'http://localhost/elabo/?post_type=video&#038;p=48', 0, 'video', '', 0),
(49, 1, '2021-04-28 08:26:07', '2021-04-28 08:26:07', 'HYNN Park Hye-won\'s new song is crazy.\r\nI know she is a good singer, but this new song expresses her feelings so well.\r\nHow many times have you said good-bye? Please don\'t let her break up.\r\nAll the staff got goosebumps when we heard the live.\r\n\r\n#HYYN #박예원 #한_번만_내_마음대로_하자', '4. Park Hye-won (HYYN)\'s NEW SONG \'한 번만 내 마음대로 하자\'ㅣOasis LIVEㅣ', '', 'publish', 'open', 'closed', '', '4-park-hye-won-hyyns-new-song-%ed%95%9c-%eb%b2%88%eb%a7%8c-%eb%82%b4-%eb%a7%88%ec%9d%8c%eb%8c%80%eb%a1%9c-%ed%95%98%ec%9e%90%e3%85%a3oasis-live%e3%85%a3', '', '', '2021-05-04 03:10:16', '2021-05-04 03:10:16', '', 0, 'http://localhost/elabo/?post_type=video&#038;p=49', 0, 'video', '', 0),
(50, 1, '2021-04-28 08:26:34', '2021-04-28 08:26:34', 'HYNN Park Hye-won\'s new song is crazy.\r\nI know she is a good singer, but this new song expresses her feelings so well.\r\nHow many times have you said good-bye? Please don\'t let her break up.\r\nAll the staff got goosebumps when we heard the live.\r\n\r\n#HYYN #박예원 #한_번만_내_마음대로_하자', '5. Park Hye-won (HYYN)\'s NEW SONG \'한 번만 내 마음대로 하자\'ㅣOasis LIVEㅣ', '', 'publish', 'open', 'closed', '', '5-park-hye-won-hyyns-new-song-%ed%95%9c-%eb%b2%88%eb%a7%8c-%eb%82%b4-%eb%a7%88%ec%9d%8c%eb%8c%80%eb%a1%9c-%ed%95%98%ec%9e%90%e3%85%a3oasis-live%e3%85%a3', '', '', '2021-05-04 03:09:23', '2021-05-04 03:09:23', '', 0, 'http://localhost/elabo/?post_type=video&#038;p=50', 0, 'video', '', 0),
(51, 1, '2021-04-29 08:27:17', '2021-04-28 08:27:17', '', '6. Park Hye-won (HYYN)\'s NEW SONG \'한 번만 내 마음대로 하자\'ㅣOasis LIVEㅣ', '', 'publish', 'open', 'closed', '', '6-park-hye-won-hyyns-new-song-%ed%95%9c-%eb%b2%88%eb%a7%8c-%eb%82%b4-%eb%a7%88%ec%9d%8c%eb%8c%80%eb%a1%9c-%ed%95%98%ec%9e%90%e3%85%a3oasis-live%e3%85%a3', '', '', '2021-05-05 06:51:50', '2021-05-05 06:51:50', '', 0, 'http://localhost/elabo/?post_type=video&#038;p=51', 0, 'video', '', 0),
(52, 1, '2021-04-28 08:27:48', '2021-04-28 08:27:48', '', '7. Park Hye-won (HYYN)\'s NEW SONG \'한 번만 내 마음대로 하자\'ㅣOasis LIVEㅣ', '', 'publish', 'open', 'closed', '', '%ed%95%9c-%eb%b2%88%eb%a7%8c-%eb%82%b4-%eb%a7%88%ec%9d%8c%eb%8c%80%eb%a1%9c-%ed%95%98%ec%9e%90', '', '', '2021-05-04 06:51:50', '2021-05-04 06:51:50', '', 0, 'http://localhost/elabo/?post_type=video&#038;p=52', 0, 'video', '', 0),
(53, 1, '2021-04-28 10:07:23', '2021-04-28 10:07:23', '', 'no_image_available', '', 'inherit', 'open', 'closed', '', 'no_image_available', '', '', '2021-04-28 10:07:23', '2021-04-28 10:07:23', '', 0, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/no_image_available.jpg', 0, 'attachment', 'image/jpeg', 0),
(54, 1, '2021-05-05 07:17:09', '2021-04-28 16:44:31', ' ', '', '', 'publish', 'closed', 'closed', '', '54', '', '', '2021-05-05 07:17:09', '2021-05-05 07:17:09', '', 0, 'http://localhost/elabo/?p=54', 5, 'nav_menu_item', '', 0),
(55, 1, '2021-05-05 07:17:09', '2021-04-28 16:44:31', ' ', '', '', 'publish', 'closed', 'closed', '', '55', '', '', '2021-05-05 07:17:09', '2021-05-05 07:17:09', '', 0, 'http://localhost/elabo/?p=55', 3, 'nav_menu_item', '', 0),
(56, 1, '2021-05-05 07:17:09', '2021-04-28 16:44:31', ' ', '', '', 'publish', 'closed', 'closed', '', '56', '', '', '2021-05-05 07:17:09', '2021-05-05 07:17:09', '', 0, 'http://localhost/elabo/?p=56', 6, 'nav_menu_item', '', 0),
(57, 1, '2021-05-05 07:17:09', '2021-04-28 16:44:32', ' ', '', '', 'publish', 'closed', 'closed', '', '57', '', '', '2021-05-05 07:17:09', '2021-05-05 07:17:09', '', 0, 'http://localhost/elabo/?p=57', 8, 'nav_menu_item', '', 0),
(58, 1, '2021-05-05 07:17:09', '2021-04-28 16:44:31', ' ', '', '', 'publish', 'closed', 'closed', '', '58', '', '', '2021-05-05 07:17:09', '2021-05-05 07:17:09', '', 0, 'http://localhost/elabo/?p=58', 2, 'nav_menu_item', '', 0),
(59, 1, '2021-05-05 07:17:09', '2021-04-28 16:44:32', ' ', '', '', 'publish', 'closed', 'closed', '', '59', '', '', '2021-05-05 07:17:09', '2021-05-05 07:17:09', '', 0, 'http://localhost/elabo/?p=59', 7, 'nav_menu_item', '', 0),
(60, 1, '2021-05-05 07:17:09', '2021-04-28 16:44:31', ' ', '', '', 'publish', 'closed', 'closed', '', '60', '', '', '2021-05-05 07:17:09', '2021-05-05 07:17:09', '', 0, 'http://localhost/elabo/?p=60', 4, 'nav_menu_item', '', 0),
(64, 1, '2021-04-29 02:17:46', '0000-00-00 00:00:00', '<!-- wp:paragraph -->\n<p>\n  admin created this sample page in the Setup Wizard of the <b>Tag Groups</b> plugin. You can safely edit and delete it or keep it for future reference.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>\n  The following blocks use a variety of parameters so that you can get an idea of the options. Feel free to generate a new sample page with more features after upgrading the plugin.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>\n  This page was created while you had the Gutenberg editor enabled. Therefore the features only display while Gutenberg is active.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>\n  Please find links to the documentation in the <a href=\"http://192.168.1.157/elabo/wp-admin/admin.php?page=tag-groups-settings\">Tag Groups settings</a>.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:separator -->\n<hr class=\"wp-block-separator\"/>\n<!-- /wp:separator -->\n\n<!-- wp:heading -->\n<h2>Tabbed Tag Cloud</h2>\n<!-- /wp:heading -->\n\n<!-- wp:chatty-mango/tag-groups-cloud-tabs {\"source\":\"gutenberg\",\"append\":\"{count}\",\"custom_title\":\"We found {count} post(s) for this tag.\",\"hide_empty\":0,\"hide_empty_tabs\":1} /-->\n\n<!-- wp:separator -->\n<hr class=\"wp-block-separator\"/>\n<!-- /wp:separator -->\n\n<!-- wp:heading -->\n<h2>Accordion Tag Cloud</h2>\n<!-- /wp:heading -->\n\n<!-- wp:chatty-mango/tag-groups-cloud-accordion {\"source\":\"gutenberg\",\"hide_empty\":0,\"hide_empty_content\":1,\"mouseover\":1,\"prepend\":\"#\",\"separator\":\"|\"} /-->\n\n<!-- wp:separator -->\n<hr class=\"wp-block-separator\"/>\n<!-- /wp:separator -->\n\n<!-- wp:heading -->\n<h2>Alphabetical Tag Cloud</h2>\n<!-- /wp:heading -->\n\n<!-- wp:chatty-mango/tag-groups-alphabet-tabs {\"source\":\"gutenberg\",\"exclude_letters\":\"äöüß\"} /-->\n\n<!-- wp:separator -->\n<hr class=\"wp-block-separator\"/>\n<!-- /wp:separator -->\n\n<!-- wp:heading -->\n<h2>Tag List</h2>\n<!-- /wp:heading -->\n\n<!-- wp:chatty-mango/tag-groups-tag-list {\"source\":\"gutenberg\",\"append\":\") : ({count})\",\"column_count\":3,\"hide_empty\":0,\"largest\":18,\"prepend\":\"(\",\"smallest\":14} /-->\n\n<!-- wp:separator -->\n<hr class=\"wp-block-separator\"/>\n<!-- /wp:separator -->\n\n<!-- wp:heading -->\n<h2>Alphabetical Tag Index</h2>\n<!-- /wp:heading -->\n\n<!-- wp:chatty-mango/tag-groups-alphabetical-tag-index {\"source\":\"gutenberg\",\"exclude_letters\":\"0123456789\",\"hide_empty\":0,\"largest\":24,\"smallest\":16} /-->\n\n<!-- wp:separator -->\n<hr class=\"wp-block-separator\"/>\n<!-- /wp:separator -->\n\n<!-- wp:paragraph -->\n<p>Created by <a href=\"https://chattymango.com/tag-groups/\" target=\"_blank\" rel=\"noopener\">Chatty Mango\'s Tag Groups plugin</a></p>\n<!-- /wp:paragraph -->\n', 'Tag Groups (Free) Sample Page - Gutenberg Editor', '', 'draft', 'closed', 'closed', '', '', '', '', '2021-04-29 02:17:46', '0000-00-00 00:00:00', '', 0, 'http://192.168.1.157/elabo/?page_id=64', 0, 'page', '', 0),
(65, 1, '2021-04-29 02:33:38', '2021-04-29 02:33:38', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:8:\"taxonomy\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:9:\"video_tag\";}}}s:8:\"position\";s:15:\"acf_after_title\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Parent tag', 'parent-tag', 'trash', 'closed', 'closed', '', 'group_608a1a93131ca__trashed', '', '', '2021-04-29 02:41:20', '2021-04-29 02:41:20', '', 0, 'http://192.168.1.157/elabo/?post_type=acf-field-group&#038;p=65', 0, 'acf-field-group', '', 0),
(66, 1, '2021-04-29 02:33:38', '2021-04-29 02:33:38', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'prent tag', 'prent_tag', 'trash', 'closed', 'closed', '', 'field_608a1ab2f2f97__trashed', '', '', '2021-04-29 02:41:20', '2021-04-29 02:41:20', '', 65, 'http://192.168.1.157/elabo/?post_type=acf-field&#038;p=66', 0, 'acf-field', '', 0),
(67, 1, '2021-05-05 07:17:09', '2021-04-29 02:57:07', ' ', '', '', 'publish', 'closed', 'closed', '', '67', '', '', '2021-05-05 07:17:09', '2021-05-05 07:17:09', '', 0, 'http://192.168.1.157/elabo/?p=67', 9, 'nav_menu_item', '', 0),
(68, 1, '2021-05-05 07:17:09', '2021-04-29 02:57:07', ' ', '', '', 'publish', 'closed', 'closed', '', '68', '', '', '2021-05-05 07:17:09', '2021-05-05 07:17:09', '', 0, 'http://192.168.1.157/elabo/?p=68', 10, 'nav_menu_item', '', 0),
(69, 1, '2021-05-05 07:17:09', '2021-04-29 02:57:07', ' ', '', '', 'publish', 'closed', 'closed', '', '69', '', '', '2021-05-05 07:17:09', '2021-05-05 07:17:09', '', 0, 'http://192.168.1.157/elabo/?p=69', 11, 'nav_menu_item', '', 0),
(70, 77777, '2021-04-29 15:40:45', '2021-04-29 15:40:45', '', '7. Park Hye-won (HYYN)’s NEW SONG ‘한 번만 내 마음대로 하자’ㅣOasis LIVEㅣ', '', 'inherit', 'open', 'open', '', '', '', '', '2021-04-29 15:40:45', '2021-04-29 15:40:45', '', 52, 'https://hivepress.io/wp-content/uploads/2021/01/logo.svg', 0, 'attachment', 'image/jpeg', 0),
(71, 1, '2021-04-29 08:41:41', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2021-04-29 08:41:41', '0000-00-00 00:00:00', '', 0, 'http://192.168.1.157/elabo/?post_type=video&p=71', 0, 'video', '', 0),
(74, 1, '2021-04-29 09:14:24', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2021-04-29 09:14:24', '0000-00-00 00:00:00', '', 0, 'http://192.168.1.157/elabo/?p=74', 0, 'post', '', 0),
(76, 1, '2021-04-29 09:53:52', '2021-04-29 09:53:52', '', 'Home Page', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2021-04-29 09:53:52', '2021-04-29 09:53:52', '', 2, 'http://192.168.1.157/elabo/?p=76', 0, 'revision', '', 0),
(77, 1, '2021-05-04 04:49:28', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', '', 'closed', '', '', '', '', '2021-05-04 04:49:28', '0000-00-00 00:00:00', '', 0, 'http://192.168.1.157/elabo/?post_type=video&p=77', 0, 'video', '', 0),
(78, 1, '2021-05-04 04:49:59', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', '', 'closed', '', '', '', '', '2021-05-04 04:49:59', '0000-00-00 00:00:00', '', 0, 'http://192.168.1.157/elabo/?post_type=video&p=78', 0, 'video', '', 0),
(79, 1, '2021-05-04 08:28:20', '2021-05-04 08:28:20', '', 'test', '', 'trash', 'closed', 'closed', '', 'test__trashed', '', '', '2021-05-04 08:30:17', '2021-05-04 08:30:17', '', 0, 'http://192.168.1.157/elabo/?post_type=video&#038;p=79', 0, 'video', '', 0),
(80, 77777, '2021-05-04 15:29:44', '2021-05-04 15:29:44', '', 'test', '', 'inherit', 'open', 'open', '', '', '', '', '2021-05-04 15:29:44', '2021-05-04 15:29:44', '', 79, 'https://wpastra.com/wp-content/uploads/2019/05/astra-logo.svg', 0, 'attachment', 'image/jpeg', 0),
(81, 1, '2021-05-04 08:30:36', '2021-05-04 08:30:36', 'test video', 'test video', '', 'trash', 'closed', 'closed', '', 'test-video__trashed', '', '', '2021-05-04 09:02:00', '2021-05-04 09:02:00', '', 0, 'http://192.168.1.157/elabo/?post_type=video&#038;p=81', 0, 'video', '', 0),
(82, 77777, '2021-05-04 15:30:36', '2021-05-04 15:30:36', '', 'test video', '', 'inherit', 'open', 'open', '', '', '', '', '2021-05-04 15:30:36', '2021-05-04 15:30:36', '', 81, 'https://wpastra.com/wp-content/uploads/2019/05/astra-logo.svg', 0, 'attachment', 'image/jpeg', 0),
(83, 1, '2021-05-04 08:34:17', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', '', 'closed', '', '', '', '', '2021-05-04 08:34:17', '0000-00-00 00:00:00', '', 0, 'http://192.168.1.157/elabo/?post_type=video&p=83', 0, 'video', '', 0),
(84, 1, '2021-05-04 08:34:32', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', '', 'closed', '', '', '', '', '2021-05-04 08:34:32', '0000-00-00 00:00:00', '', 0, 'http://192.168.1.157/elabo/?post_type=video&p=84', 0, 'video', '', 0),
(85, 1, '2021-05-04 08:35:51', '2021-05-04 08:35:51', '', 'test add image', '', 'trash', 'closed', 'closed', '', 'test-add-image__trashed', '', '', '2021-05-04 09:01:57', '2021-05-04 09:01:57', '', 0, 'http://192.168.1.157/elabo/?post_type=video&#038;p=85', 0, 'video', '', 0),
(86, 77777, '2021-05-04 15:35:51', '2021-05-04 15:35:51', '', 'test add image', '', 'inherit', 'open', 'open', '', '', '', '', '2021-05-04 15:35:51', '2021-05-04 15:35:51', '', 85, 'https://wpastra.com/wp-content/uploads/2019/05/astra-logo.svg', 0, 'attachment', 'image/jpeg', 0),
(87, 1, '2021-05-04 09:20:37', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', '', 'closed', '', '', '', '', '2021-05-04 09:20:37', '0000-00-00 00:00:00', '', 0, 'http://192.168.1.157/elabo/?post_type=video&p=87', 0, 'video', '', 0),
(88, 77777, '2021-05-05 09:10:18', '2021-05-05 09:10:18', '', 'Movies 03', '', 'inherit', 'open', 'open', '', '', '', '', '2021-05-05 09:10:18', '2021-05-05 09:10:18', '', 46, 'https://wpastra.com/wp-content/uploads/2019/05/astra-logo.svg', 0, 'attachment', 'image/jpeg', 0),
(89, 1, '2021-05-05 04:59:13', '2021-05-05 04:59:13', 'ja', 'Japanese', '', 'publish', 'closed', 'closed', '', 'japanese', '', '', '2021-05-05 04:59:13', '2021-05-05 04:59:13', '', 0, 'http://192.168.1.157/elabo/language_switcher/japanese/', 0, 'language_switcher', '', 0),
(90, 1, '2021-05-05 04:59:13', '2021-05-05 04:59:13', 'en_US', 'English', '', 'publish', 'closed', 'closed', '', 'english', '', '', '2021-05-05 04:59:13', '2021-05-05 04:59:13', '', 0, 'http://192.168.1.157/elabo/language_switcher/english/', 0, 'language_switcher', '', 0),
(91, 1, '2021-05-05 04:59:13', '2021-05-05 04:59:13', 'current_language', 'Current Language', '', 'publish', 'closed', 'closed', '', 'current-language', '', '', '2021-05-05 04:59:13', '2021-05-05 04:59:13', '', 0, 'http://192.168.1.157/elabo/language_switcher/current-language/', 0, 'language_switcher', '', 0),
(92, 1, '2021-05-05 04:59:24', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2021-05-05 04:59:24', '0000-00-00 00:00:00', '', 0, 'http://192.168.1.157/elabo/?p=92', 1, 'nav_menu_item', '', 0),
(93, 1, '2021-05-05 04:59:24', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2021-05-05 04:59:24', '0000-00-00 00:00:00', '', 0, 'http://192.168.1.157/elabo/?p=93', 1, 'nav_menu_item', '', 0),
(94, 1, '2021-05-05 06:11:17', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', '', 'closed', '', '', '', '', '2021-05-05 06:11:17', '0000-00-00 00:00:00', '', 0, 'http://192.168.1.157/elabo/?post_type=video&p=94', 0, 'video', '', 0),
(95, 1, '2021-05-05 06:12:07', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', '', 'closed', '', '', '', '', '2021-05-05 06:12:07', '0000-00-00 00:00:00', '', 0, 'http://192.168.1.157/elabo/?post_type=video&p=95', 0, 'video', '', 0),
(96, 1, '2021-05-05 06:13:28', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', '', 'closed', '', '', '', '', '2021-05-05 06:13:28', '0000-00-00 00:00:00', '', 0, 'http://192.168.1.157/elabo/?post_type=video&p=96', 0, 'video', '', 0),
(97, 1, '2021-05-05 06:14:47', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', '', 'closed', '', '', '', '', '2021-05-05 06:14:47', '0000-00-00 00:00:00', '', 0, 'http://192.168.1.157/elabo/?post_type=video&p=97', 0, 'video', '', 0),
(98, 1, '2021-05-05 06:15:06', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', '', 'closed', '', '', '', '', '2021-05-05 06:15:06', '0000-00-00 00:00:00', '', 0, 'http://192.168.1.157/elabo/?post_type=video&p=98', 0, 'video', '', 0),
(99, 1, '2021-05-05 06:16:22', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2021-05-05 06:16:22', '0000-00-00 00:00:00', '', 0, 'http://192.168.1.157/elabo/?post_type=acf-field-group&p=99', 0, 'acf-field-group', '', 0),
(100, 1, '2021-05-05 06:16:59', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', '', 'closed', '', '', '', '', '2021-05-05 06:16:59', '0000-00-00 00:00:00', '', 0, 'http://192.168.1.157/elabo/?post_type=video&p=100', 0, 'video', '', 0),
(101, 1, '2021-05-05 06:20:42', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', '', 'closed', '', '', '', '', '2021-05-05 06:20:42', '0000-00-00 00:00:00', '', 0, 'http://192.168.1.157/elabo/?post_type=video&p=101', 0, 'video', '', 0),
(102, 1, '2021-05-05 06:22:22', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', '', 'closed', '', '', '', '', '2021-05-05 06:22:22', '0000-00-00 00:00:00', '', 0, 'http://192.168.1.157/elabo/?post_type=video&p=102', 0, 'video', '', 0),
(103, 1, '2021-05-05 06:25:53', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', '', 'closed', '', '', '', '', '2021-05-05 06:25:53', '0000-00-00 00:00:00', '', 0, 'http://192.168.1.157/elabo/?post_type=video&p=103', 0, 'video', '', 0),
(104, 1, '2021-05-05 06:28:42', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', '', 'closed', '', '', '', '', '2021-05-05 06:28:42', '0000-00-00 00:00:00', '', 0, 'http://192.168.1.157/elabo/?post_type=video&p=104', 0, 'video', '', 0),
(105, 1, '2021-05-05 06:30:18', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2021-05-05 06:30:18', '0000-00-00 00:00:00', '', 0, 'http://192.168.1.157/elabo/?post_type=acf-field-group&p=105', 0, 'acf-field-group', '', 0),
(106, 1, '2021-05-05 06:30:22', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', '', 'closed', '', '', '', '', '2021-05-05 06:30:22', '0000-00-00 00:00:00', '', 0, 'http://192.168.1.157/elabo/?post_type=video&p=106', 0, 'video', '', 0),
(107, 1, '2021-05-05 06:30:36', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', '', 'closed', '', '', '', '', '2021-05-05 06:30:36', '0000-00-00 00:00:00', '', 0, 'http://192.168.1.157/elabo/?post_type=video&p=107', 0, 'video', '', 0),
(108, 1, '2021-05-05 06:31:11', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', '', 'closed', '', '', '', '', '2021-05-05 06:31:11', '0000-00-00 00:00:00', '', 0, 'http://192.168.1.157/elabo/?post_type=video&p=108', 0, 'video', '', 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `wp_termmeta`
--

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `wp_termmeta`
--

INSERT INTO `wp_termmeta` (`meta_id`, `term_id`, `meta_key`, `meta_value`) VALUES
(1, 9, '_cm_term_group_array', ',0,'),
(2, 16, '_cm_term_group_array', ',1,'),
(3, 10, '_cm_term_group_array', ',0,'),
(4, 17, '_cm_term_group_array', ',0,'),
(5, 11, '_cm_term_group_array', ',0,'),
(6, 18, '_cm_term_group_array', ',0,'),
(7, 19, '_cm_term_group_array', ',0,'),
(8, 25, '_cm_term_group_array', ',0,'),
(9, 20, '_cm_term_group_array', ',0,'),
(10, 21, '_cm_term_group_array', ',0,'),
(11, 12, '_cm_term_group_array', ',0,'),
(12, 13, '_cm_term_group_array', ',0,'),
(13, 14, '_cm_term_group_array', ',0,'),
(14, 15, '_cm_term_group_array', ',0,'),
(16, 23, '_cm_term_group_array', ',0,'),
(17, 26, '_cm_term_group_array', ',0,'),
(18, 28, '_cm_term_group_array', ',0,'),
(20, 6, '_cm_term_group_array', ',0,'),
(24, 22, '_cm_term_group_array', ',0,'),
(25, 27, '_cm_term_group_array', ',0,'),
(27, 1, '_cm_term_group_array', ',0,'),
(28, 24, '_cm_term_group_array', ',0,');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `wp_terms`
--

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(6, 'Main menu', 'main-menu', 0),
(9, '3D', '3d', 0),
(10, 'Abuse', 'abuse', 0),
(11, 'Adult Awards', 'adult-awards', 0),
(12, 'Anchorwoman', 'anchorwoman', 0),
(13, 'Aunt', 'aunt', 0),
(14, 'AV OPEN 2014 Super Heavyweight', 'av-open-2014-super-heavyweight', 0),
(15, 'AV OPEN 2016 Hardcore Dept', 'av-open-2016-hardcore-dept', 0),
(16, 'Abe Kanna', 'abe-kanna', 0),
(17, 'Adachi Mami', 'adachi-mami', 0),
(18, 'Agawa Nanako', 'aagawa-nanako', 0),
(19, 'Ai Kawana', 'ai-kawana', 0),
(20, 'Amateur Fishing', 'amateur-fishing', 0),
(21, 'Amateur Hey-hey Power', 'amateur-hey-hey-power', 0),
(22, 'Popular', 'popular', 0),
(23, 'Censored JAV', 'censored', 0),
(24, 'Uncensored JAV', 'uncensored', 0),
(25, 'Amateur', 'amateur', 0),
(26, 'Chinese Subtitles', 'chinese-subtitles', 0),
(27, 'Reducing Mosaic', 'reducing-mosaic', 0),
(28, 'English Subtitles', 'english-subtitles', 0),
(33, 'Cast', 'cast', 0),
(34, 'Genre', 'genre', 0),
(35, 'Maker', 'maker', 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `wp_term_relationships`
--

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `term_order` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(12, 6, 0),
(22, 22, 0),
(46, 10, 0),
(46, 24, 0),
(47, 22, 0),
(48, 22, 0),
(49, 22, 0),
(50, 22, 0),
(51, 10, 0),
(51, 24, 0),
(52, 9, 0),
(52, 10, 0),
(52, 22, 0),
(52, 23, 0),
(52, 24, 0),
(52, 26, 0),
(52, 27, 0),
(52, 28, 0),
(54, 6, 0),
(55, 6, 0),
(56, 6, 0),
(57, 6, 0),
(58, 6, 0),
(59, 6, 0),
(60, 6, 0),
(67, 6, 0),
(68, 6, 0),
(69, 6, 0),
(85, 10, 0),
(85, 25, 0),
(85, 26, 0),
(85, 33, 0),
(85, 35, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `wp_term_taxonomy`
--

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `count` bigint(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 0),
(6, 6, 'nav_menu', '', 0, 11),
(9, 9, 'video_tag', '', 34, 1),
(10, 10, 'video_tag', '', 33, 3),
(11, 11, 'video_tag', '', 33, 0),
(12, 12, 'video_tag', '', 33, 0),
(13, 13, 'video_tag', '', 33, 0),
(14, 14, 'video_tag', '', 34, 0),
(15, 15, 'video_tag', '', 34, 0),
(16, 16, 'video_tag', '', 35, 0),
(17, 17, 'video_tag', '', 34, 0),
(18, 18, 'video_tag', '', 33, 0),
(19, 19, 'video_tag', '', 33, 0),
(20, 20, 'video_tag', '', 35, 0),
(21, 21, 'video_tag', '', 34, 0),
(22, 22, 'categories', '', 0, 6),
(23, 23, 'categories', '', 0, 1),
(24, 24, 'categories', '', 0, 3),
(25, 25, 'categories', '', 0, 0),
(26, 26, 'categories', '', 0, 1),
(27, 27, 'categories', '', 0, 1),
(28, 28, 'categories', '', 0, 1),
(33, 33, 'video_tag', '', 0, 0),
(34, 34, 'video_tag', '', 0, 0),
(35, 35, 'video_tag', '', 0, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `wp_trp_dictionary_ja_en_us`
--

CREATE TABLE `wp_trp_dictionary_ja_en_us` (
  `id` bigint(20) NOT NULL,
  `original` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `translated` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(20) DEFAULT 0,
  `block_type` int(20) DEFAULT 0,
  `original_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `wp_trp_dictionary_ja_en_us`
--

INSERT INTO `wp_trp_dictionary_ja_en_us` (`id`, `original`, `translated`, `status`, `block_type`, `original_id`) VALUES
(1, 'Search', '', 0, 0, 1),
(2, 'Home', '', 0, 0, 2),
(3, 'Popular', '', 0, 0, 3),
(4, 'Censored JAV', '', 0, 0, 4),
(5, 'Uncensored JAV', '', 0, 0, 5),
(6, 'Amateur', '', 0, 0, 6),
(7, 'Chinese Subtitles', '', 0, 0, 7),
(8, 'Reducing Mosaic', '', 0, 0, 8),
(9, 'English Subtitles', '', 0, 0, 9),
(10, 'Cast', '', 0, 0, 10),
(11, 'Genre', '', 0, 0, 11),
(12, 'Maker', '', 0, 0, 12),
(13, 'Latest Videos', '', 0, 0, 13),
(14, '7. Park Hye-won (HYYN)&#8217;s NEW SONG &#8216;한 번만 내 마음대로 하자&#8217;ㅣOasis LIVEㅣ', '', 0, 0, 14),
(15, '3 View', '', 0, 0, 15),
(16, '6. Park Hye-won (HYYN)&#8217;s NEW SONG &#8216;한 번만 내 마음대로 하자&#8217;ㅣOasis LIVEㅣ', '', 0, 0, 16),
(17, '5 View', '', 0, 0, 17),
(18, '5. Park Hye-won (HYYN)&#8217;s NEW SONG &#8216;한 번만 내 마음대로 하자&#8217;ㅣOasis LIVEㅣ', '', 0, 0, 18),
(19, '2 View', '', 0, 0, 19),
(20, '4. Park Hye-won (HYYN)&#8217;s NEW SONG &#8216;한 번만 내 마음대로 하자&#8217;ㅣOasis LIVEㅣ', '', 0, 0, 20),
(21, '3. Park Hye-won (HYYN)&#8217;s NEW SONG &#8216;한 번만 내 마음대로 하자&#8217;ㅣOasis LIVEㅣ', '', 0, 0, 21),
(22, '1 View', '', 0, 0, 22),
(23, '2. Park Hye-won (HYYN)&#8217;s NEW SONG &#8216;한 번만 내 마음대로 하자&#8217;ㅣOasis LIVEㅣ', '', 0, 0, 23),
(24, 'Movies 03', '', 0, 0, 24),
(25, 'Movie002', '', 0, 0, 25),
(26, 'Movie 001', '', 0, 0, 26),
(27, '4-14天崩开局！国服阿轲自带bug级高能四杀翻盘！', '', 0, 0, 27),
(28, 'Got Mental Breakdown While Getting Crazy Medical Checkups!ㅣMy Idol&#8217;s Medical Checkup | GWSN', '', 0, 0, 28),
(29, 'Park Hye-won (HYYN)&#8217;s NEW SONG &#8216;한 번만 내 마음대로 하자&#8217;ㅣOasis LIVEㅣ', '', 0, 0, 29),
(30, '&copy; 2021 Paracel Technology Solutions. All Rights Reserved', '', 0, 0, 30),
(31, 'http://192.168.1.157/elabo/wp-content/themes/videotube/img/no_image_available-150x150.jpg', '', 0, 0, 31),
(32, 'https://hivepress.io/wp-content/uploads/2021/01/logo.svg', '', 0, 0, 32),
(33, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/f531fec46cb7475ab03bb412c1344f77-295x197.jpg', '', 0, 0, 33),
(34, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/JeALalala-295x197.jpg', '', 0, 0, 34),
(35, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/no_image_available-295x197.jpg', '', 0, 0, 35),
(36, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/580eb99da440318419e445bb83d884937c089497r1_647_1018v2_uhq-295x197.jpg', '', 0, 0, 36),
(37, 'https://wpastra.com/wp-content/uploads/2019/05/astra-logo.svg', '', 0, 0, 37),
(38, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/img-share-295x197.jpg', '', 0, 0, 38),
(39, '7. Park Hye-won (HYYN)’s NEW SONG ‘한 번만 내 마음대로 하자’ㅣOasis LIVEㅣ', '', 0, 0, 39),
(40, '6. Park Hye-won (HYYN)’s NEW SONG ‘한 번만 내 마음대로 하자’ㅣOasis LIVEㅣ', '', 0, 0, 40),
(41, '5. Park Hye-won (HYYN)’s NEW SONG ‘한 번만 내 마음대로 하자’ㅣOasis LIVEㅣ', '', 0, 0, 41),
(42, '4. Park Hye-won (HYYN)’s NEW SONG ‘한 번만 내 마음대로 하자’ㅣOasis LIVEㅣ', '', 0, 0, 42),
(43, '3. Park Hye-won (HYYN)’s NEW SONG ‘한 번만 내 마음대로 하자’ㅣOasis LIVEㅣ', '', 0, 0, 43),
(44, '2. Park Hye-won (HYYN)’s NEW SONG ‘한 번만 내 마음대로 하자’ㅣOasis LIVEㅣ', '', 0, 0, 44),
(45, 'Got Mental Breakdown While Getting Crazy Medical Checkups!ㅣMy Idol’s Medical Checkup | GWSN', '', 0, 0, 45),
(46, 'Park Hye-won (HYYN)’s NEW SONG ‘한 번만 내 마음대로 하자’ㅣOasis LIVEㅣ', '', 0, 0, 46),
(47, '3D', '', 0, 0, 47),
(48, 'Abuse', '', 0, 0, 48),
(49, 'Related Videos', '', 0, 0, 49),
(50, 'What if JeA is the professor at the practical music interview!?Real tipsㅣJeA Lalala EP.10', '', 0, 0, 50),
(51, '4 View', '', 0, 0, 51),
(52, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/img-share-360x240.jpg', '', 0, 0, 52),
(53, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/f531fec46cb7475ab03bb412c1344f77-360x240.jpg', '', 0, 0, 53),
(54, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/580eb99da440318419e445bb83d884937c089497r1_647_1018v2_uhq-360x240.jpg', '', 0, 0, 54),
(55, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/no_image_available-360x240.jpg', '', 0, 0, 55),
(56, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/JeALalala-360x240.jpg', '', 0, 0, 56),
(57, 'K-pop Idol doing FREAKIN&#8217; MEDICAL CHECK _ GWSN', '', 0, 0, 57),
(58, 'GWSN is promoting with their new song BAZOOKA!', '', 0, 0, 58),
(59, 'A medical checkup service for them to promote healthily!', '', 0, 0, 59),
(60, 'But the checkups aren&#8217;t what they used to know! LOL', '', 0, 0, 60),
(61, 'Which member gets their name on the Hall of Fame for King Healthy Idols?!', '', 0, 0, 61),
(62, 'Check out the members&#8217; fill-out form information on the Community tab!', '', 0, 0, 62),
(63, 'Please subscribe and enable notifications http://bit.ly/2RpCcV1', '', 0, 0, 63),
(64, 'Got Mental Breakdown While Getting Crazy Medical Checkups!girls nearly fought with their manager during the checkup haha ㅣMy Idol&#8217;s Medical Checkup | GWSN', '', 0, 0, 64),
(65, 'Warning', '', 0, 0, 65),
(66, ':  Invalid argument supplied for foreach() in', '', 0, 0, 66),
(67, 'C:\\xampp\\htdocs\\elabo\\wp-content\\themes\\videotube\\single-video.php', '', 0, 0, 67),
(68, 'on line', '', 0, 0, 68),
(69, '<trp-post-container data-trp-post-id=\'47\'>2. Park Hye-won (HYYN)’s NEW SONG ‘한 번만 내 마음대로 하자’ㅣOasis LIVEㅣ</trp-post-container>', '', 0, 0, 69),
(70, '<trp-post-container data-trp-post-id=\'48\'>3. Park Hye-won (HYYN)’s NEW SONG ‘한 번만 내 마음대로 하자’ㅣOasis LIVEㅣ</trp-post-container>', '', 0, 0, 70),
(71, '<trp-post-container data-trp-post-id=\'22\'>4-14天崩开局！国服阿轲自带bug级高能四杀翻盘！</trp-post-container>', '', 0, 0, 71),
(72, '<trp-post-container data-trp-post-id=\'27\'>Movie 001</trp-post-container>', '', 0, 0, 72),
(73, '<trp-post-container data-trp-post-id=\'49\'>4. Park Hye-won (HYYN)’s NEW SONG ‘한 번만 내 마음대로 하자’ㅣOasis LIVEㅣ</trp-post-container>', '', 0, 0, 73),
(74, '<trp-post-container data-trp-post-id=\'50\'>5. Park Hye-won (HYYN)’s NEW SONG ‘한 번만 내 마음대로 하자’ㅣOasis LIVEㅣ</trp-post-container>', '', 0, 0, 74),
(75, '<trp-post-container data-trp-post-id=\'46\'>Movies 03</trp-post-container>', '', 0, 0, 75),
(76, '<trp-post-container data-trp-post-id=\'52\'>7. Park Hye-won (HYYN)’s NEW SONG ‘한 번만 내 마음대로 하자’ㅣOasis LIVEㅣ</trp-post-container>', '', 0, 0, 76),
(77, '<trp-post-container data-trp-post-id=\'20\'>Got Mental Breakdown While Getting Crazy Medical Checkups!ㅣMy Idol’s Medical Checkup | GWSN</trp-post-container>', '', 0, 0, 77),
(78, '<trp-post-container data-trp-post-id=\'14\'>What if JeA is the professor at the practical music interview!?Real tipsㅣJeA Lalala EP.10</trp-post-container>', '', 0, 0, 78),
(79, '<trp-post-container data-trp-post-id=\'51\'>6. Park Hye-won (HYYN)’s NEW SONG ‘한 번만 내 마음대로 하자’ㅣOasis LIVEㅣ</trp-post-container>', '', 0, 0, 79),
(80, '<trp-post-container data-trp-post-id=\'18\'>Park Hye-won (HYYN)’s NEW SONG ‘한 번만 내 마음대로 하자’ㅣOasis LIVEㅣ</trp-post-container>', '', 0, 0, 80),
(81, 'HYNN Park Hye-won&#8217;s new song is crazy.', '', 0, 0, 81),
(82, 'I know she is a good singer, but this new song expresses her feelings so well.', '', 0, 0, 82),
(83, 'How many times have you said good-bye? Please don&#8217;t let her break up.', '', 0, 0, 83),
(84, 'All the staff got goosebumps when we heard the live.', '', 0, 0, 84),
(85, '#HYYN #박예원 #한_번만_내_마음대로_하자', '', 0, 0, 85),
(86, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/JeALalala-230x150.jpg', '', 0, 0, 86),
(87, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/no_image_available-230x150.jpg', '', 0, 0, 87),
(88, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/580eb99da440318419e445bb83d884937c089497r1_647_1018v2_uhq-230x150.jpg', '', 0, 0, 88),
(89, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/f531fec46cb7475ab03bb412c1344f77-230x150.jpg', '', 0, 0, 89),
(90, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/img-share-230x150.jpg', '', 0, 0, 90);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `wp_trp_gettext_en_us`
--

CREATE TABLE `wp_trp_gettext_en_us` (
  `id` bigint(20) NOT NULL,
  `original` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `translated` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `domain` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `wp_trp_gettext_en_us`
--

INSERT INTO `wp_trp_gettext_en_us` (`id`, `original`, `translated`, `domain`, `status`) VALUES
(1, 'Search videos...', '', 'mars', 0),
(2, 'Facebook', '', 'mars', 0),
(3, 'twitter', '', 'mars', 0),
(4, 'Google Plus', '', 'mars', 0),
(5, 'Instagram', '', 'mars', 0),
(6, 'Linkedin', '', 'mars', 0),
(7, 'Tumblr', '', 'mars', 0),
(8, 'Youtube', '', 'mars', 0),
(9, 'Vimeo', '', 'mars', 0),
(10, 'Pinterest', '', 'mars', 0),
(11, 'Snapchat', '', 'mars', 0),
(12, 'words', '', 'default', 0),
(13, '%s', '', 'mars', 0),
(14, 'Edit Profile', '', 'default', 0),
(15, 'Log Out', '', 'default', 0),
(16, 'Search', '', 'default', 0),
(17, 'Howdy, %s', '', 'default', 0),
(18, 'About WordPress', '', 'default', 0),
(19, 'WordPress.org', '', 'default', 0),
(20, 'https://wordpress.org/', '', 'default', 0),
(21, 'Documentation', '', 'default', 0),
(22, 'https://wordpress.org/support/', '', 'default', 0),
(23, 'Support', '', 'default', 0),
(24, 'https://wordpress.org/support/forums/', '', 'default', 0),
(25, 'Feedback', '', 'default', 0),
(26, 'https://wordpress.org/support/forum/requests-and-feedback', '', 'default', 0),
(27, 'Dashboard', '', 'default', 0),
(28, 'Themes', '', 'default', 0),
(29, 'Widgets', '', 'default', 0),
(30, 'Menus', '', 'default', 0),
(31, 'Background', '', 'default', 0),
(32, 'Customize', '', 'default', 0),
(33, '%d Plugin Update', '', 'default', 0),
(34, 'Translation Updates', '', 'default', 0),
(35, '%s Comments in moderation', '', 'default', 0),
(36, 'User', '', 'default', 0),
(37, 'New', '', 'default', 0),
(38, 'Skip to toolbar', '', 'default', 0),
(39, 'Toolbar', '', 'default', 0),
(40, 'This is somewhat embarrassing, isn\'t it?', '', 'mars', 0),
(41, 'It seems we can not find what you are looking for. Perhaps searching can help.', '', 'mars', 0),
(42, '%s item', '', 'default', 0),
(43, '%s items', '', 'default', 0),
(44, 'Pages:', '', 'mars', 0),
(45, 'Comments', '', 'mars', 0),
(46, 'Share', '', 'mars', 0),
(47, 'Turn off Light', '', 'mars', 0),
(48, 'Twitter', '', 'mars', 0),
(49, 'Google plus', '', 'mars', 0),
(50, 'Reddit', '', 'mars', 0),
(51, 'Odnok', '', 'mars', 0),
(52, 'Vkontakte', '', 'mars', 0),
(53, 'I saw this and thought of you! %s', '', 'mars', 0),
(54, 'Email', '', 'mars', 0),
(55, 'Published on %s by %s', '', 'mars', 0),
(56, 'off', '', 'default', 0),
(57, 'Read less', '', 'mars', 0),
(58, 'Continue reading %s', '', 'default', 0),
(59, '(more&hellip;)', '', 'default', 0),
(60, 'Edit', '', 'mars', 0),
(61, 'Cast :', '', 'mars', 0),
(62, 'Maker :', '', 'mars', 0),
(63, 'Genre :', '', 'mars', 0),
(64, 'Archives', '', 'default', 0),
(65, '%s:', '', 'default', 0),
(66, 'Latest', '', 'mars', 0),
(67, 'Viewed', '', 'mars', 0),
(68, 'Liked', '', 'mars', 0),
(69, 'Sort by:', '', 'mars', 0),
(70, 'About %s result', '', 'mars', 0),
(71, 'Nothing Found.', '', 'mars', 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `wp_trp_gettext_ja`
--

CREATE TABLE `wp_trp_gettext_ja` (
  `id` bigint(20) NOT NULL,
  `original` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `translated` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `domain` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `wp_trp_gettext_ja`
--

INSERT INTO `wp_trp_gettext_ja` (`id`, `original`, `translated`, `domain`, `status`) VALUES
(1, 'Search videos...', '', 'mars', 0),
(2, 'Facebook', '', 'mars', 0),
(3, 'twitter', '', 'mars', 0),
(4, 'Google Plus', '', 'mars', 0),
(5, 'Instagram', '', 'mars', 0),
(6, 'Linkedin', '', 'mars', 0),
(7, 'Tumblr', '', 'mars', 0),
(8, 'Youtube', '', 'mars', 0),
(9, 'Vimeo', '', 'mars', 0),
(10, 'Pinterest', '', 'mars', 0),
(11, 'Snapchat', '', 'mars', 0),
(12, 'words', 'characters_including_spaces', 'default', 2),
(13, '%s', '', 'mars', 0),
(14, 'Edit Profile', 'プロフィールを編集', 'default', 2),
(15, 'Log Out', 'ログアウト', 'default', 2),
(16, 'Search', '検索', 'default', 2),
(17, 'Howdy, %s', 'こんにちは、%s さん', 'default', 2),
(18, 'About WordPress', 'WordPress について', 'default', 2),
(19, 'WordPress.org', '', 'default', 0),
(20, 'https://wordpress.org/', 'https://ja.wordpress.org/', 'default', 2),
(21, 'Documentation', 'ドキュメンテーション', 'default', 2),
(22, 'https://wordpress.org/support/', 'https://ja.wordpress.org/support/', 'default', 2),
(23, 'Support', 'サポート', 'default', 2),
(24, 'https://wordpress.org/support/forums/', 'https://ja.wordpress.org/support/forums/', 'default', 2),
(25, 'Feedback', 'フィードバック', 'default', 2),
(26, 'https://wordpress.org/support/forum/requests-and-feedback', 'https://ja.wordpress.org/support/forum/feedback/', 'default', 2),
(27, 'Dashboard', 'ダッシュボード', 'default', 2),
(28, 'Themes', 'テーマ', 'default', 2),
(29, 'Widgets', 'ウィジェット', 'default', 2),
(30, 'Menus', 'メニュー', 'default', 2),
(31, 'Background', '背景', 'default', 2),
(32, 'Customize', 'カスタマイズ', 'default', 2),
(33, '%d Plugin Update', '%d件のプラグイン更新', 'default', 2),
(34, 'Translation Updates', '翻訳の更新', 'default', 2),
(35, '%s Comments in moderation', '%s件のコメントが承認待ちです', 'default', 2),
(36, 'User', 'ユーザー', 'default', 2),
(37, 'New', '新規', 'default', 2),
(38, 'Skip to toolbar', 'ツールバーへスキップ', 'default', 2),
(39, 'Toolbar', 'ツールバー', 'default', 2);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `wp_trp_original_meta`
--

CREATE TABLE `wp_trp_original_meta` (
  `meta_id` bigint(20) NOT NULL,
  `original_id` bigint(20) NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `wp_trp_original_meta`
--

INSERT INTO `wp_trp_original_meta` (`meta_id`, `original_id`, `meta_key`, `meta_value`) VALUES
(1, 25, 'post_parent_id', '33'),
(2, 57, 'post_parent_id', '33'),
(3, 58, 'post_parent_id', '33'),
(4, 59, 'post_parent_id', '33'),
(5, 60, 'post_parent_id', '33'),
(6, 61, 'post_parent_id', '33'),
(7, 62, 'post_parent_id', '33'),
(8, 63, 'post_parent_id', '33'),
(9, 64, 'post_parent_id', '33'),
(10, 23, 'post_parent_id', '47'),
(11, 21, 'post_parent_id', '48'),
(12, 27, 'post_parent_id', '22'),
(13, 26, 'post_parent_id', '27'),
(14, 20, 'post_parent_id', '49'),
(15, 18, 'post_parent_id', '50'),
(16, 24, 'post_parent_id', '46'),
(17, 14, 'post_parent_id', '52'),
(18, 28, 'post_parent_id', '20'),
(19, 50, 'post_parent_id', '14'),
(20, 16, 'post_parent_id', '51'),
(21, 29, 'post_parent_id', '18'),
(22, 81, 'post_parent_id', '50'),
(23, 82, 'post_parent_id', '50'),
(24, 83, 'post_parent_id', '50'),
(25, 84, 'post_parent_id', '50'),
(26, 85, 'post_parent_id', '50');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `wp_trp_original_strings`
--

CREATE TABLE `wp_trp_original_strings` (
  `id` bigint(20) NOT NULL,
  `original` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `wp_trp_original_strings`
--

INSERT INTO `wp_trp_original_strings` (`id`, `original`) VALUES
(1, 'Search'),
(2, 'Home'),
(3, 'Popular'),
(4, 'Censored JAV'),
(5, 'Uncensored JAV'),
(6, 'Amateur'),
(7, 'Chinese Subtitles'),
(8, 'Reducing Mosaic'),
(9, 'English Subtitles'),
(10, 'Cast'),
(11, 'Genre'),
(12, 'Maker'),
(13, 'Latest Videos'),
(14, '7. Park Hye-won (HYYN)&#8217;s NEW SONG &#8216;한 번만 내 마음대로 하자&#8217;ㅣOasis LIVEㅣ'),
(15, '3 View'),
(16, '6. Park Hye-won (HYYN)&#8217;s NEW SONG &#8216;한 번만 내 마음대로 하자&#8217;ㅣOasis LIVEㅣ'),
(17, '5 View'),
(18, '5. Park Hye-won (HYYN)&#8217;s NEW SONG &#8216;한 번만 내 마음대로 하자&#8217;ㅣOasis LIVEㅣ'),
(19, '2 View'),
(20, '4. Park Hye-won (HYYN)&#8217;s NEW SONG &#8216;한 번만 내 마음대로 하자&#8217;ㅣOasis LIVEㅣ'),
(21, '3. Park Hye-won (HYYN)&#8217;s NEW SONG &#8216;한 번만 내 마음대로 하자&#8217;ㅣOasis LIVEㅣ'),
(22, '1 View'),
(23, '2. Park Hye-won (HYYN)&#8217;s NEW SONG &#8216;한 번만 내 마음대로 하자&#8217;ㅣOasis LIVEㅣ'),
(24, 'Movies 03'),
(25, 'Movie002'),
(26, 'Movie 001'),
(27, '4-14天崩开局！国服阿轲自带bug级高能四杀翻盘！'),
(28, 'Got Mental Breakdown While Getting Crazy Medical Checkups!ㅣMy Idol&#8217;s Medical Checkup | GWSN'),
(29, 'Park Hye-won (HYYN)&#8217;s NEW SONG &#8216;한 번만 내 마음대로 하자&#8217;ㅣOasis LIVEㅣ'),
(30, '&copy; 2021 Paracel Technology Solutions. All Rights Reserved'),
(31, 'http://192.168.1.157/elabo/wp-content/themes/videotube/img/no_image_available-150x150.jpg'),
(32, 'https://hivepress.io/wp-content/uploads/2021/01/logo.svg'),
(33, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/f531fec46cb7475ab03bb412c1344f77-295x197.jpg'),
(34, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/JeALalala-295x197.jpg'),
(35, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/no_image_available-295x197.jpg'),
(36, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/580eb99da440318419e445bb83d884937c089497r1_647_1018v2_uhq-295x197.jpg'),
(37, 'https://wpastra.com/wp-content/uploads/2019/05/astra-logo.svg'),
(38, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/img-share-295x197.jpg'),
(39, '7. Park Hye-won (HYYN)’s NEW SONG ‘한 번만 내 마음대로 하자’ㅣOasis LIVEㅣ'),
(40, '6. Park Hye-won (HYYN)’s NEW SONG ‘한 번만 내 마음대로 하자’ㅣOasis LIVEㅣ'),
(41, '5. Park Hye-won (HYYN)’s NEW SONG ‘한 번만 내 마음대로 하자’ㅣOasis LIVEㅣ'),
(42, '4. Park Hye-won (HYYN)’s NEW SONG ‘한 번만 내 마음대로 하자’ㅣOasis LIVEㅣ'),
(43, '3. Park Hye-won (HYYN)’s NEW SONG ‘한 번만 내 마음대로 하자’ㅣOasis LIVEㅣ'),
(44, '2. Park Hye-won (HYYN)’s NEW SONG ‘한 번만 내 마음대로 하자’ㅣOasis LIVEㅣ'),
(45, 'Got Mental Breakdown While Getting Crazy Medical Checkups!ㅣMy Idol’s Medical Checkup | GWSN'),
(46, 'Park Hye-won (HYYN)’s NEW SONG ‘한 번만 내 마음대로 하자’ㅣOasis LIVEㅣ'),
(47, '3D'),
(48, 'Abuse'),
(49, 'Related Videos'),
(50, 'What if JeA is the professor at the practical music interview!?Real tipsㅣJeA Lalala EP.10'),
(51, '4 View'),
(52, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/img-share-360x240.jpg'),
(53, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/f531fec46cb7475ab03bb412c1344f77-360x240.jpg'),
(54, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/580eb99da440318419e445bb83d884937c089497r1_647_1018v2_uhq-360x240.jpg'),
(55, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/no_image_available-360x240.jpg'),
(56, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/JeALalala-360x240.jpg'),
(57, 'K-pop Idol doing FREAKIN&#8217; MEDICAL CHECK _ GWSN'),
(58, 'GWSN is promoting with their new song BAZOOKA!'),
(59, 'A medical checkup service for them to promote healthily!'),
(60, 'But the checkups aren&#8217;t what they used to know! LOL'),
(61, 'Which member gets their name on the Hall of Fame for King Healthy Idols?!'),
(62, 'Check out the members&#8217; fill-out form information on the Community tab!'),
(63, 'Please subscribe and enable notifications http://bit.ly/2RpCcV1'),
(64, 'Got Mental Breakdown While Getting Crazy Medical Checkups!girls nearly fought with their manager during the checkup haha ㅣMy Idol&#8217;s Medical Checkup | GWSN'),
(65, 'Warning'),
(66, ':  Invalid argument supplied for foreach() in'),
(67, 'C:\\xampp\\htdocs\\elabo\\wp-content\\themes\\videotube\\single-video.php'),
(68, 'on line'),
(69, '<trp-post-container data-trp-post-id=\'47\'>2. Park Hye-won (HYYN)’s NEW SONG ‘한 번만 내 마음대로 하자’ㅣOasis LIVEㅣ</trp-post-container>'),
(70, '<trp-post-container data-trp-post-id=\'48\'>3. Park Hye-won (HYYN)’s NEW SONG ‘한 번만 내 마음대로 하자’ㅣOasis LIVEㅣ</trp-post-container>'),
(71, '<trp-post-container data-trp-post-id=\'22\'>4-14天崩开局！国服阿轲自带bug级高能四杀翻盘！</trp-post-container>'),
(72, '<trp-post-container data-trp-post-id=\'27\'>Movie 001</trp-post-container>'),
(73, '<trp-post-container data-trp-post-id=\'49\'>4. Park Hye-won (HYYN)’s NEW SONG ‘한 번만 내 마음대로 하자’ㅣOasis LIVEㅣ</trp-post-container>'),
(74, '<trp-post-container data-trp-post-id=\'50\'>5. Park Hye-won (HYYN)’s NEW SONG ‘한 번만 내 마음대로 하자’ㅣOasis LIVEㅣ</trp-post-container>'),
(75, '<trp-post-container data-trp-post-id=\'46\'>Movies 03</trp-post-container>'),
(76, '<trp-post-container data-trp-post-id=\'52\'>7. Park Hye-won (HYYN)’s NEW SONG ‘한 번만 내 마음대로 하자’ㅣOasis LIVEㅣ</trp-post-container>'),
(77, '<trp-post-container data-trp-post-id=\'20\'>Got Mental Breakdown While Getting Crazy Medical Checkups!ㅣMy Idol’s Medical Checkup | GWSN</trp-post-container>'),
(78, '<trp-post-container data-trp-post-id=\'14\'>What if JeA is the professor at the practical music interview!?Real tipsㅣJeA Lalala EP.10</trp-post-container>'),
(79, '<trp-post-container data-trp-post-id=\'51\'>6. Park Hye-won (HYYN)’s NEW SONG ‘한 번만 내 마음대로 하자’ㅣOasis LIVEㅣ</trp-post-container>'),
(80, '<trp-post-container data-trp-post-id=\'18\'>Park Hye-won (HYYN)’s NEW SONG ‘한 번만 내 마음대로 하자’ㅣOasis LIVEㅣ</trp-post-container>'),
(81, 'HYNN Park Hye-won&#8217;s new song is crazy.'),
(82, 'I know she is a good singer, but this new song expresses her feelings so well.'),
(83, 'How many times have you said good-bye? Please don&#8217;t let her break up.'),
(84, 'All the staff got goosebumps when we heard the live.'),
(85, '#HYYN #박예원 #한_번만_내_마음대로_하자'),
(86, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/JeALalala-230x150.jpg'),
(87, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/no_image_available-230x150.jpg'),
(88, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/580eb99da440318419e445bb83d884937c089497r1_647_1018v2_uhq-230x150.jpg'),
(89, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/f531fec46cb7475ab03bb412c1344f77-230x150.jpg'),
(90, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/img-share-230x150.jpg');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `wp_usermeta`
--

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'admin'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'theme_editor_notice'),
(15, 1, 'show_welcome_panel', '0'),
(16, 1, 'session_tokens', 'a:1:{s:64:\"01c8db73c8becca824543cf39d7036db0eb7e88efe77de665ddbfe63c93622f9\";a:4:{s:10:\"expiration\";i:1620266413;s:2:\"ip\";s:13:\"192.168.1.157\";s:2:\"ua\";s:109:\"Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36\";s:5:\"login\";i:1620093613;}}'),
(17, 1, 'wp_dashboard_quick_press_last_post_id', '4'),
(18, 1, 'wp_user-settings', 'mfold=o&libraryContent=browse&editor=html&post_dfw=off&widgets_access=off'),
(19, 1, 'wp_user-settings-time', '1619684800'),
(20, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(21, 1, 'metaboxhidden_nav-menus', 'a:2:{i:0;s:19:\"add-post-type-video\";i:1;s:12:\"add-post_tag\";}'),
(22, 1, 'closedpostboxes_nav-menus', 'a:0:{}'),
(23, 1, 'nav_menu_recently_edited', '6'),
(24, 1, 'closedpostboxes_video', 'a:0:{}'),
(25, 1, 'metaboxhidden_video', 'a:1:{i:0;s:7:\"slugdiv\";}'),
(26, 1, 'meta-box-order_video', 'a:4:{s:15:\"acf_after_title\";s:15:\"acf-group_video\";s:4:\"side\";s:65:\"submitdiv,categoriesdiv,video_tagdiv,postimagediv,imageUrlMetaBox\";s:6:\"normal\";s:57:\"wpseo_meta,commentstatusdiv,slugdiv,commentsdiv,authordiv\";s:8:\"advanced\";s:0:\"\";}'),
(27, 1, 'screen_layout_video', '2'),
(28, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:11:\"192.168.1.0\";}'),
(29, 1, 'closedpostboxes_dashboard', 'a:1:{i:0;s:18:\"dashboard_activity\";}'),
(30, 1, 'metaboxhidden_dashboard', 'a:0:{}');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `wp_users`
--

CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT 0,
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'admin', '$P$Bts/ZYkW3nRVZHpZTSwD/019Oj6a1M.', 'admin', 'khailv@dn.paracelsoft.com', 'http://localhost/elabo', '2021-04-27 13:58:06', '', 0, 'admin');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `wp_yoast_indexable`
--

CREATE TABLE `wp_yoast_indexable` (
  `id` int(11) UNSIGNED NOT NULL,
  `permalink` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permalink_hash` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `object_id` bigint(20) DEFAULT NULL,
  `object_type` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `object_sub_type` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `author_id` bigint(20) DEFAULT NULL,
  `post_parent` bigint(20) DEFAULT NULL,
  `title` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `breadcrumb_title` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_public` tinyint(1) DEFAULT NULL,
  `is_protected` tinyint(1) DEFAULT 0,
  `has_public_posts` tinyint(1) DEFAULT NULL,
  `number_of_pages` int(11) UNSIGNED DEFAULT NULL,
  `canonical` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `primary_focus_keyword` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `primary_focus_keyword_score` int(3) DEFAULT NULL,
  `readability_score` int(3) DEFAULT NULL,
  `is_cornerstone` tinyint(1) DEFAULT 0,
  `is_robots_noindex` tinyint(1) DEFAULT 0,
  `is_robots_nofollow` tinyint(1) DEFAULT 0,
  `is_robots_noarchive` tinyint(1) DEFAULT 0,
  `is_robots_noimageindex` tinyint(1) DEFAULT 0,
  `is_robots_nosnippet` tinyint(1) DEFAULT 0,
  `twitter_title` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter_image` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter_image_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter_image_source` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `open_graph_title` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `open_graph_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `open_graph_image` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `open_graph_image_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `open_graph_image_source` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `open_graph_image_meta` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_count` int(11) DEFAULT NULL,
  `incoming_link_count` int(11) DEFAULT NULL,
  `prominent_words_version` int(11) UNSIGNED DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `blog_id` bigint(20) NOT NULL DEFAULT 1,
  `language` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `region` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `schema_page_type` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `schema_article_type` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `has_ancestors` tinyint(1) DEFAULT 0,
  `estimated_reading_time_minutes` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `wp_yoast_indexable`
--

INSERT INTO `wp_yoast_indexable` (`id`, `permalink`, `permalink_hash`, `object_id`, `object_type`, `object_sub_type`, `author_id`, `post_parent`, `title`, `description`, `breadcrumb_title`, `post_status`, `is_public`, `is_protected`, `has_public_posts`, `number_of_pages`, `canonical`, `primary_focus_keyword`, `primary_focus_keyword_score`, `readability_score`, `is_cornerstone`, `is_robots_noindex`, `is_robots_nofollow`, `is_robots_noarchive`, `is_robots_noimageindex`, `is_robots_nosnippet`, `twitter_title`, `twitter_image`, `twitter_description`, `twitter_image_id`, `twitter_image_source`, `open_graph_title`, `open_graph_description`, `open_graph_image`, `open_graph_image_id`, `open_graph_image_source`, `open_graph_image_meta`, `link_count`, `incoming_link_count`, `prominent_words_version`, `created_at`, `updated_at`, `blog_id`, `language`, `region`, `schema_page_type`, `schema_article_type`, `has_ancestors`, `estimated_reading_time_minutes`) VALUES
(1, 'http://192.168.1.157/elabo/author/admin/', '40:c9e6a635ba295a53a62db09d64e09607', 1, 'user', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 'https://0.gravatar.com/avatar/028e2dc363b58b8c384a3cf8c8cd40fe?s=500&d=mm&r=g', NULL, NULL, 'gravatar-image', NULL, NULL, 'https://0.gravatar.com/avatar/028e2dc363b58b8c384a3cf8c8cd40fe?s=500&d=mm&r=g', NULL, 'gravatar-image', NULL, NULL, NULL, NULL, '2021-04-29 09:19:28', '2021-05-04 01:30:36', 1, NULL, NULL, NULL, NULL, 0, NULL),
(2, 'http://192.168.1.157/elabo/video/%ed%95%9c-%eb%b2%88%eb%a7%8c-%eb%82%b4-%eb%a7%88%ec%9d%8c%eb%8c%80%eb%a1%9c-%ed%95%98%ec%9e%90/', '128:fe9cfbb0f377ffa56afe18461b3aa102', 52, 'post', 'video', 1, 0, NULL, NULL, '7. Park Hye-won (HYYN)&#8217;s NEW SONG &#8216;한 번만 내 마음대로 하자&#8217;ㅣOasis LIVEㅣ', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 30, 0, NULL, 0, NULL, NULL, NULL, NULL, 'https://hivepress.io/wp-content/uploads/2021/01/logo.svg', NULL, '70', 'featured-image', NULL, NULL, 'https://hivepress.io/wp-content/uploads/2021/01/logo.svg', '70', 'featured-image', '{\"width\":\"186\",\"height\":\"40\",\"url\":\"https://hivepress.io/wp-content/uploads/2021/01/logo.svg\",\"path\":\"https://hivepress.io/wp-content/uploads/2021/01/logo.svg\",\"size\":\"full\",\"id\":70,\"alt\":\"7. Park Hye-won (HYYN)\\u2019s NEW SONG \\u2018\\ud55c \\ubc88\\ub9cc \\ub0b4 \\ub9c8\\uc74c\\ub300\\ub85c \\ud558\\uc790\\u2019\\u3163Oasis LIVE\\u3163\",\"pixels\":7440,\"type\":\"image/jpeg\"}', 0, NULL, NULL, '2021-04-29 09:19:28', '2021-05-03 23:51:50', 1, NULL, NULL, NULL, NULL, 0, NULL),
(3, 'http://192.168.1.157/elabo/video/6-park-hye-won-hyyns-new-song-%ed%95%9c-%eb%b2%88%eb%a7%8c-%eb%82%b4-%eb%a7%88%ec%9d%8c%eb%8c%80%eb%a1%9c-%ed%95%98%ec%9e%90%e3%85%a3oasis-live%e3%85%a3/', '186:4e55f8ad5ed8aa31ea0a4afb2ab575aa', 51, 'post', 'video', 1, 0, NULL, NULL, '6. Park Hye-won (HYYN)&#8217;s NEW SONG &#8216;한 번만 내 마음대로 하자&#8217;ㅣOasis LIVEㅣ', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 30, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/f531fec46cb7475ab03bb412c1344f77.jpg', NULL, '21', 'featured-image', NULL, NULL, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/f531fec46cb7475ab03bb412c1344f77.jpg', '21', 'featured-image', '{\"width\":900,\"height\":600,\"url\":\"http://192.168.1.157/elabo/wp-content/uploads/2021/04/f531fec46cb7475ab03bb412c1344f77.jpg\",\"path\":\"C:\\\\xampp\\\\htdocs\\\\elabo/wp-content/uploads/2021/04/f531fec46cb7475ab03bb412c1344f77.jpg\",\"size\":\"full\",\"id\":21,\"alt\":\"\",\"pixels\":540000,\"type\":\"image/jpeg\"}', 0, NULL, NULL, '2021-04-29 09:19:29', '2021-05-04 00:35:33', 1, NULL, NULL, NULL, NULL, 0, NULL),
(4, 'http://192.168.1.157/elabo/video/5-park-hye-won-hyyns-new-song-%ed%95%9c-%eb%b2%88%eb%a7%8c-%eb%82%b4-%eb%a7%88%ec%9d%8c%eb%8c%80%eb%a1%9c-%ed%95%98%ec%9e%90%e3%85%a3oasis-live%e3%85%a3/', '186:09a8c14e39ca961b04657956669896de', 50, 'post', 'video', 1, 0, NULL, NULL, '5. Park Hye-won (HYYN)&#8217;s NEW SONG &#8216;한 번만 내 마음대로 하자&#8217;ㅣOasis LIVEㅣ', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 90, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/JeALalala.jpg', NULL, '15', 'featured-image', NULL, NULL, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/JeALalala.jpg', '15', 'featured-image', '{\"width\":1920,\"height\":1080,\"url\":\"http://192.168.1.157/elabo/wp-content/uploads/2021/04/JeALalala.jpg\",\"path\":\"C:\\\\xampp\\\\htdocs\\\\elabo/wp-content/uploads/2021/04/JeALalala.jpg\",\"size\":\"full\",\"id\":15,\"alt\":\"\",\"pixels\":2073600,\"type\":\"image/jpeg\"}', 0, NULL, NULL, '2021-04-29 09:19:29', '2021-05-03 20:09:23', 1, NULL, NULL, NULL, NULL, 0, NULL),
(5, 'http://192.168.1.157/elabo/video/4-park-hye-won-hyyns-new-song-%ed%95%9c-%eb%b2%88%eb%a7%8c-%eb%82%b4-%eb%a7%88%ec%9d%8c%eb%8c%80%eb%a1%9c-%ed%95%98%ec%9e%90%e3%85%a3oasis-live%e3%85%a3/', '186:5986243287907d50c6aadd0102d0bb78', 49, 'post', 'video', 1, 0, NULL, NULL, '4. Park Hye-won (HYYN)&#8217;s NEW SONG &#8216;한 번만 내 마음대로 하자&#8217;ㅣOasis LIVEㅣ', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 90, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/no_image_available.jpg', NULL, '53', 'featured-image', NULL, NULL, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/no_image_available.jpg', '53', 'featured-image', '{\"width\":600,\"height\":600,\"url\":\"http://192.168.1.157/elabo/wp-content/uploads/2021/04/no_image_available.jpg\",\"path\":\"C:\\\\xampp\\\\htdocs\\\\elabo/wp-content/uploads/2021/04/no_image_available.jpg\",\"size\":\"full\",\"id\":53,\"alt\":\"\",\"pixels\":360000,\"type\":\"image/jpeg\"}', 0, NULL, NULL, '2021-04-29 09:19:29', '2021-05-03 20:10:16', 1, NULL, NULL, NULL, NULL, 0, NULL),
(6, 'http://192.168.1.157/elabo/video/3-park-hye-won-hyyns-new-song-%ed%95%9c-%eb%b2%88%eb%a7%8c-%eb%82%b4-%eb%a7%88%ec%9d%8c%eb%8c%80%eb%a1%9c-%ed%95%98%ec%9e%90%e3%85%a3oasis-live%e3%85%a3/', '186:1639b4a8a7a74f1728c060dc4c13c9b4', 48, 'post', 'video', 1, 0, NULL, NULL, '3. Park Hye-won (HYYN)&#8217;s NEW SONG &#8216;한 번만 내 마음대로 하자&#8217;ㅣOasis LIVEㅣ', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 90, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/580eb99da440318419e445bb83d884937c089497r1_647_1018v2_uhq.jpg', NULL, '19', 'featured-image', NULL, NULL, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/580eb99da440318419e445bb83d884937c089497r1_647_1018v2_uhq.jpg', '19', 'featured-image', '{\"width\":647,\"height\":1018,\"url\":\"http://192.168.1.157/elabo/wp-content/uploads/2021/04/580eb99da440318419e445bb83d884937c089497r1_647_1018v2_uhq.jpg\",\"path\":\"C:\\\\xampp\\\\htdocs\\\\elabo/wp-content/uploads/2021/04/580eb99da440318419e445bb83d884937c089497r1_647_1018v2_uhq.jpg\",\"size\":\"full\",\"id\":19,\"alt\":\"\",\"pixels\":658646,\"type\":\"image/jpeg\"}', 0, NULL, NULL, '2021-04-29 09:19:29', '2021-05-03 20:11:01', 1, NULL, NULL, NULL, NULL, 0, NULL),
(7, 'http://192.168.1.157/elabo/video/2-park-hye-won-hyyns-new-song-%ed%95%9c-%eb%b2%88%eb%a7%8c-%eb%82%b4-%eb%a7%88%ec%9d%8c%eb%8c%80%eb%a1%9c-%ed%95%98%ec%9e%90%e3%85%a3oasis-live%e3%85%a3/', '186:b6f9916e9adb42555754e80fb652ae31', 47, 'post', 'video', 1, 0, NULL, NULL, '2. Park Hye-won (HYYN)&#8217;s NEW SONG &#8216;한 번만 내 마음대로 하자&#8217;ㅣOasis LIVEㅣ', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 90, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/f531fec46cb7475ab03bb412c1344f77.jpg', NULL, '21', 'featured-image', NULL, NULL, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/f531fec46cb7475ab03bb412c1344f77.jpg', '21', 'featured-image', '{\"width\":900,\"height\":600,\"url\":\"http://192.168.1.157/elabo/wp-content/uploads/2021/04/f531fec46cb7475ab03bb412c1344f77.jpg\",\"path\":\"C:\\\\xampp\\\\htdocs\\\\elabo/wp-content/uploads/2021/04/f531fec46cb7475ab03bb412c1344f77.jpg\",\"size\":\"full\",\"id\":21,\"alt\":\"\",\"pixels\":540000,\"type\":\"image/jpeg\"}', 0, NULL, NULL, '2021-04-29 09:19:29', '2021-05-03 20:11:11', 1, NULL, NULL, NULL, NULL, 0, NULL),
(8, 'http://192.168.1.157/elabo/video/movies-03/', '43:f3831094d8a2f33f0074312aabfb65b1', 46, 'post', 'video', 1, 0, NULL, NULL, 'Movies 03', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-04-29 09:19:29', '2021-04-29 02:22:13', 1, NULL, NULL, NULL, NULL, 0, NULL),
(9, 'http://192.168.1.157/elabo/video/movie002/', '42:468b33c1d255f53576b4a537d8e352f8', 33, 'post', 'video', 1, 0, NULL, NULL, 'Movie002', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-04-29 09:19:29', '2021-04-29 02:22:13', 1, NULL, NULL, NULL, NULL, 0, NULL),
(10, 'http://192.168.1.157/elabo/video/movie-001/', '43:ee22eb3d52cd8781641f6dc4ea9a0a98', 27, 'post', 'video', 1, 0, NULL, NULL, 'Movie 001', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-04-29 09:19:29', '2021-04-29 02:22:13', 1, NULL, NULL, NULL, NULL, 0, NULL),
(11, 'http://192.168.1.157/elabo/video/4-14%e5%a4%a9%e5%b4%a9%e5%bc%80%e5%b1%80%ef%bc%81%e5%9b%bd%e6%9c%8d%e9%98%bf%e8%bd%b2%e8%87%aa%e5%b8%a6bug%e7%ba%a7%e9%ab%98%e8%83%bd%e5%9b%9b%e6%9d%80%e7%bf%bb%e7%9b%98%ef%bc%81/', '212:fc05ef07cc048f58f576271c145f8d89', 22, 'post', 'video', 1, 0, NULL, NULL, '4-14天崩开局！国服阿轲自带bug级高能四杀翻盘！', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/img-share.jpg', NULL, '23', 'featured-image', NULL, NULL, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/img-share.jpg', '23', 'featured-image', '{\"width\":1200,\"height\":630,\"url\":\"http://192.168.1.157/elabo/wp-content/uploads/2021/04/img-share.jpg\",\"path\":\"C:\\\\xampp\\\\htdocs\\\\elabo/wp-content/uploads/2021/04/img-share.jpg\",\"size\":\"full\",\"id\":23,\"alt\":\"\",\"pixels\":756000,\"type\":\"image/jpeg\"}', 0, NULL, NULL, '2021-04-29 09:19:29', '2021-05-03 20:11:30', 1, NULL, NULL, NULL, NULL, 0, NULL),
(12, 'http://192.168.1.157/elabo/video/got-mental-breakdown-while-getting-crazy-medical-checkups%e3%85%a3my-idols-medical-checkup-gwsn/', '129:1395c93e53fbae27776c4575f5ad9ca0', 20, 'post', 'video', 1, 0, NULL, NULL, 'Got Mental Breakdown While Getting Crazy Medical Checkups!ㅣMy Idol&#8217;s Medical Checkup | GWSN', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/f531fec46cb7475ab03bb412c1344f77.jpg', NULL, '21', 'featured-image', NULL, NULL, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/f531fec46cb7475ab03bb412c1344f77.jpg', '21', 'featured-image', '{\"width\":900,\"height\":600,\"url\":\"http://192.168.1.157/elabo/wp-content/uploads/2021/04/f531fec46cb7475ab03bb412c1344f77.jpg\",\"path\":\"C:\\\\xampp\\\\htdocs\\\\elabo/wp-content/uploads/2021/04/f531fec46cb7475ab03bb412c1344f77.jpg\",\"size\":\"full\",\"id\":21,\"alt\":\"\",\"pixels\":540000,\"type\":\"image/jpeg\"}', 0, NULL, NULL, '2021-04-29 09:19:29', '2021-04-29 02:22:13', 1, NULL, NULL, NULL, NULL, 0, NULL),
(13, 'http://192.168.1.157/elabo/video/park-hye-won-hyyns-new-song-%ed%95%9c-%eb%b2%88%eb%a7%8c-%eb%82%b4-%eb%a7%88%ec%9d%8c%eb%8c%80%eb%a1%9c-%ed%95%98%ec%9e%90%e3%85%a3oasis-live%e3%85%a3/', '184:341227c92f304b4f02b639b94d16baf7', 18, 'post', 'video', 1, 0, NULL, NULL, 'Park Hye-won (HYYN)&#8217;s NEW SONG &#8216;한 번만 내 마음대로 하자&#8217;ㅣOasis LIVEㅣ', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/580eb99da440318419e445bb83d884937c089497r1_647_1018v2_uhq.jpg', NULL, '19', 'featured-image', NULL, NULL, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/580eb99da440318419e445bb83d884937c089497r1_647_1018v2_uhq.jpg', '19', 'featured-image', '{\"width\":647,\"height\":1018,\"url\":\"http://192.168.1.157/elabo/wp-content/uploads/2021/04/580eb99da440318419e445bb83d884937c089497r1_647_1018v2_uhq.jpg\",\"path\":\"C:\\\\xampp\\\\htdocs\\\\elabo/wp-content/uploads/2021/04/580eb99da440318419e445bb83d884937c089497r1_647_1018v2_uhq.jpg\",\"size\":\"full\",\"id\":19,\"alt\":\"\",\"pixels\":658646,\"type\":\"image/jpeg\"}', 0, NULL, NULL, '2021-04-29 09:19:29', '2021-04-29 02:22:13', 1, NULL, NULL, NULL, NULL, 0, NULL),
(14, 'http://192.168.1.157/elabo/video/what-if-jea-is-the-professor-at-the-practical-music-interviewreal-tips%e3%85%a3jea-lalala-ep-10/', '129:c631d3add3646266badcc445d572c389', 14, 'post', 'video', 1, 0, NULL, NULL, 'What if JeA is the professor at the practical music interview!?Real tipsㅣJeA Lalala EP.10', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/JeALalala.jpg', NULL, '15', 'featured-image', NULL, NULL, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/JeALalala.jpg', '15', 'featured-image', '{\"width\":1920,\"height\":1080,\"url\":\"http://192.168.1.157/elabo/wp-content/uploads/2021/04/JeALalala.jpg\",\"path\":\"C:\\\\xampp\\\\htdocs\\\\elabo/wp-content/uploads/2021/04/JeALalala.jpg\",\"size\":\"full\",\"id\":15,\"alt\":\"\",\"pixels\":2073600,\"type\":\"image/jpeg\"}', 0, NULL, NULL, '2021-04-29 09:19:29', '2021-04-29 02:22:13', 1, NULL, NULL, NULL, NULL, 0, NULL),
(15, 'http://192.168.1.157/elabo/category/uncategorized/', '50:e42b4f8e9671e360fa22fd8953309d14', 1, 'term', 'category', NULL, NULL, NULL, NULL, 'Uncategorized', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-04-29 09:22:05', '2021-04-29 02:22:14', 1, NULL, NULL, NULL, NULL, 0, NULL),
(16, 'http://192.168.1.157/elabo/video_tag/3d/', '40:43facba623c5f7414ff9a5cfb31a19f8', 9, 'term', 'video_tag', NULL, NULL, NULL, NULL, '3D', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-04-29 09:22:05', '2021-04-29 02:22:14', 1, NULL, NULL, NULL, NULL, 0, NULL),
(17, 'http://192.168.1.157/elabo/video_tag/genre/', '43:57e39fe9d7ce0c2b2eadfdda65bb187d', 34, 'term', 'video_tag', NULL, NULL, NULL, NULL, 'Genre', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-04-29 09:22:05', '2021-04-29 02:22:19', 1, NULL, NULL, NULL, NULL, 0, NULL),
(18, 'http://192.168.1.157/elabo/video_tag/abuse/', '43:372867b5b171c4f6f7dc3f87736fefe5', 10, 'term', 'video_tag', NULL, NULL, NULL, NULL, 'Abuse', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-04-29 09:22:05', '2021-04-29 02:22:15', 1, NULL, NULL, NULL, NULL, 0, NULL),
(19, 'http://192.168.1.157/elabo/video_tag/cast/', '42:64011a30ab4aeb92e39b42517208bde8', 33, 'term', 'video_tag', NULL, NULL, NULL, NULL, 'Cast', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-04-29 09:22:05', '2021-04-29 02:22:19', 1, NULL, NULL, NULL, NULL, 0, NULL),
(20, 'http://192.168.1.157/elabo/video_tag/adult-awards/', '50:24085836b77fcb85642ad7498dc4c6ce', 11, 'term', 'video_tag', NULL, NULL, NULL, NULL, 'Adult Awards', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-04-29 09:22:05', '2021-04-29 02:22:15', 1, NULL, NULL, NULL, NULL, 0, NULL),
(21, 'http://192.168.1.157/elabo/video_tag/anchorwoman/', '49:245a8920462331082eb907f5e7d56497', 12, 'term', 'video_tag', NULL, NULL, NULL, NULL, 'Anchorwoman', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-04-29 09:22:05', '2021-04-29 02:22:15', 1, NULL, NULL, NULL, NULL, 0, NULL),
(22, 'http://192.168.1.157/elabo/video_tag/aunt/', '42:ebf2e69af18841d5961b174c6668d96e', 13, 'term', 'video_tag', NULL, NULL, NULL, NULL, 'Aunt', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-04-29 09:22:06', '2021-04-29 02:22:15', 1, NULL, NULL, NULL, NULL, 0, NULL),
(23, 'http://192.168.1.157/elabo/video_tag/av-open-2014-super-heavyweight/', '68:d2e3f6874e0244ae0aaf3bccb1a1655e', 14, 'term', 'video_tag', NULL, NULL, NULL, NULL, 'AV OPEN 2014 Super Heavyweight', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-04-29 09:22:06', '2021-04-29 02:22:15', 1, NULL, NULL, NULL, NULL, 0, NULL),
(24, 'http://192.168.1.157/elabo/video_tag/av-open-2016-hardcore-dept/', '64:4afd976094184adf78df477b445a6d8e', 15, 'term', 'video_tag', NULL, NULL, NULL, NULL, 'AV OPEN 2016 Hardcore Dept', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-04-29 09:22:06', '2021-04-29 02:22:15', 1, NULL, NULL, NULL, NULL, 0, NULL),
(25, 'http://192.168.1.157/elabo/video_tag/abe-kanna/', '47:01d88f346b45a75f4094f748becbb272', 16, 'term', 'video_tag', NULL, NULL, NULL, NULL, 'Abe Kanna', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-04-29 09:22:06', '2021-04-29 02:22:15', 1, NULL, NULL, NULL, NULL, 0, NULL),
(26, 'http://192.168.1.157/elabo/video_tag/maker/', '43:787152b50dcee84b6b9d1419235a265f', 35, 'term', 'video_tag', NULL, NULL, NULL, NULL, 'Maker', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-04-29 09:22:06', '2021-04-29 02:22:19', 1, NULL, NULL, NULL, NULL, 0, NULL),
(27, 'http://192.168.1.157/elabo/video_tag/adachi-mami/', '49:55a06751f0833ac90c34e69dc5ce00c4', 17, 'term', 'video_tag', NULL, NULL, NULL, NULL, 'Adachi Mami', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-04-29 09:22:06', '2021-04-29 02:22:15', 1, NULL, NULL, NULL, NULL, 0, NULL),
(28, 'http://192.168.1.157/elabo/video_tag/aagawa-nanako/', '51:c9a2648c7dbe4970ee0650173a8cb0b5', 18, 'term', 'video_tag', NULL, NULL, NULL, NULL, 'Agawa Nanako', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-04-29 09:22:06', '2021-04-29 02:22:16', 1, NULL, NULL, NULL, NULL, 0, NULL),
(29, 'http://192.168.1.157/elabo/video_tag/ai-kawana/', '47:dd438d2a539425b8d57a3fb18df31d93', 19, 'term', 'video_tag', NULL, NULL, NULL, NULL, 'Ai Kawana', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-04-29 09:22:06', '2021-04-29 02:22:17', 1, NULL, NULL, NULL, NULL, 0, NULL),
(30, 'http://192.168.1.157/elabo/video_tag/amateur-fishing/', '53:b7084da2ef783ff9d1045444388e2a0e', 20, 'term', 'video_tag', NULL, NULL, NULL, NULL, 'Amateur Fishing', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-04-29 09:22:06', '2021-04-29 02:22:17', 1, NULL, NULL, NULL, NULL, 0, NULL),
(31, 'http://192.168.1.157/elabo/video_tag/amateur-hey-hey-power/', '59:4d317194f92c6eb1c384459380a270f6', 21, 'term', 'video_tag', NULL, NULL, NULL, NULL, 'Amateur Hey-hey Power', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-04-29 09:22:06', '2021-04-29 02:22:17', 1, NULL, NULL, NULL, NULL, 0, NULL),
(32, 'http://192.168.1.157/elabo/categories/popular/', '46:55d6ff744009be52b31e62ced08b1e91', 22, 'term', 'categories', NULL, NULL, NULL, NULL, 'Popular', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-04-29 09:22:06', '2021-04-29 02:22:17', 1, NULL, NULL, NULL, NULL, 0, NULL),
(33, 'http://192.168.1.157/elabo/categories/censored/', '47:bf8073c7c548e60e2fd2c47f9ebb4a67', 23, 'term', 'categories', NULL, NULL, NULL, NULL, 'Censored JAV', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-04-29 09:22:06', '2021-04-29 02:22:18', 1, NULL, NULL, NULL, NULL, 0, NULL),
(34, 'http://192.168.1.157/elabo/categories/uncensored/', '49:e231c405bbab26b082a8cc8fbe96221c', 24, 'term', 'categories', NULL, NULL, NULL, NULL, 'Uncensored JAV', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-04-29 09:22:06', '2021-04-29 02:22:18', 1, NULL, NULL, NULL, NULL, 0, NULL),
(35, 'http://192.168.1.157/elabo/categories/amateur/', '46:583bb3d067da8e184535f93029974823', 25, 'term', 'categories', NULL, NULL, NULL, NULL, 'Amateur', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-04-29 09:22:06', '2021-04-29 02:22:18', 1, NULL, NULL, NULL, NULL, 0, NULL),
(36, 'http://192.168.1.157/elabo/categories/chinese-subtitles/', '56:551b8c496dad2c013c2d71b16ed1cf2e', 26, 'term', 'categories', NULL, NULL, NULL, NULL, 'Chinese Subtitles', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-04-29 09:22:06', '2021-04-29 02:22:18', 1, NULL, NULL, NULL, NULL, 0, NULL),
(37, 'http://192.168.1.157/elabo/categories/reducing-mosaic/', '54:a5b52d6d0fab9451d1eac66d3b534d5d', 27, 'term', 'categories', NULL, NULL, NULL, NULL, 'Reducing Mosaic', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-04-29 09:22:07', '2021-04-29 02:22:18', 1, NULL, NULL, NULL, NULL, 0, NULL),
(38, 'http://192.168.1.157/elabo/categories/english-subtitles/', '56:1dffdebf8d61fa5d7a5b4165a4c32322', 28, 'term', 'categories', NULL, NULL, NULL, NULL, 'English Subtitles', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-04-29 09:22:07', '2021-04-29 02:22:19', 1, NULL, NULL, NULL, NULL, 0, NULL),
(39, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/JeALalala.jpg', '67:56f63731376a16b1aab676035fe148c1', 15, 'post', 'attachment', 1, 14, NULL, NULL, 'JeALalala', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/JeALalala.jpg', NULL, '15', 'attachment-image', NULL, NULL, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/JeALalala.jpg', '15', 'attachment-image', '{\"width\":1920,\"height\":1080,\"url\":\"http://192.168.1.157/elabo/wp-content/uploads/2021/04/JeALalala.jpg\",\"path\":\"C:\\\\xampp\\\\htdocs\\\\elabo/wp-content/uploads/2021/04/JeALalala.jpg\",\"size\":\"full\",\"id\":15,\"alt\":\"\",\"pixels\":2073600,\"type\":\"image/jpeg\"}', NULL, NULL, NULL, '2021-04-29 09:22:07', '2021-04-29 02:22:07', 1, NULL, NULL, NULL, NULL, 0, NULL),
(40, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/580eb99da440318419e445bb83d884937c089497r1_647_1018v2_uhq.jpg', '115:40febe6aaeebd3e2d4a181bfbc1a1a7e', 19, 'post', 'attachment', 1, 18, NULL, NULL, '580eb99da440318419e445bb83d884937c089497r1_647_1018v2_uhq', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/580eb99da440318419e445bb83d884937c089497r1_647_1018v2_uhq.jpg', NULL, '19', 'attachment-image', NULL, NULL, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/580eb99da440318419e445bb83d884937c089497r1_647_1018v2_uhq.jpg', '19', 'attachment-image', '{\"width\":647,\"height\":1018,\"url\":\"http://192.168.1.157/elabo/wp-content/uploads/2021/04/580eb99da440318419e445bb83d884937c089497r1_647_1018v2_uhq.jpg\",\"path\":\"C:\\\\xampp\\\\htdocs\\\\elabo/wp-content/uploads/2021/04/580eb99da440318419e445bb83d884937c089497r1_647_1018v2_uhq.jpg\",\"size\":\"full\",\"id\":19,\"alt\":\"\",\"pixels\":658646,\"type\":\"image/jpeg\"}', NULL, NULL, NULL, '2021-04-29 09:22:08', '2021-04-29 02:22:08', 1, NULL, NULL, NULL, NULL, 0, NULL),
(41, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/f531fec46cb7475ab03bb412c1344f77.jpg', '90:2838915c5a895db81606ef34b84cb90c', 21, 'post', 'attachment', 1, 20, NULL, NULL, 'f531fec46cb7475ab03bb412c1344f77', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/f531fec46cb7475ab03bb412c1344f77.jpg', NULL, '21', 'attachment-image', NULL, NULL, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/f531fec46cb7475ab03bb412c1344f77.jpg', '21', 'attachment-image', '{\"width\":900,\"height\":600,\"url\":\"http://192.168.1.157/elabo/wp-content/uploads/2021/04/f531fec46cb7475ab03bb412c1344f77.jpg\",\"path\":\"C:\\\\xampp\\\\htdocs\\\\elabo/wp-content/uploads/2021/04/f531fec46cb7475ab03bb412c1344f77.jpg\",\"size\":\"full\",\"id\":21,\"alt\":\"\",\"pixels\":540000,\"type\":\"image/jpeg\"}', NULL, NULL, NULL, '2021-04-29 09:22:08', '2021-04-29 02:22:08', 1, NULL, NULL, NULL, NULL, 0, NULL),
(42, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/img-share.jpg', '67:5ee3f7a0764ff7c433758b5eb22ec046', 23, 'post', 'attachment', 1, 22, NULL, NULL, 'img-share', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/img-share.jpg', NULL, '23', 'attachment-image', NULL, NULL, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/img-share.jpg', '23', 'attachment-image', '{\"width\":1200,\"height\":630,\"url\":\"http://192.168.1.157/elabo/wp-content/uploads/2021/04/img-share.jpg\",\"path\":\"C:\\\\xampp\\\\htdocs\\\\elabo/wp-content/uploads/2021/04/img-share.jpg\",\"size\":\"full\",\"id\":23,\"alt\":\"\",\"pixels\":756000,\"type\":\"image/jpeg\"}', NULL, NULL, NULL, '2021-04-29 09:22:08', '2021-04-29 02:22:08', 1, NULL, NULL, NULL, NULL, 0, NULL),
(43, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/pc_login.jpg', '66:6a539776b11cb8e7b49b1a521b9bf9e8', 29, 'post', 'attachment', 1, 27, NULL, NULL, 'pc_login', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/pc_login.jpg', NULL, '29', 'attachment-image', NULL, NULL, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/pc_login-1024x897.jpg', '29', 'attachment-image', '{\"width\":1024,\"height\":897,\"path\":\"2021/04/pc_login-1024x897.jpg\",\"url\":\"http://192.168.1.157/elabo/wp-content/uploads/2021/04/pc_login-1024x897.jpg\",\"size\":\"large\",\"id\":29,\"alt\":\"\",\"pixels\":918528,\"type\":\"image/jpeg\"}', NULL, NULL, NULL, '2021-04-29 09:22:08', '2021-04-29 02:22:08', 1, NULL, NULL, NULL, NULL, 0, NULL),
(44, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/no_image_available.jpg', '76:6e96869b17bb52aa2260653c8d438217', 53, 'post', 'attachment', 1, 0, NULL, NULL, 'no_image_available', 'inherit', 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/no_image_available.jpg', NULL, '53', 'attachment-image', NULL, NULL, 'http://192.168.1.157/elabo/wp-content/uploads/2021/04/no_image_available.jpg', '53', 'attachment-image', '{\"width\":600,\"height\":600,\"url\":\"http://192.168.1.157/elabo/wp-content/uploads/2021/04/no_image_available.jpg\",\"path\":\"C:\\\\xampp\\\\htdocs\\\\elabo/wp-content/uploads/2021/04/no_image_available.jpg\",\"size\":\"full\",\"id\":53,\"alt\":\"\",\"pixels\":360000,\"type\":\"image/jpeg\"}', NULL, NULL, NULL, '2021-04-29 09:22:08', '2021-04-29 02:22:08', 1, NULL, NULL, NULL, NULL, 0, NULL),
(45, 'http://192.168.1.157/elabo/author/', '34:718ae27f4857128188ac9e1821876cf9', 77777, 'user', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 'https://2.gravatar.com/avatar/?s=500&d=mm&r=g', NULL, NULL, 'gravatar-image', NULL, NULL, 'https://2.gravatar.com/avatar/?s=500&d=mm&r=g', NULL, 'gravatar-image', NULL, NULL, NULL, NULL, '2021-04-29 09:22:08', '2021-04-29 02:22:08', 1, NULL, NULL, NULL, NULL, 0, NULL),
(46, 'https://hivepress.io/wp-content/uploads/2021/01/logo.svg', '56:6b4c38dbc2ad5ea185a03ddf70032aea', 70, 'post', 'attachment', 77777, 52, NULL, NULL, '7. Park Hye-won (HYYN)’s NEW SONG ‘한 번만 내 마음대로 하자’ㅣOasis LIVEㅣ', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'https://hivepress.io/wp-content/uploads/2021/01/logo.svg', NULL, '70', 'attachment-image', NULL, NULL, 'https://hivepress.io/wp-content/uploads/2021/01/logo.svg', '70', 'attachment-image', '{\"width\":\"186\",\"height\":\"40\",\"url\":\"https://hivepress.io/wp-content/uploads/2021/01/logo.svg\",\"path\":\"https://hivepress.io/wp-content/uploads/2021/01/logo.svg\",\"size\":\"full\",\"id\":70,\"alt\":\"7. Park Hye-won (HYYN)\\u2019s NEW SONG \\u2018\\ud55c \\ubc88\\ub9cc \\ub0b4 \\ub9c8\\uc74c\\ub300\\ub85c \\ud558\\uc790\\u2019\\u3163Oasis LIVE\\u3163\",\"pixels\":7440,\"type\":\"image/jpeg\"}', NULL, NULL, NULL, '2021-04-29 09:22:08', '2021-04-29 02:22:08', 1, NULL, NULL, NULL, NULL, 0, NULL),
(47, 'http://192.168.1.157/elabo/?page_id=3', '37:ddadd88e0c59cf3ec3edf2fb911da76c', 3, 'post', 'page', 1, 0, NULL, NULL, 'Privacy Policy', 'draft', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-29 09:22:08', '2021-04-29 02:22:08', 1, NULL, NULL, NULL, NULL, 0, NULL),
(48, 'http://192.168.1.157/elabo/?page_id=64', '38:deb54601a97197314e5fdbf9519efb37', 64, 'post', 'page', 1, 0, NULL, NULL, 'Tag Groups (Free) Sample Page &#8211; Gutenberg Editor', 'draft', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-29 09:22:08', '2021-04-29 02:22:08', 1, NULL, NULL, NULL, NULL, 0, NULL),
(49, 'http://192.168.1.157/elabo/', '27:cb10def602e29ac0cdf408f0974a8405', 2, 'post', 'page', 1, 0, NULL, NULL, 'Home Page', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-04-29 09:22:08', '2021-04-29 02:53:53', 1, NULL, NULL, NULL, NULL, 0, 0),
(50, 'http://192.168.1.157/elabo/?p=4', '31:36f91ad2bd12557b8b5a91811156fa22', 4, 'post', 'post', 1, 0, NULL, NULL, 'Auto Draft', 'auto-draft', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-29 09:22:08', '2021-04-29 02:22:08', 1, NULL, NULL, NULL, NULL, 0, NULL),
(51, 'http://192.168.1.157/elabo/?p=39', '32:0d0c5de4fcb7e529efc482a18bf77088', 39, 'post', 'post', 1, 0, NULL, NULL, 'Auto Draft', 'auto-draft', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-29 09:22:08', '2021-04-29 02:22:08', 1, NULL, NULL, NULL, NULL, 0, NULL),
(52, 'http://192.168.1.157/elabo/?p=74', '32:0ee71c5a24ed16066f8cf16dbcc69100', 74, 'post', 'post', 1, 0, NULL, NULL, 'Auto Draft', 'auto-draft', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-29 09:22:08', '2021-04-29 02:22:09', 1, NULL, NULL, NULL, NULL, 0, NULL),
(53, 'http://192.168.1.157/elabo/?p=1', '31:49e5c40f7a2d217570cb30dfbeef8b74', 1, 'post', 'post', 1, 0, NULL, NULL, 'Hello world!', 'trash', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-29 09:22:10', '2021-04-29 02:22:10', 1, NULL, NULL, NULL, NULL, 0, NULL),
(54, 'http://192.168.1.157/elabo/?post_type=video&p=30', '48:3887c366e7eca9edf0301d6254a255a4', 30, 'post', 'video', 1, 0, NULL, NULL, 'Auto Draft', 'auto-draft', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-29 09:22:10', '2021-04-29 02:22:10', 1, NULL, NULL, NULL, NULL, 0, NULL),
(55, 'http://192.168.1.157/elabo/?post_type=video&p=41', '48:84906ea54a83c11e644511b81786093b', 41, 'post', 'video', 1, 0, NULL, NULL, 'Auto Draft', 'auto-draft', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-29 09:22:10', '2021-04-29 02:22:10', 1, NULL, NULL, NULL, NULL, 0, NULL),
(56, 'http://192.168.1.157/elabo/?post_type=video&p=42', '48:dda3a073ff28b42b648d1699aa503b3a', 42, 'post', 'video', 1, 0, NULL, NULL, 'Auto Draft', 'auto-draft', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-29 09:22:10', '2021-04-29 02:22:10', 1, NULL, NULL, NULL, NULL, 0, NULL),
(57, 'http://192.168.1.157/elabo/?post_type=video&p=71', '48:3632335ee61add605c2a6a575bb08cde', 71, 'post', 'video', 1, 0, NULL, NULL, 'Auto Draft', 'auto-draft', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-29 09:22:10', '2021-04-29 02:22:10', 1, NULL, NULL, NULL, NULL, 0, NULL),
(58, 'http://192.168.1.157/elabo/video/', '33:ea467b34baa40d38861795fa8a4f532a', NULL, 'post-type-archive', 'video', NULL, NULL, '%%pt_plural%% Archive %%page%% %%sep%% %%sitename%%', '', 'Videos', NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-29 09:22:11', '2021-05-04 01:30:36', 1, NULL, NULL, NULL, NULL, 0, NULL),
(59, NULL, NULL, NULL, 'system-page', '404', NULL, NULL, 'Page not found %%sep%% %%sitename%%', NULL, 'Error 404: Page not found', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-29 09:22:11', '2021-04-29 02:22:11', 1, NULL, NULL, NULL, NULL, 0, NULL),
(60, NULL, NULL, NULL, 'system-page', 'search-result', NULL, NULL, 'You searched for %%searchphrase%% %%page%% %%sep%% %%sitename%%', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-29 09:22:11', '2021-04-29 02:22:11', 1, NULL, NULL, NULL, NULL, 0, NULL),
(61, NULL, NULL, NULL, 'date-archive', NULL, NULL, NULL, '%%date%% %%page%% %%sep%% %%sitename%%', '', NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-29 09:22:11', '2021-04-29 02:22:11', 1, NULL, NULL, NULL, NULL, 0, NULL),
(62, 'http://192.168.1.157/elabo/', '27:cb10def602e29ac0cdf408f0974a8405', NULL, 'home-page', NULL, NULL, NULL, '%%sitename%% %%page%% %%sep%% %%sitedesc%%', '', 'Home', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, '2021-04-29 09:22:28', '2021-04-29 02:22:28', 1, NULL, NULL, NULL, NULL, 0, NULL),
(63, 'http://192.168.1.157/elabo/?post_type=video&p=79', '48:dce54a5532199b55079e6dfd5c99cb24', 79, 'post', 'video', 1, 0, NULL, NULL, 'test', 'trash', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'https://wpastra.com/wp-content/uploads/2019/05/astra-logo.svg', NULL, '80', 'featured-image', NULL, NULL, 'https://wpastra.com/wp-content/uploads/2019/05/astra-logo.svg', '80', 'featured-image', '{\"width\":\"155\",\"height\":\"45\",\"url\":\"https://wpastra.com/wp-content/uploads/2019/05/astra-logo.svg\",\"path\":\"https://wpastra.com/wp-content/uploads/2019/05/astra-logo.svg\",\"size\":\"full\",\"id\":80,\"alt\":\"test\",\"pixels\":6975,\"type\":\"image/jpeg\"}', 0, NULL, NULL, '2021-05-04 08:28:16', '2021-05-04 01:30:17', 1, NULL, NULL, NULL, NULL, 0, NULL),
(64, 'http://192.168.1.157/elabo/video/test-video/', '44:0a018ed609e61ab322cd0a4830664e73', 81, 'post', 'video', 1, 0, NULL, NULL, 'test video', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 60, 0, NULL, 0, NULL, NULL, NULL, NULL, 'https://wpastra.com/wp-content/uploads/2019/05/astra-logo.svg', NULL, '82', 'featured-image', NULL, NULL, 'https://wpastra.com/wp-content/uploads/2019/05/astra-logo.svg', '82', 'featured-image', '{\"width\":\"155\",\"height\":\"45\",\"url\":\"https://wpastra.com/wp-content/uploads/2019/05/astra-logo.svg\",\"path\":\"https://wpastra.com/wp-content/uploads/2019/05/astra-logo.svg\",\"size\":\"full\",\"id\":82,\"alt\":\"test video\",\"pixels\":6975,\"type\":\"image/jpeg\"}', 0, NULL, NULL, '2021-05-04 08:30:28', '2021-05-04 01:30:36', 1, NULL, NULL, NULL, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `wp_yoast_indexable_hierarchy`
--

CREATE TABLE `wp_yoast_indexable_hierarchy` (
  `indexable_id` int(11) UNSIGNED NOT NULL,
  `ancestor_id` int(11) UNSIGNED NOT NULL,
  `depth` int(11) UNSIGNED DEFAULT NULL,
  `blog_id` bigint(20) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `wp_yoast_indexable_hierarchy`
--

INSERT INTO `wp_yoast_indexable_hierarchy` (`indexable_id`, `ancestor_id`, `depth`, `blog_id`) VALUES
(1, 0, 0, 1),
(2, 0, 0, 1),
(3, 0, 0, 1),
(4, 0, 0, 1),
(5, 0, 0, 1),
(6, 0, 0, 1),
(7, 0, 0, 1),
(8, 0, 0, 1),
(9, 0, 0, 1),
(10, 0, 0, 1),
(11, 0, 0, 1),
(12, 0, 0, 1),
(13, 0, 0, 1),
(14, 0, 0, 1),
(15, 0, 0, 1),
(16, 17, 1, 1),
(17, 0, 0, 1),
(18, 19, 1, 1),
(19, 0, 0, 1),
(20, 19, 1, 1),
(21, 19, 1, 1),
(22, 19, 1, 1),
(23, 17, 1, 1),
(24, 17, 1, 1),
(25, 26, 1, 1),
(26, 0, 0, 1),
(27, 17, 1, 1),
(28, 19, 1, 1),
(29, 19, 1, 1),
(30, 26, 1, 1),
(31, 17, 1, 1),
(32, 0, 0, 1),
(33, 0, 0, 1),
(34, 0, 0, 1),
(35, 0, 0, 1),
(36, 0, 0, 1),
(37, 0, 0, 1),
(38, 0, 0, 1),
(39, 14, 1, 1),
(40, 13, 1, 1),
(41, 12, 1, 1),
(42, 11, 1, 1),
(43, 10, 1, 1),
(44, 0, 0, 1),
(46, 2, 1, 1),
(47, 0, 0, 1),
(48, 0, 0, 1),
(49, 0, 0, 1),
(50, 0, 0, 1),
(51, 0, 0, 1),
(52, 0, 0, 1),
(53, 0, 0, 1),
(54, 0, 0, 1),
(55, 0, 0, 1),
(56, 0, 0, 1),
(57, 0, 0, 1),
(59, 0, 0, 1),
(60, 0, 0, 1),
(62, 0, 0, 1),
(63, 0, 0, 1),
(64, 0, 0, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `wp_yoast_migrations`
--

CREATE TABLE `wp_yoast_migrations` (
  `id` int(11) UNSIGNED NOT NULL,
  `version` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `wp_yoast_migrations`
--

INSERT INTO `wp_yoast_migrations` (`id`, `version`) VALUES
(1, '20171228151840'),
(2, '20171228151841'),
(3, '20190529075038'),
(4, '20191011111109'),
(5, '20200408101900'),
(6, '20200420073606'),
(7, '20200428123747'),
(8, '20200428194858'),
(9, '20200429105310'),
(10, '20200430075614'),
(11, '20200430150130'),
(12, '20200507054848'),
(13, '20200513133401'),
(14, '20200609154515'),
(15, '20200616130143'),
(16, '20200617122511'),
(17, '20200702141921'),
(18, '20200728095334'),
(19, '20201202144329'),
(20, '20201216124002'),
(21, '20201216141134');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `wp_yoast_primary_term`
--

CREATE TABLE `wp_yoast_primary_term` (
  `id` int(11) UNSIGNED NOT NULL,
  `post_id` bigint(20) DEFAULT NULL,
  `term_id` bigint(20) DEFAULT NULL,
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `blog_id` bigint(20) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `wp_yoast_primary_term`
--

INSERT INTO `wp_yoast_primary_term` (`id`, `post_id`, `term_id`, `taxonomy`, `created_at`, `updated_at`, `blog_id`) VALUES
(1, 51, 24, 'categories', '2021-05-04 03:09:08', '2021-05-04 00:35:33', 1),
(2, 50, 22, 'categories', '2021-05-04 03:09:23', '2021-05-03 20:09:23', 1),
(3, 49, 22, 'categories', '2021-05-04 03:09:41', '2021-05-03 20:10:16', 1),
(4, 48, 22, 'categories', '2021-05-04 03:11:01', '2021-05-03 20:11:01', 1),
(5, 47, 22, 'categories', '2021-05-04 03:11:11', '2021-05-03 20:11:11', 1),
(6, 22, 22, 'categories', '2021-05-04 03:11:30', '2021-05-03 20:11:30', 1),
(7, 52, 10, 'video_tag', '2021-05-04 06:51:50', '2021-05-03 23:51:50', 1),
(8, 51, 10, 'video_tag', '2021-05-04 07:12:26', '2021-05-04 00:35:33', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `wp_yoast_seo_links`
--

CREATE TABLE `wp_yoast_seo_links` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `post_id` bigint(20) UNSIGNED DEFAULT NULL,
  `target_post_id` bigint(20) UNSIGNED DEFAULT NULL,
  `type` varchar(8) DEFAULT NULL,
  `indexable_id` int(11) UNSIGNED DEFAULT NULL,
  `target_indexable_id` int(11) UNSIGNED DEFAULT NULL,
  `height` int(11) UNSIGNED DEFAULT NULL,
  `width` int(11) UNSIGNED DEFAULT NULL,
  `size` int(11) UNSIGNED DEFAULT NULL,
  `language` varchar(32) DEFAULT NULL,
  `region` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `wp_yoast_seo_links`
--

INSERT INTO `wp_yoast_seo_links` (`id`, `url`, `post_id`, `target_post_id`, `type`, `indexable_id`, `target_indexable_id`, `height`, `width`, `size`, `language`, `region`) VALUES
(2, 'http://bit.ly/2RpCcV1', 20, NULL, 'external', 12, NULL, NULL, NULL, NULL, NULL, NULL);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Chỉ mục cho bảng `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Chỉ mục cho bảng `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Chỉ mục cho bảng `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`),
  ADD KEY `autoload` (`autoload`);

--
-- Chỉ mục cho bảng `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Chỉ mục cho bảng `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Chỉ mục cho bảng `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Chỉ mục cho bảng `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Chỉ mục cho bảng `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Chỉ mục cho bảng `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Chỉ mục cho bảng `wp_trp_dictionary_ja_en_us`
--
ALTER TABLE `wp_trp_dictionary_ja_en_us`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `index_name` (`original`(100));
ALTER TABLE `wp_trp_dictionary_ja_en_us` ADD FULLTEXT KEY `original_fulltext` (`original`);

--
-- Chỉ mục cho bảng `wp_trp_gettext_en_us`
--
ALTER TABLE `wp_trp_gettext_en_us`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `index_name` (`original`(100));
ALTER TABLE `wp_trp_gettext_en_us` ADD FULLTEXT KEY `original_fulltext` (`original`);

--
-- Chỉ mục cho bảng `wp_trp_gettext_ja`
--
ALTER TABLE `wp_trp_gettext_ja`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `index_name` (`original`(100));
ALTER TABLE `wp_trp_gettext_ja` ADD FULLTEXT KEY `original_fulltext` (`original`);

--
-- Chỉ mục cho bảng `wp_trp_original_meta`
--
ALTER TABLE `wp_trp_original_meta`
  ADD PRIMARY KEY (`meta_id`),
  ADD UNIQUE KEY `meta_id` (`meta_id`),
  ADD KEY `index_original_id` (`original_id`),
  ADD KEY `meta_key` (`meta_key`);

--
-- Chỉ mục cho bảng `wp_trp_original_strings`
--
ALTER TABLE `wp_trp_original_strings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_original` (`original`(100));

--
-- Chỉ mục cho bảng `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Chỉ mục cho bảng `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- Chỉ mục cho bảng `wp_yoast_indexable`
--
ALTER TABLE `wp_yoast_indexable`
  ADD PRIMARY KEY (`id`),
  ADD KEY `object_type_and_sub_type` (`object_type`,`object_sub_type`),
  ADD KEY `object_id_and_type` (`object_id`,`object_type`),
  ADD KEY `permalink_hash_and_object_type` (`permalink_hash`,`object_type`),
  ADD KEY `subpages` (`post_parent`,`object_type`,`post_status`,`object_id`),
  ADD KEY `prominent_words` (`prominent_words_version`,`object_type`,`object_sub_type`,`post_status`);

--
-- Chỉ mục cho bảng `wp_yoast_indexable_hierarchy`
--
ALTER TABLE `wp_yoast_indexable_hierarchy`
  ADD PRIMARY KEY (`indexable_id`,`ancestor_id`),
  ADD KEY `indexable_id` (`indexable_id`),
  ADD KEY `ancestor_id` (`ancestor_id`),
  ADD KEY `depth` (`depth`);

--
-- Chỉ mục cho bảng `wp_yoast_migrations`
--
ALTER TABLE `wp_yoast_migrations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `wp_yoast_migrations_version` (`version`);

--
-- Chỉ mục cho bảng `wp_yoast_primary_term`
--
ALTER TABLE `wp_yoast_primary_term`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_taxonomy` (`post_id`,`taxonomy`),
  ADD KEY `post_term` (`post_id`,`term_id`);

--
-- Chỉ mục cho bảng `wp_yoast_seo_links`
--
ALTER TABLE `wp_yoast_seo_links`
  ADD PRIMARY KEY (`id`),
  ADD KEY `link_direction` (`post_id`,`type`),
  ADD KEY `indexable_link_direction` (`indexable_id`,`type`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=604;

--
-- AUTO_INCREMENT cho bảng `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=638;

--
-- AUTO_INCREMENT cho bảng `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;

--
-- AUTO_INCREMENT cho bảng `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT cho bảng `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT cho bảng `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT cho bảng `wp_trp_dictionary_ja_en_us`
--
ALTER TABLE `wp_trp_dictionary_ja_en_us`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT cho bảng `wp_trp_gettext_en_us`
--
ALTER TABLE `wp_trp_gettext_en_us`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT cho bảng `wp_trp_gettext_ja`
--
ALTER TABLE `wp_trp_gettext_ja`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT cho bảng `wp_trp_original_meta`
--
ALTER TABLE `wp_trp_original_meta`
  MODIFY `meta_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT cho bảng `wp_trp_original_strings`
--
ALTER TABLE `wp_trp_original_strings`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT cho bảng `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT cho bảng `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `wp_yoast_indexable`
--
ALTER TABLE `wp_yoast_indexable`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT cho bảng `wp_yoast_migrations`
--
ALTER TABLE `wp_yoast_migrations`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT cho bảng `wp_yoast_primary_term`
--
ALTER TABLE `wp_yoast_primary_term`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT cho bảng `wp_yoast_seo_links`
--
ALTER TABLE `wp_yoast_seo_links`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
