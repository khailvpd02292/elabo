<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp_elabo' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '$<nlQY!4[a(%;6WhT}j4;uYD+o<^oP/GbSl/~@SV;x~^OEC3E8KJ>fWyaW8+i G1' );
define( 'SECURE_AUTH_KEY',  '~8acgfBd WGp10QM2 1AO?aH+oDZ:DvURvqnzVG~*xX^&X *o2dtp!oyNBIO>d#d' );
define( 'LOGGED_IN_KEY',    '5CSK_xc4~RY_$%Dhm5;ZU_8x}baxG&%Ogrt)o1;?J_Z^66d.(zB`?pUZ6xdYX&vK' );
define( 'NONCE_KEY',        'F!+(HU)&t&5q=(5!C:/fbb7:?AmHOKZ&6TT8U3V$9ARWM_gF+$&JKuc57=tE}=jn' );
define( 'AUTH_SALT',        'W,ZVY2+Kp6egP3!t%?3)+cr(g siN<&ifa%^Ovuhpp|6bm2>OzE#*znM1RSbX$26' );
define( 'SECURE_AUTH_SALT', '2%c|YJ(uni?9 ?gguQ^VnPS&oMB]5~9OB=&HEI[=`X{{zA8%G4R#QBQ48^l+7{MW' );
define( 'LOGGED_IN_SALT',   '?dv;Et]+]Y5E62^4A>N-`<v.,d-$@RLV=A`pqTE4aMEy,$-<~NW%Dk6 RO(O!1WY' );
define( 'NONCE_SALT',       'U;/fo))qoT4L(ts8kI$&sB+q`6!RD*)at=REm_T# PJBcC+`$$JkNXcET]iXWx4a' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
